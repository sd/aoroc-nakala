<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateResourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::connection()->getPdo()->exec('ALTER TABLE public.resource RENAME COLUMN temporal TO temporal_litteraly');

        Schema::table('resource', function (Blueprint $table) {
            $table->string('temporal_start', 30)->nullable(true);
            $table->string('temporal_end', 30)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection()->getPdo()->exec('ALTER TABLE public.resource RENAME COLUMN temporal_litteraly TO temporal');

        Schema::table('resource', function (Blueprint $table) {
            $table->dropColumn('temporal_start');
            $table->dropColumn('temporal_end');
        });
    }
}
