<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableSends extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public.sends', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('users_id')->nullable(false);
            $table->string('project', 20);
            $table->string('name', 50);
            $table->string('file_data', 41);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        DB::connection()->getPdo()->exec('
            ALTER TABLE public.sends ADD COLUMN status public.t_sends_status NOT NULL DEFAULT \'a\';
            CREATE TRIGGER updated_at BEFORE UPDATE ON public.sends FOR EACH ROW EXECUTE PROCEDURE updated_at();
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('public.sends');
    }
}
