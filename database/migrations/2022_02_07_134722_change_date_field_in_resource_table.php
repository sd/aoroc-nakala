<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDateFieldInResourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resource', function (Blueprint $table) {
            $table->dropColumn('available');
            $table->dropColumn('date');
        });

        Schema::table('resource', function (Blueprint $table) {
            $table->date('available')->nullable(true);
            $table->string('date')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resource', function (Blueprint $table) {
            $table->dropColumn('available');
            $table->dropColumn('date');
        });

        Schema::table('resource', function (Blueprint $table) {
            $table->string('available')->nullable(true)->change();
            $table->date('date')->nullable(true)->change();
        });
    }
}
