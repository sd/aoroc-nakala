<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('public.users', function (Blueprint $table) {
            $table->string('firstname', 50)->nullable(true)->default('');
            $table->string('lastname', 50)->nullable(true)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('public.users', function (Blueprint $table) {
            $table->dropColumn(['firstname', 'lastname']);
        });
    }
}
