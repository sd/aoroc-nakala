<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableCsv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::connection()->getPdo()->exec('
            CREATE TABLE public.csv (
                id serial PRIMARY KEY,
                sends_id INTEGER NOT NULL,
                data VARCHAR NOT NULL,
                CONSTRAINT sends_id_fk FOREIGN KEY (sends_id) REFERENCES public.sends (id) ON DELETE CASCADE ON UPDATE CASCADE
            );
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection()->getPdo()->exec('
            DROP TABLE public.csv;
        ');
    }
}
