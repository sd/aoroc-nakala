<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApiKeyFieldToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('public.users', function (Blueprint $table) {
            $table->string('api_key', 36)->nullable(true)->default('');
        });

        DB::connection()->getPdo()->exec('
            CREATE TRIGGER updated_at BEFORE UPDATE ON public.users FOR EACH ROW EXECUTE PROCEDURE updated_at();
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('public.users', function (Blueprint $table) {
            $table->dropColumn('api_key');
        });

        DB::connection()->getPdo()->exec('
            DROP TRIGGER updated_at ON public.users;
        ');
    }
}
