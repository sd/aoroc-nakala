<?php

use Illuminate\Database\Migrations\Migration;

class AddTriggerUpdatedAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = 'CREATE OR REPLACE FUNCTION public.updated_at() RETURNS TRIGGER AS $$
                BEGIN
                    NEW.updated_at = CURRENT_TIMESTAMP(0);
                    RETURN NEW;
                END;
                $$ LANGUAGE plpgsql;

                CREATE TRIGGER role_updated_at BEFORE UPDATE ON public.role FOR EACH ROW EXECUTE PROCEDURE public.updated_at();
                CREATE TRIGGER users_updated_at BEFORE UPDATE ON public.users FOR EACH ROW EXECUTE PROCEDURE public.updated_at();';

        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = 'DROP TRIGGER role_updated_at ON public.role;
                DROP TRIGGER users_updated_at ON public.users;
                DROP FUNCTION public.updated_at();';

        DB::connection()->getPdo()->exec($sql);
    }
}
