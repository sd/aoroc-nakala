<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableResource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public.resource', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sends_id')->nullable(false);
            $table->string('files')->nullable(false);
            $table->text('collection')->nullable(false);

            // Champs obligatoire
            $table->text('nk_title')->nullable(false);
            $table->text('nk_creator')->nullable(true);
            $table->text('nk_created')->nullable(true);
            $table->text('nk_license')->nullable(false);
            $table->text('nk_type')->nullable(false);

            $table->text('abstract')->nullable(true);
            $table->text('access_rights')->nullable(true);
            $table->text('accrual_method')->nullable(true);
            $table->text('accrual_periodicity')->nullable(true);
            $table->text('accrual_policy')->nullable(true);
            $table->text('alternative')->nullable(true);
            $table->text('audience')->nullable(true);
            $table->text('available')->nullable(true);
            $table->text('bibliographic_citation')->nullable(true);
            $table->text('conforms_to')->nullable(true);
            $table->text('contributor')->nullable(true);
            $table->text('coverage')->nullable(true);
            $table->text('created')->nullable(true);
            $table->text('creator')->nullable(true);
            $table->date('date')->nullable(true);
            $table->text('date_accepted')->nullable(true);
            $table->text('date_copyrighted')->nullable(true);
            $table->text('date_submitted')->nullable(true);
            $table->text('description')->nullable(true);
            $table->text('education_level')->nullable(true);
            $table->text('extent')->nullable(true);
            $table->text('format')->nullable(true);
            $table->text('has_format')->nullable(true);
            $table->text('has_part')->nullable(true);
            $table->text('has_version')->nullable(true);
            $table->text('identifier')->nullable(true);
            $table->text('instructional_method')->nullable(true);
            $table->text('is_format_of')->nullable(true);
            $table->text('is_part_of')->nullable(true);
            $table->text('is_referenced_by')->nullable(true);
            $table->text('is_replaced_by')->nullable(true);
            $table->text('is_required_by')->nullable(true);
            $table->text('issued')->nullable(true);
            $table->text('is_version_of')->nullable(true);
            $table->text('language')->nullable(false);
            $table->text('license')->nullable(true);
            $table->text('mediator')->nullable(true);
            $table->text('medium')->nullable(true);
            $table->text('modified')->nullable(true);
            $table->text('provenance')->nullable(true);
            $table->text('publisher')->nullable(true);
            $table->text('reference')->nullable(true);
            $table->text('relation')->nullable(true);
            $table->text('replaces')->nullable(true);
            $table->text('requires')->nullable(true);
            $table->text('rights')->nullable(true);
            $table->text('rights_holder')->nullable(true);
            $table->text('same_as')->nullable(true);
            $table->text('source')->nullable(true);
            $table->text('spatial')->nullable(true);
            $table->text('subject')->nullable(true);
            $table->text('table_of_contents')->nullable(true);
            $table->text('temporal')->nullable(true);
            $table->text('title')->nullable(true);
            $table->text('type')->nullable(true);
            $table->text('valid')->nullable(true);
            $table->text('update_fields')->nullable(true);

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->string('error')->nullable(true);

            $table->foreign('sends_id')
                ->references('id')
                ->on('sends')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        DB::connection()->getPdo()->exec('
            ALTER TABLE public.resource ADD COLUMN status public.t_resource_status NOT NULL DEFAULT \'wt\';
            CREATE TRIGGER updated_at BEFORE UPDATE ON public.resource FOR EACH ROW EXECUTE PROCEDURE updated_at();
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('public.resource');
    }
}
