<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeSendsStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // t_sends_status_users
        // - published : envoyé et publié
        // - pending   : envoyé et en attente
        DB::connection()->getPdo()->exec('
            CREATE TYPE public.t_sends_status_users AS ENUM (\'published\', \'pending\');
            ALTER TABLE public.sends ADD COLUMN sends_status_users public.t_sends_status_users NOT NULL DEFAULT \'published\';
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection()->getPdo()->exec('
            ALTER TABLE public.sends DROP COLUMN sends_status_users;
            DROP TYPE public.t_sends_status_users;
        ');
    }
}
