<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // t_sends_status
        // - a : envoi actif
        // - p : envoi purgé
        // - s : envoi supprimé

        // t_sends_detail_status                    - Codes couleur bootstrap
        // - er : ligne en erreur                   - danger
        // - wt : en attente de postage sur NAKALA  - secondary
        // - sp : en cours d'envoi                  - warning
        // - po : posté sur NAKALA - pending        - info
        // - pu : publié sur NAKALA - published     - success
        // - up : à renvoyer suite à mise à jour    - info

        $sql = '
            CREATE TYPE public.t_sends_status AS ENUM (\'a\', \'p\', \'s\');
            CREATE TYPE public.t_resource_status AS ENUM (\'er\', \'wt\', \'sp\', \'po\', \'pu\', \'up\');
        ';

        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = '
            DROP TYPE public.t_sends_status;
            DROP TYPE public.t_resource_status;
        ';

        DB::connection()->getPdo()->exec($sql);
    }
}
