<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon as Carbon;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('role')->insert([
            [
                'name' => 'Administrateur',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'name' => 'Utilisateur',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}
