<?php

namespace Tests\Unit;

use Tests\TestCase;
//use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Http\Controllers\CheckResourceController;
use Illuminate\Support\Facades\DB;

class CheckResourceTest extends TestCase
{


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkNakalaLicenseIsOk()
    {
        $license = 'NAKALA LICENSE';
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkNakalaLicense($license);
        $metas = $checkResourceController->getMetas();
        $check_errors = $checkResourceController->getErrors();

        $this->assertIsArray($metas);                      // Check si $metas est bien un tableau
        $this->assertArrayHasKey('value', $metas[0]);      // Check si la clé "value" est bien présente dans le tableau $metas[0]
        $this->assertEquals($license, $metas[0]['value']); // Check si la valeur de $metas[0]['value'] est = $license

        $this->assertIsArray($check_errors);               // Check si $check_errors est bien un tableau
        $this->assertEmpty($check_errors);                 // Check si $check_errors est bien un tableau vide
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkNakalaLicenseIsNotOk()
    {
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkNakalaLicense('');
        $metas = $checkResourceController->getMetas();
        $check_errors = $checkResourceController->getErrors();

        $this->assertIsArray($metas);                                               // Check si $metas est bien un tableau
        $this->assertArrayHasKey('nk_license', $check_errors);                      // Check si la clé "nk_license" est bien présente dans le tableau $check_errors[0]
        $this->assertEquals('Licence non renseignée', $check_errors['nk_license']); // Check si la valeur de $check_errors['nk_license'] est = 'Licence non renseignée'
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkNakalaTypeIsOk()
    {
        $type = 'NAKALA TYPE';
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkNakalaType($type);
        $metas = $checkResourceController->getMetas();
        $check_errors = $checkResourceController->getErrors();

        $this->assertIsArray($metas);
        $this->assertArrayHasKey('value', $metas[0]);
        $this->assertEquals($type, $metas[0]['value']);

        $this->assertIsArray($check_errors);
        $this->assertEmpty($check_errors);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkNakalaTypeIsNotOk()
    {
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkNakalaType('');
        $metas = $checkResourceController->getMetas();
        $check_errors = $checkResourceController->getErrors();

        $this->assertIsArray($metas);
        $this->assertArrayHasKey('nk_type', $check_errors);
        $this->assertEquals('Type non renseigné', $check_errors['nk_type']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkUniqueCollectionIsOk()
    {
        $collection = '10.34847/nkl.b992bs33';
        $checkResourceController = new CheckResourceController;

        $return = $checkResourceController->checkCollection($collection);
        $check_errors = $checkResourceController->getErrors();

        $this->assertIsArray($return);
        $this->assertEquals($collection, $return[0]);

        $this->assertIsArray($check_errors);
        $this->assertEmpty($check_errors);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkMultipleCollectionIsOk()
    {
        $collection = '10.34847/nkl.b992bs33;10.34847/nkl.69da8jzu4';
        $checkResourceController = new CheckResourceController;

        $return = $checkResourceController->checkCollection($collection);
        $check_errors = $checkResourceController->getErrors();

        $this->assertIsArray($return);
        $this->assertCount(2, $return);
        $this->assertEquals('10.34847/nkl.b992bs33', $return[0]);
        $this->assertEquals('10.34847/nkl.69da8jzu4', $return[1]);

        $this->assertIsArray($check_errors);
        $this->assertEmpty($check_errors);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkCollectionIsNotOk()
    {
        $checkResourceController = new CheckResourceController;

        $return = $checkResourceController->checkCollection('');
        $check_errors = $checkResourceController->getErrors();

        $this->assertIsArray($return);
        $this->assertArrayHasKey('collection', $check_errors);
        $this->assertEquals('collection manquante', $check_errors['collection']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkNakalaCreatedIsOk()
    {
        $date = '2000';
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkNakalaCreated($date);
        $metas = $checkResourceController->getMetas();

        $this->assertIsArray($metas);                       // Check si $metas est bien un tableau
        $this->assertArrayHasKey('value', $metas[0]);       // Check si la clé "value" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('lang', $metas[0]);        // Check si la clé "lang" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('propertyUri', $metas[0]); // Check si la clé "propertyUri" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('typeUri', $metas[0]);     // Check si la clé "typeUri" est bien présente dans le tableau $metas[0]

        $this->assertEquals($date, $metas[0]['value']);
        $this->assertEquals(null, $metas[0]['lang']);
        $this->assertEquals('http://nakala.fr/terms#created', $metas[0]['propertyUri']);
        $this->assertEquals('http://www.w3.org/2001/XMLSchema#string', $metas[0]['typeUri']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkNakalaCreatedIsNotOk()
    {
        $date = '';
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkNakalaCreated($date);
        $metas = $checkResourceController->getMetas();

        $this->assertIsArray($metas);                       // Check si $metas est bien un tableau
        $this->assertArrayHasKey('value', $metas[0]);       // Check si la clé "value" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('lang', $metas[0]);        // Check si la clé "lang" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('propertyUri', $metas[0]); // Check si la clé "propertyUri" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('typeUri', $metas[0]);     // Check si la clé "typeUri" est bien présente dans le tableau $metas[0]

        $this->assertEquals(null, $metas[0]['value']);
        $this->assertEquals(null, $metas[0]['lang']);
        $this->assertEquals('http://nakala.fr/terms#created', $metas[0]['propertyUri']);
        $this->assertEquals(null, $metas[0]['typeUri']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkNakalaCreatorIsOk()
    {
        $creator = 'CREATOR';
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkNakalaCreator($creator);
        $metas = $checkResourceController->getMetas();

        $this->assertIsArray($metas);                       // Check si $metas est bien un tableau
        $this->assertArrayHasKey('value', $metas[0]);       // Check si la clé "value" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('lang', $metas[0]);        // Check si la clé "lang" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('propertyUri', $metas[0]); // Check si la clé "propertyUri" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('typeUri', $metas[0]);     // Check si la clé "typeUri" est bien présente dans le tableau $metas[0]

        $this->assertEquals($creator, $metas[0]['value']['givenname']);
        $this->assertEquals('', $metas[0]['value']['surname']);
        $this->assertEquals(null, $metas[0]['value']['orcid']);
        $this->assertEquals(null, $metas[0]['lang']);
        $this->assertEquals('http://nakala.fr/terms#creator', $metas[0]['propertyUri']);
        $this->assertEquals('http://www.w3.org/2001/XMLSchema#string', $metas[0]['typeUri']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkNakalaCreatorIsNotOk()
    {
        $creator = '';
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkNakalaCreator($creator);
        $metas = $checkResourceController->getMetas();

        $this->assertIsArray($metas);                       // Check si $metas est bien un tableau
        $this->assertArrayHasKey('value', $metas[0]);       // Check si la clé "value" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('lang', $metas[0]);        // Check si la clé "lang" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('propertyUri', $metas[0]); // Check si la clé "propertyUri" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('typeUri', $metas[0]);     // Check si la clé "typeUri" est bien présente dans le tableau $metas[0]

        $this->assertEquals(null, $metas[0]['value']);
        $this->assertEquals(null, $metas[0]['lang']);
        $this->assertEquals('http://nakala.fr/terms#creator', $metas[0]['propertyUri']);
        $this->assertEquals(null, $metas[0]['typeUri']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkTemporalIsOkWithThreeValues_TemporalLitteralyStartEnd()
    {
        $temporal_litteraly = 'IIe-Ve siècle';
        $temporal_start = '150';
        $temporal_end = '180';
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkTemporal($temporal_litteraly, $temporal_start, $temporal_end);
        $metas = $checkResourceController->getMetas();

        $this->assertIsArray($metas);                       // Check si $metas est bien un tableau
        $this->assertArrayHasKey('value', $metas[0]);       // Check si la clé "value" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('lang', $metas[0]);        // Check si la clé "lang" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('propertyUri', $metas[0]); // Check si la clé "propertyUri" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('typeUri', $metas[0]);     // Check si la clé "typeUri" est bien présente dans le tableau $metas[0]

        $this->assertEquals($temporal_litteraly. ';start=' . $temporal_start. ';end=' . $temporal_end, $metas[0]['value']);
        $this->assertEquals(null, $metas[0]['lang']);
        $this->assertEquals('http://purl.org/dc/terms/temporal', $metas[0]['propertyUri']);
        $this->assertEquals('http://www.w3.org/2001/XMLSchema#string', $metas[0]['typeUri']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkTemporalIsOkWithTwoValues_StartEnd()
    {
        $temporal_litteraly = '';
        $temporal_start = '150';
        $temporal_end = '180';
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkTemporal($temporal_litteraly, $temporal_start, $temporal_end);
        $metas = $checkResourceController->getMetas();

        $this->assertIsArray($metas);                       // Check si $metas est bien un tableau
        $this->assertArrayHasKey('value', $metas[0]);       // Check si la clé "value" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('lang', $metas[0]);        // Check si la clé "lang" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('propertyUri', $metas[0]); // Check si la clé "propertyUri" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('typeUri', $metas[0]);     // Check si la clé "typeUri" est bien présente dans le tableau $metas[0]

        $this->assertEquals($temporal_litteraly. ';start=' . $temporal_start. ';end=' . $temporal_end, $metas[0]['value']);
        $this->assertEquals(null, $metas[0]['lang']);
        $this->assertEquals('http://purl.org/dc/terms/temporal', $metas[0]['propertyUri']);
        $this->assertEquals('http://www.w3.org/2001/XMLSchema#string', $metas[0]['typeUri']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkTemporalIsOkWithOneValue_Start()
    {
        $temporal_litteraly = '';
        $temporal_start = '150';
        $temporal_end = '';
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkTemporal($temporal_litteraly, $temporal_start, $temporal_end);
        $metas = $checkResourceController->getMetas();

        $this->assertIsArray($metas);                       // Check si $metas est bien un tableau
        $this->assertArrayHasKey('value', $metas[0]);       // Check si la clé "value" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('lang', $metas[0]);        // Check si la clé "lang" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('propertyUri', $metas[0]); // Check si la clé "propertyUri" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('typeUri', $metas[0]);     // Check si la clé "typeUri" est bien présente dans le tableau $metas[0]

        $this->assertEquals(';start=' . $temporal_start. ';', $metas[0]['value']);
        $this->assertEquals(null, $metas[0]['lang']);
        $this->assertEquals('http://purl.org/dc/terms/temporal', $metas[0]['propertyUri']);
        $this->assertEquals('http://www.w3.org/2001/XMLSchema#string', $metas[0]['typeUri']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkTemporalIsOkWithOneValue_End()
    {
        $temporal_litteraly = '';
        $temporal_start = '';
        $temporal_end = '180';
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkTemporal($temporal_litteraly, $temporal_start, $temporal_end);
        $metas = $checkResourceController->getMetas();

        $this->assertIsArray($metas);                       // Check si $metas est bien un tableau
        $this->assertArrayHasKey('value', $metas[0]);       // Check si la clé "value" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('lang', $metas[0]);        // Check si la clé "lang" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('propertyUri', $metas[0]); // Check si la clé "propertyUri" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('typeUri', $metas[0]);     // Check si la clé "typeUri" est bien présente dans le tableau $metas[0]

        $this->assertEquals(';;end=' . $temporal_end, $metas[0]['value']);
        $this->assertEquals(null, $metas[0]['lang']);
        $this->assertEquals('http://purl.org/dc/terms/temporal', $metas[0]['propertyUri']);
        $this->assertEquals('http://www.w3.org/2001/XMLSchema#string', $metas[0]['typeUri']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkUniqueMetaIsOk()
    {
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkUniqueMeta('abstract', 'http://purl.org/dc/terms/abstract', 'http://www.w3.org/2001/XMLSchema#');
        $metas = $checkResourceController->getMetas();

        $this->assertIsArray($metas);                       // Check si $metas est bien un tableau
        $this->assertArrayHasKey('value', $metas[0]);       // Check si la clé "value" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('lang', $metas[0]);        // Check si la clé "lang" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('propertyUri', $metas[0]); // Check si la clé "propertyUri" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('typeUri', $metas[0]);     // Check si la clé "typeUri" est bien présente dans le tableau $metas[0]

        $this->assertEquals('abstract', $metas[0]['value']);
        $this->assertEquals(null, $metas[0]['lang']);
        $this->assertEquals('http://purl.org/dc/terms/abstract', $metas[0]['propertyUri']);
        $this->assertEquals('http://www.w3.org/2001/XMLSchema#', $metas[0]['typeUri']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkFileIsOk()
    {
        $checkResourceController = new CheckResourceController;

        $data_file = [
            [
                'file' => [
                    'sha1' => 'd3490dffd7873c39c7b5ee90c6396be11d3c9ecb',
                    'name' => 'test.jpg',
                    //'description' => 'Petite description',
                ]
            ]
        ];

        $file = $checkResourceController->checkFiles($data_file);

        $this->assertIsArray($file);
        $this->assertArrayHasKey('sha1', $file[0]);
        $this->assertArrayHasKey('name', $file[0]);
        $this->assertArrayHasKey('embargoed', $file[0]);
        //$this->assertArrayHasKey('description', $file[0]);

        $this->assertEquals($data_file[0]['file']['sha1'], $file[0]['sha1']);
        $this->assertEquals($data_file[0]['file']['name'], $file[0]['name']);
        $this->assertEquals(date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))), $file[0]['embargoed']);
        //$this->assertEquals($data_file[0]['file']['description'], $file[0]['description']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkUniqueMetaIsNotOkAndNotRequired()
    {
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkUniqueMeta('', '', '');
        $metas = $checkResourceController->getMetas();

        $this->assertEmpty($metas); // Check si $metas est bien vide vu que $value n'est pas renseigné
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkUniqueMetaIsNotOkAndRequired()
    {
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkUniqueMeta('', 'http://purl.org/dc/terms/abstract', '', null, true);
        $metas = $checkResourceController->getMetas();

        $this->assertEmpty($metas); // Check si $metas est bien vide vu que $value n'est pas renseigné

        $errors = $checkResourceController->getErrors();

        $this->assertIsArray($errors);
        $this->assertArrayHasKey('abstract', $errors);
        $this->assertEquals('Champ abstract non renseigné', $errors['abstract']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkMultipleMetaIsOk()
    {
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkMultipleMeta('contributor', 'http://purl.org/dc/terms/contributor', 'http://www.w3.org/2001/XMLSchema#');
        $metas = $checkResourceController->getMetas();

        $this->assertIsArray($metas);                       // Check si $metas est bien un tableau
        $this->assertArrayHasKey('value', $metas[0]);       // Check si la clé "value" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('lang', $metas[0]);        // Check si la clé "lang" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('propertyUri', $metas[0]); // Check si la clé "propertyUri" est bien présente dans le tableau $metas[0]
        $this->assertArrayHasKey('typeUri', $metas[0]);     // Check si la clé "typeUri" est bien présente dans le tableau $metas[0]

        $this->assertEquals('contributor', $metas[0]['value']);
        $this->assertEquals(null, $metas[0]['lang']);
        $this->assertEquals('http://purl.org/dc/terms/contributor', $metas[0]['propertyUri']);
        $this->assertEquals('http://www.w3.org/2001/XMLSchema#', $metas[0]['typeUri']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkMultipleMetaIsNotOkAndNotRequired()
    {
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkMultipleMeta('', '', '');
        $metas = $checkResourceController->getMetas();

        $this->assertEmpty($metas); // Check si $metas est bien vide vu que $value n'est pas renseigné
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkMultipleMetaIsNotOkAndRequired()
    {
        $checkResourceController = new CheckResourceController;

        $checkResourceController->checkUniqueMeta('', 'http://purl.org/dc/terms/contributor', '', null, true);
        $metas = $checkResourceController->getMetas();

        $this->assertEmpty($metas); // Check si $metas est bien vide vu que $value n'est pas renseigné

        $errors = $checkResourceController->getErrors();

        $this->assertIsArray($errors);
        $this->assertArrayHasKey('contributor', $errors);
        $this->assertEquals('Champ contributor non renseigné', $errors['contributor']);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkOtherMetasIsOk()
    {
        $checkResourceController = new CheckResourceController;

        // TODO : Vider et insérer dans la table une ressource pour être sur d'avoir la ligne qu'on veux
        $data = DB::select('
            SELECT r.id AS resource_id, r.*, s.project, s.sends_status_users
            FROM public.resource AS r
                LEFT JOIN public.sends AS s ON r.sends_id = s.id
            WHERE r.sends_id = :sends_id
            ORDER BY r.status DESC, r.id',
            ['sends_id' => 241]
        );

        $resource = (array) $data[0];

        $checkResourceController->checkOtherMetas($resource);
        $metas = $checkResourceController->getMetas();
        $error = $checkResourceController->getErrors();

        $this->assertIsArray($metas);
        $this->assertIsArray($error);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * @test
     * @return void
     */
    public function checkSetErrorMetaReturnError()
    {
        $checkResourceController = new CheckResourceController;

        $checkResourceController->setErrorMeta('http://purl.org/dc/terms/contributor');
        $error = $checkResourceController->getErrors();

        $this->assertIsArray($error); // Check si $metas est bien vide vu que $value n'est pas renseigné
        $this->assertArrayHasKey('contributor', $error);
    }
}
