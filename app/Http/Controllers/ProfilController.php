<?php

namespace App\Http\Controllers;

use App\Models\Profil;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProfilController extends Controller
{
    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * Display the specified resource
     */
    public function show()
    {
        $users = DB::table('users')
            ->leftJoin('role', 'users.id_role', '=', 'role.id')
            ->select('users.id', 'users.name', 'users.email', 'users.firstname', 'users.lastname', 'role.name AS role')
            ->where('users.id', '=', Auth::user()->id)
            ->get();

        return view('profil', ['user' => $users[0]]);
    }

    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * Show the form for editing the specified resource
     */
    public function edit()
    {
        return DB::select(
            'SELECT api_key FROM public.users WHERE id = :id_user',
            ['id_user' => Auth::user()->id]
        )[0];
    }

    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * Update the specified resource in storage
     */
    public function update(Request $request)
    {
        if (strlen($request->api_key) !== 36)
            return 'error_api_key';

        DB::update(
            'UPDATE public.users SET api_key = :api_key WHERE id = :id_users',
            ['api_key' => $request->api_key, 'id_users' => Auth::user()->id]);
    }
}
