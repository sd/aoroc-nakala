<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;
use App\Models\Sends;
use App\Http\Controllers\ProcessingDataFileController;
use App\Http\Controllers\ProcessingMediaFileController;
use App\Http\Controllers\ResourceController;

class SendsController extends Controller
{
    private $sends_id = 0;
    private $rand_media_dir = '';


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * index
     */
    public function index()
    {
        return view('sends');
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * setProjectChoice
     */
    public function setProjectChoice(string $choice): string
    {
        switch ($choice)
        {
        case 'nahan':
            $project_name = 'NAHAN';
            $project_choice = 'nahan';
            break;

        default:
            $project_name = 'AUTRE';
            $project_choice = 'autre';
        }

        session(['project_choice' => $project_choice]);

        return $project_name;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * setSendsName
     */
    public function setSendsName(string $name): string
    {
        if (!empty($name) and strlen($name) <= 50)
        {
            $car = config('aoroc_nakala.sends.name.replace_car');
            $name = str_replace($car[0], $car[1], $name);

            session(['sends_name' => trim($name)]);
        }
        else
            $name = 'error';

        return $name;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * uploadDataFile
     */
    public function uploadDataFile(Request $request)
    {
        $data_file = $request->file('data_file');
        $extention = $data_file->getClientOriginalExtension();

        if (in_array(strtolower($extention), ['csv', 'json']))
        {
            $uuid = Uuid::uuid4();
            $new_name = $uuid . '.' . $extention;
            $data_file->move(base_path('upload/data'), $new_name);
            session(['data_file_name' => $new_name]);

            //Log::channel('upload')->info('Envoi du fichier de données => ' . $new_name);

            return 'success'; //back()->with('success', 'Fichier envoyé');
        }
        else
            return 'error'; //back()->with('error', 'ERROR');
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * uploadMediaFile
     */
    public function uploadMediaFile(Request $request)
    {
        $this->rand_media_dir = base_path('upload/media') . '/' . rand();

        if (!file_exists($this->rand_media_dir))
            mkdir($this->rand_media_dir);

        foreach ($request->file('media_file') as $image)
        {
            //$extention = $image->getClientOriginalExtension();
            //$file_name = lower_extension($image->getClientOriginalName());

            if (in_array(strtolower($image->getClientOriginalExtension()), ['jpg', 'png', 'gif', 'pdf', 'zip', '7z']))
            {
                $image->move($this->rand_media_dir, strtolower($image->getClientOriginalName()));
                //Log::channel('upload')->info('Envoi du fichier média => ' . $image->getClientOriginalName());
            }
            else
            {
                rrmdir($this->rand_media_dir);

                return 'error';
            }
        }

        // Enregistrement des infos de l'envoi
        if (!$this->saveSendsData())
            return 'error';

        // Check des fichiers média
        return ($this->checkMediaFiles())
            ? 'success'
            : 'error';
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * saveSendsData
     */
    private function saveSendsData()
    {
        $sends = new Sends;
        $sends->project = session('project_choice');
        $sends->name = session('sends_name');
        $sends->users_id = Auth::user()->id;
        $sends->file_data = session('data_file_name');

        $sends->save();
        $this->sends_id = $sends->id;

        session(['sends_id' => $this->sends_id]);

        // Traitement du fichier de données CSV ou JSON
        $processing_data_file = new ProcessingDataFileController;

        return $processing_data_file->checkFileType($sends->file_data, $this->sends_id, $sends->project);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * setSendsStatus
     */
    public function setSendsStatus(string $choice): string
    {
        $this->updateSendsStatus(session('sends_id'), $choice);

        return 'success';
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkMediaFiles
     */
    private function checkMediaFiles(): bool
    {
        // Traitement des fichiers média
        $processing_media_file = new ProcessingMediaFileController(
            $this->rand_media_dir,
            $this->sends_id
        );

        return $processing_media_file->processingMediaFile();
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * sendsList
     */
    public function sendsList()
    {
        $query = DB::select('
            SELECT s.id AS sends_id, project, name, sends_status_users,
                   TO_CHAR(s.created_at, \'DD-MM-YYYY\') AS created_at,
                   COUNT(r.id) AS nb_sends_line,
                   COUNT(*) FILTER (WHERE nakala_id != \'\') AS resources_on_nakala
            FROM public.sends AS s
                LEFT JOIN public.resource AS r ON s.id = r.sends_id
            WHERE users_id = :users_id
            GROUP BY s.id ORDER BY s.id DESC',
            ['users_id' => Auth::user()->id]
        );

        $sends_list = $sends_id = [];

        foreach ($query as $k => $v)
        {
            $sends_list[$k] = $v;
            $sends_list[$k]->statut = $this->sendsNbStatus($v->sends_id);
            $sends_id[] = $v->sends_id;
        }

        return view('sends_list', [
            'sends_list' => $sends_list,
            //'sends_resource_status_list' => $this->sendsResourceStatus(),
            'btn_delete_on_nakala' => $this->displayBtnDeleteNakala($sends_id),
        ]);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * sendsResourceStatus
     *
     * @param int $sends_id
     * @return string
     */
    private function sendsNbStatus(int $sends_id): string
    {
        $query = DB::select(
            'SELECT COUNT(status) AS nb_sends_line, status FROM public.resource WHERE sends_id = :sends_id GROUP BY status',
            ['sends_id' => $sends_id]
        );

        $tab_statut = [];

        foreach ($query as $v)
            switch ($v->status)
            {
                case $v->status:
                    $tab_statut[] = $v->status;
                    break;
            }

        if (in_array('er', $tab_statut))
            return 'er';

        if (in_array('wt', $tab_statut))
            return 'wt';

        if (in_array('po', $tab_statut))
            return 'po';

        if (in_array('up', $tab_statut))
            return 'up';

        return '--';
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * sendsResourceStatus
     *
     * @param bool $admin_mode false
     * @return array
     */
    private function sendsResourceStatus(bool $admin_mode = false): array
    {
        if (!$admin_mode)
            $query = DB::select('
                SELECT DISTINCT(COUNT(r.status)) AS nb, r.status, sends_id FROM public.resource AS r
                LEFT JOIN sends s ON s.id = r.sends_id WHERE users_id = :users_id
                GROUP BY r.status, sends_id ORDER BY r.status',
                ['users_id' => Auth::user()->id]
            );
        else
            $query = DB::select('
                SELECT DISTINCT(COUNT(r.status)) AS nb, r.status, sends_id
                FROM public.resource AS r LEFT JOIN sends s ON s.id = r.sends_id
                GROUP BY r.status, sends_id ORDER BY r.status');

        $sends_status_list = [];
        $resource = new ResourceController;

        foreach ($query as $k => $v)
        {
            $sends_status_list[$v->sends_id][] = [
                'nb' => $v->nb,
                'status' => $v->status,
                'color' => $resource->getResourceInfoStatus()[$v->status]['color'],
                'description' => $resource->getResourceInfoStatus()[$v->status]['description'],
            ];
        }

        return $sends_status_list;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * adminSendsList
     */
    public function adminSendsList()
    {
        $query = DB::select('
            SELECT s.id AS sends_id, project, s.name, TO_CHAR(s.created_at, \'DD-MM-YYYY\') AS created_at, COUNT(r.id) AS nb_sends_line,
                   CONCAT(u.firstname, \' \', u.lastname) AS user
            FROM public.sends AS s
                LEFT JOIN public.resource AS r ON s.id = r.sends_id
                LEFT JOIN public.users AS u ON u.id = s.users_id
            GROUP BY s.id, u.firstname, u.lastname ORDER BY s.id DESC'
        );

        return view('admin.sends_list', [
            'sends_list' => $query,
            'sends_resource_status_list' => $this->sendsResourceStatus(true),
        ]);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * changeSendsStatus
     *
     * @param int $sends_id
     * @param string $status
     */
    public function changeSendsStatus(int $sends_id, string $status)
    {
        $this->updateSendsStatus($sends_id, $status);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * setSendsStatusOnNakala
     *
     * @param int $sends_id
     * @param string $status
     */
    public function changeSendsStatusOnNakala(int $sends_id)
    {
        $resource = new ResourceController;
        $resource->getResourcesForUpdateStatusOnNakala($sends_id); // Modif du statut via l'API NAKALA
        $this->updateSendsStatus($sends_id, 'published'); // Modif du statut dans la base d'importation
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * setSendsStatus
     *
     * @param int $sends_id
     * @param string $status
     */
    private function updateSendsStatus(int $sends_id, string $status)
    {
        DB::update(
            'UPDATE public.sends SET sends_status_users = :sends_status_users WHERE id = :sends_id',
            ['sends_status_users' => $status, 'sends_id' => $sends_id]
        );
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * displayBtnDeleteNakala
     *
     * @param array $arr_sends_id
     * @return array
     */
    private function displayBtnDeleteNakala(array $arr_sends_id): array
    {
        $arr_sends_id = array_flip($arr_sends_id);

        $query_result = DB::select(
            'WITH q AS (
                SELECT
                    sends_id,
                    COUNT(*) AS n_resources_total,
                    COUNT(*) FILTER (WHERE r.status = \'po\' AND nakala_id != \'\') AS n_resources_ok
                FROM public.resource AS r
                    LEFT JOIN public.sends AS s ON s.id = r.sends_id
                WHERE users_id = :users_id 
                GROUP BY sends_id
                ORDER BY sends_id
            )
            SELECT sends_id FROM q
            WHERE n_resources_ok = n_resources_total',
            ['users_id' => Auth::user()->id]
        );

        $sends_id = [];

        if (!empty($query_result))
        {
            foreach ($query_result as $v)
                $result[$v->sends_id] = null;

            foreach ($arr_sends_id as $id => $v)
                $sends_id[$id] = (array_key_exists($id, $result)) ? 'ok' : 'no';

            return $sends_id;
        }
        else
            return $arr_sends_id;
    }
}
