<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ResourceErrorController extends Controller
{
    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * saveError
     *
     * @param int $resource_id
     * @param array $errors
     * @return void
     */
    public static function saveError(int $resource_id, array $errors): void
    {
        $error = '';

        foreach ($errors as $k => $v)
        {
            $error .= $k . '|' . $v . ';';
            Log::error('ResourceErrorController::saveError() |---> [ champ : ' . $k . ' ] [ message : ' . $v . ' ]');
        }

        self::updateError($resource_id, 'er', $error, true);
    }

    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkErrorToReset
     *
     * @desc  Vérifie si un champ est en erreur et si oui la supprime
     * @param int $resource_id
     * @param string $field
     * @return void
     */
    public static function checkErrorToReset(int $resource_id, string $field): void
    {
        Log::info('ResourceErrorController::checkErrorToReset() [ resource_id = ' . $resource_id . ' ] [ field = ' . $field . ' ]');

        //$status = 'wt';
        $error = '';
        $errors = DB::select(
            'SELECT error, status FROM public.resource WHERE id = :resource_id AND error LIKE :field',
            ['resource_id' => $resource_id, 'field' => '%' . $field . '%']
        );

        if (!empty($errors))
        {
            $arr_errors = explode(';', substr($errors[0]->error, 0, -1));

            foreach ($arr_errors as $k => $v)
            {
                $a = explode('|', $v);

                if ($a[0] === $field)
                    unset($arr_errors[$k]);
                else
                    $error .= $v . ';';
            }

            $status = (empty($error)) ? 'wt' : 'er'; // Si il n'y a plus d'erreur on passe la ressource en "wt"
            self::updateError($resource_id, $status, $error);
        }
        /*elseif ($errors[0]->error === 'sp')
        {
            self::updateError($resource_id, $status, $error);
        }*/
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * saveErrorNakala
     *
     * @param int $resource_id
     * @param string $message
     * @param object message
     * @return void
     */
    public static function saveErrorNakala(int $resource_id, string $message, object $output): void
    {
        $message = (!empty($output->payload->validationErrors[0]))
            ? $output->payload->validationErrors[0]
            : ((!empty($output->message))
                ? $output->message
                : $message);

        Log::info('ResourceErrorController::saveErrorNakala() [ resource_id = ' . $resource_id . ' ] [ message = ' . $message . ']');

        switch ($message)
        {
        case (strstr($message, 'collection') !== false):
            // "{"code":404,"message":"Following collections not found : 10.34847\/nkl.b992bs33, 10.34847\/nkl.69da8jzu","payload":[404]}"
            $field = 'collection';
            break;

        case (strstr($message, 'nakala.fr/terms#created') !== false):
            // {"code":422, "message":"Data could not be submitted because of invalid data",
            // "payload":{"validationErrors":["The value \"moi\" for metadata http:\/\/nakala.fr\/terms#created is unauthorized."]}}"
            $field = 'nk_created';
            break;

        case (strstr($message, 'curl') !== false):
            $field = 'curl';
            break;

        default:
            $field = 'error';
        }

        self::updateError($resource_id, 'er', $field . '|' . $message . ';');
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * updateError
     *
     * @param int $resource_id
     * @param string $status
     * @param string $error
     * @param bool $concat
     * @return void
     */
    private static function updateError(int $resource_id, string $status, string $error, bool $concat = false): void
    {
        Log::info('ResourceErrorController::updateError() [ resource_id = ' . $resource_id . ' ] [ status = ' . $status . ' ] [ error = ' . $error . ' ]');

        $c = ($concat) ? 'CONCAT(error, :error::VARCHAR)' : ':error';

        DB::update(
            'UPDATE public.resource SET error = ' . $c . ', status = :status WHERE id = :resource_id',
            ['error' => $error, 'status' => $status, 'resource_id' => $resource_id]
        );
    }
}
