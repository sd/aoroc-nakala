<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ResourceErrorController;

class UpdateMediaFileController extends Controller
{

    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * uploadMediaFile
     *
     * @param  Request $request
     * @return bool|string
     */
    public function updateMediaFile(Request $request)
    {
        $files = '';
        $replace_car = config('aoroc_nakala.replace_car');
        $media_dir = base_path('upload/media') . '/' . $request->sends_id;

        // Suppression des fichiers présents sur le serveur
        $this->removeOldFilesOnServer($media_dir, $request->resource_id);

        // Suppression des références aux fichiers en base de donnnées
        $this->removeOldFilesOnDatabase($request->resource_id);

        // Envoi des nouveaux fichiers
        foreach ($request->file('media_file') as $image)
        {
            $file_name = $image->getClientOriginalName();
            $new_file_name = str_replace($replace_car[0], $replace_car[1], $file_name);

            if (in_array(strtolower($image->getClientOriginalExtension()), ['jpg', 'png', 'gif', 'pdf']))
            {
                $image->move($media_dir, $file_name);
                rename($media_dir . '/' . $file_name, $media_dir . '/' . $new_file_name);
                $files .= $new_file_name . ';';
            }
            else
                return 'error|Type de fichier incorrect. Types acceptés : JPG, PNG, GIF et PDF';
        }

        $this->updateFilesDB($files, $request->resource_id);

        // Reset de l'erreur si existante
        ResourceErrorController::checkErrorToReset($request->resource_id, 'files');

        return true;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * removeOldFilesOnServer
     *
     * @param string $media_dir
     * @param int $resource_id
     * @return void
     */
    private function removeOldFilesOnServer(string $media_dir, int $resource_id): void
    {
        $result = DB::select('SELECT files FROM public.resource WHERE id = :resource_id', ['resource_id' => $resource_id])[0]->files;
        $files = explode(';', $result);

        foreach ($files as $file)
            @unlink($media_dir . '/' . $file);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * removeOldFilesOnDatabase
     *
     * @param string $media_dir
     * @param int $resource_id
     * @return void
     */
    private function removeOldFilesOnDatabase(int $resource_id): void
    {
        DB::update('UPDATE public.resource SET files = \'\' WHERE id = :resource_id', ['resource_id' => $resource_id]);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * updateFilesDB
     *
     * @param string $files
     * @param int $resource_id
     * @return void
     */
    private function updateFilesDB(string $files, int $resource_id): void
    {
        // Si ID nakala déjà présent on passe le statut en "up"
        $nakala_id = DB::table('public.resource')->select('nakala_id')->where('id', '=', $resource_id)->get()[0]->nakala_id;
        $update_list = '';

        if (!empty($nakala_id)) // nakala_id présent
        {
            // Check si le champ est déjà présent dans la liste des champs à mettre à jour
            $f = DB::select('SELECT update_fields FROM public.resource WHERE id = :resource_id AND update_fields LIKE \'%files%\'', ['resource_id' => $resource_id]);
            $update_fields = '';

            if (empty($f)) // Champ "files" non présent dans resource.update_fields
                $update_fields = ', update_fields = CONCAT(update_fields, \'files|\'::VARCHAR)';

            $update_list = ', status = \'up\'' . $update_fields;
        }

        DB::update(
            'UPDATE public.resource SET files = :files ' . $update_list . ' WHERE id = :resource_id',
            ['files' => substr($files, 0, -1), 'resource_id' => $resource_id]
        );
    }
}
