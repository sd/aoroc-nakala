<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Log;
use App\Models\Sends;
use App\Http\Controllers\ProcessingDataFileController;

class SendsErrorController extends Controller
{
    private $sends_id = 0;
    private $rand_media_dir = '';
    private $file = '';
    private $check = '';


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * sendsListError
     */
    public function sendsListError()
    {
        $query = DB::select('
            SELECT s.id AS sends_id, project, name, sends_status_users,
                   TO_CHAR(s.created_at, \'DD-MM-YYYY\') AS created_at,
                   COUNT(r.id) AS nb_sends_line
            FROM public.sends AS s
                RIGHT JOIN public.csv AS r ON s.id = r.sends_id
            WHERE users_id = :users_id
            GROUP BY s.id ORDER BY s.id DESC',
            ['users_id' => Auth::user()->id]
        );

        $sends_list = $sends_id = [];

        foreach ($query as $k => $v)
        {
            $sends_list[$k] = $v;
            $sends_id[] = $v->sends_id;
        }

        return \view('sends_error', [
            'sends_list' => $sends_list,
        ]);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * resourcesListError
     *
     * @param int $sends_id
     */
    public function resourcesListError(int $sends_id)
    {
        $resources = DB::select(
            'SELECT c.id AS csv_id, sends_id, data, s.name, s.file_data
             FROM public.csv AS c
             LEFT JOIN public.sends AS s ON s.id = c.sends_id
             WHERE c.sends_id = :sends_id
             ORDER BY c.id',
            ['sends_id' => $sends_id]
        );

        // Lignes en erreur
        $data = [];

        foreach ($resources as $k => $v)
        {
            $data[] = [
                'csv_id' => $v->csv_id,
                'sends_id' => $v->sends_id,
                'data' => str_getcsv($v->data),
                'sends_name' => $v->name,
            ];
        }

        // Liste des champs
        $data_file_name = base_path('upload/data/') . $resources[0]->file_data;
        $fields = file_get_contents($data_file_name, false, null, 0, 449);
        $check = file_get_contents($data_file_name, false, null, 450, 45);

        return view('resource_error', [
            'fields' => str_getcsv(str_replace(':', '_', $fields)),
            'check' => str_getcsv($check),
            'resources' => $data,
        ]);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * updateLine
     *
     * @param  Request $request
     */
    public function updateLine(Request $request)
    {
        $csv_id = $request->csv_id;
        $datas = $request->post();
        $data = '';
        unset($datas['csv_id']);

        foreach ($datas as $v)
            $data .= '"' . $v . '",';

        //DB::update('UPDATE public.csv SET data = :data WHERE id = :csv_id', ['data' => substr($data, 0, -1), 'csv_id' => $csv_id]);
        //$pdfc = new ProcessingDataFileController;

        $file = DB::select(
            'SELECT s.file_data, c.sends_id FROM public.csv AS c LEFT JOIN public.sends AS s ON s.id = c.sends_id WHERE c.id = :csv_id',
            ['csv_id' => $request->csv_id]
        )[0];

        $this->replace_car = config('aoroc_nakala.replace_car');
        $this->replace_car_all_fields = config('aoroc_nakala.resource.all_fields.replace_car');
        $before = config('aoroc_nakala.replace_fields.before');
        $after = config('aoroc_nakala.replace_fields.after');
        $data_file_name = base_path('upload/data/') . $file->file_data;
        $fields = file_get_contents($data_file_name, false, null, 0, 449);
        $check = file_get_contents($data_file_name, false, null, 450, 45);
        $fields_name = str_replace($before, $after, $fields);
        $fields = str_getcsv($fields);
        $check = str_getcsv($check);
        $data = str_getcsv(substr($data, 0, -1));

        if (count($check) == count($data))
        {
            $sql_values = '';

            foreach ($data as $a => $b)
            {
                if ($check[$a] === '1') // Check si on est sur un champ obligatoire -> 1 - non obligatoire -> 0
                    if (empty($b)) // Pas de valeur renseignée pour le champ obligatoire : erreur
                        return false;

                if ($a === 1) // Second champ du tableau (l'image)
                    $b = strtolower(str_replace($this->replace_car[0], $this->replace_car[1], $b)); // Remplacement de caractères
                else
                    $b = str_replace($this->replace_car_all_fields[0], $this->replace_car_all_fields[1], $b);

                $sql_values .= '\'' . str_replace('\'', '\'\'', trim($b)) . '\',';
            }

            // Ajout de la ligne dans la table public.resource
            DB::insert(
                'INSERT INTO public.resource (sends_id,' . $fields_name . ')
                 VALUES (' . $file->sends_id . ', ' . substr($sql_values, 0, -1) . ')'
            );

            // Suppression de la ligne de la table public.csv
            DB::delete('DELETE FROM public.csv WHERE id = :csv_id', [$csv_id]);
        }
    }
}
