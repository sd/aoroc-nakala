<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Resource;
use App\Http\Controllers\ResourceErrorController;

class ResourceController extends Controller
{
    private $resources = [];
    private $resources_sha = [];
    private $metas = [];
    private $check_errors = [];
    private $error_message = [];
    private $nakala_url = null;
    private $api_url = null;


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * Infos sur les statuts de ressource
     *
     * -> er : ligne en erreur
     * -> wt : en attente d'envoi sur NAKALA
     * -> sp : en cours d'envoi
     * -> po : posté sur NAKALA - pending
     * -> pu : publié sur NAKALA - published
     */
    private $resource_info_status = [
        'er' => ['text_fr' => 'en erreur',      'text' => 'error',        'color' => 'danger',    'description' => 'ressource(s) en erreur'],
        'wt' => ['text_fr' => 'en attente',     'text' => 'waiting',      'color' => 'secondary', 'description' => 'ressource(s) en attente d\'envoi sur NAKALA'],
        'sp' => ['text_fr' => 'envoi en cours', 'text' => 'sent_process', 'color' => 'warning',   'description' => 'ressource(s) en cours d\'envoi'],
        'po' => ['text_fr' => 'postée',         'text' => 'pending',      'color' => 'info',      'description' => 'ressource(s) postée sur NAKALA'],
        'pu' => ['text_fr' => 'publiée',        'text' => 'published',    'color' => 'success',   'description' => 'ressource(s) publiée sur NAKALA'],
        'up' => ['text_fr' => 'à re-publier',   'text' => 're_publish',   'color' => 'info',      'description' => 'ressource(s) à re-publier sur NAKALA'],
    ];


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->error_message = config('aoroc_nakala.resource.error_message');
        $this->nakala_url = config('aoroc_nakala.api.nakala_url');
        $this->api_url = config('aoroc_nakala.api.api_url');
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * getResourceInfoStatus
     *
     * @return array
     */
    public function getResourceInfoStatus(): array
    {
        return $this->resource_info_status;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * resourceList
     *
     * @param int $sends_id
     * @return Application|Factory|View
     */
    public function resourceList(int $sends_id)
    {
        $ris = $this->resource_info_status;
        $updateable_fields = config('aoroc_nakala.resource.updateable_fields');
        /*
          collection, statut, nakala:license, nakala:type, dcterms:language, dcterms:publisher et dcterms:rights.
        */
        return view(
            'resource',
            [
                'sends_id' => $sends_id,
                'updateable_fields' => $updateable_fields,
                'nakala_url' => $this->nakala_url,
                'resource_list' => DB::select('
                    SELECT r.id, r.nk_title, r.description, r.error, r.type, r.files, r.status, s.project, s.name AS sends_name, r.nakala_id,
                        CASE
                            WHEN r.status = \'wt\' THEN \'' . $ris['wt']['text_fr'].'|'.$ris['wt']['color'] . '\'
                            WHEN r.status = \'sp\' THEN \'' . $ris['sp']['text_fr'].'|'.$ris['sp']['color'] . '\'
                            WHEN r.status = \'po\' THEN \'' . $ris['po']['text_fr'].'|'.$ris['po']['color'] . '\'
                            WHEN r.status = \'pu\' THEN \'' . $ris['pu']['text_fr'].'|'.$ris['pu']['color'] . '\'
                            WHEN r.status = \'up\' THEN \'' . $ris['up']['text_fr'].'|'.$ris['up']['color'] . '\'
                            ELSE \'' . $ris['er']['text_fr'].'|'.$ris['er']['color'] . '\' END
                        AS status_text
                     FROM public.resource AS r
                         RIGHT JOIN sends AS s ON s.id = r.sends_id
                     WHERE sends_id = :sends_id
                         AND s.users_id = :users_id
                     ORDER BY r.id',
                    [
                        'sends_id' => $sends_id,
                        'users_id' => Auth::user()->id
                    ]
                )
            ]
        );
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * resourceDetail
     *
     * @param  int $resource_id
     * @param  string $project
     * @return object
     */
    public function resourceDetail(int $resource_id, string $project): object
    {
        $ris = $this->resource_info_status;

        //"nakala:title","nakala:license","nakala:type","dcterms:publisher","dcterms:type","dcterms:coverage","dcterms:created"
        if ($project === 'nahan')
        {
            // Champs obligatoire
            $fields = 'collection, nk_title, nk_license, nk_type, publisher, type, coverage, created, ';
            // Champs optionnel
            $fields .= 'creator, description, language, relation, rights, source, spatial, subject,
                        temporal_litteraly, temporal_start, temporal_end, contributor';
        }
        else
        {
            // Champs obligatoire
            $fields = 'collection, nk_title, nk_license, nk_type, nk_creator, nk_created, ';
            // Champs optionnel
            $fields .= 'abstract, access_rights, accrual_method, accrual_periodicity, accrual_policy, alternative, audience, available,
                        bibliographic_citation, conforms_to, contributor, coverage, created, creator, date, date_accepted, date_copyrighted,
                        date_submitted, description, education_level, extent, format, has_format, has_part, has_version, identifier,
                        instructional_method, is_format_of, is_part_of, is_referenced_by, is_replaced_by, is_required_by, issued, is_version_of,
                        language, license, mediator, medium, modified, provenance, publisher, reference, relation, replaces, requires,
                        rights, rights_holder, source, spatial, subject, table_of_contents, temporal_litteraly, temporal_start, temporal_end,
                        title, type, valid, same_as';
        }

        // Ne pas changer l'ordre des champs status et error car c'est utilisé dans resource.js : resourceView()
        return DB::select('
            SELECT
                status, error, files,
                CASE
                    WHEN status = \'wt\' THEN \'' . $ris['wt']['text_fr'].'|'.$ris['wt']['color'] . '\'
                    WHEN status = \'sp\' THEN \'' . $ris['sp']['text_fr'].'|'.$ris['sp']['color'] . '\'
                    WHEN status = \'po\' THEN \'' . $ris['po']['text_fr'].'|'.$ris['po']['color'] . '\'
                    WHEN status = \'pu\' THEN \'' . $ris['pu']['text_fr'].'|'.$ris['pu']['color'] . '\'
                    WHEN status = \'up\' THEN \'' . $ris['up']['text_fr'].'|'.$ris['up']['color'] . '\'
                    ELSE \'' . $ris['er']['text_fr'].'|'.$ris['er']['color'] . '\'
                END AS status_text, ' . $fields . ',
                CASE
                    WHEN nakala_id != \'\' THEN \' ' . $this->nakala_url . '\'
                    ELSE \'\'
                END AS nakala_url,
                CASE
                    WHEN nakala_id != \'\' THEN \' ' . asset('images/nakala.png') . '\'
                    ELSE \'\'
                END AS nakala_image,
                nakala_id, 
                ' . $fields . '
            FROM public.resource AS r WHERE id = :resource_id',
            ['resource_id' => $resource_id]
        )[0];
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * resourceUpdate
     *
     * @param  int $resource_id
     * @param  string $field
     * @param  string $value
     * @return bool
     */
    public function resourceUpdate(int $resource_id, string $field, string $value): bool
    {
        $value = ($value !== '-') ? $value : '';

        // Reset de l'erreur si existante
        ResourceErrorController::checkErrorToReset($resource_id, $field);

        // Reset de l'erreur curl si existante
        ResourceErrorController::checkErrorToReset($resource_id, 'curl');
        ResourceErrorController::checkErrorToReset($resource_id, 'error');

        // Si ID nakala déjà présent on passe le statut en "up"
        $nakala_id = DB::table('public.resource')->select('nakala_id')->where('id', '=', $resource_id)->get()[0]->nakala_id;
        $update_list = '';

        if (!empty($nakala_id))
        {
            $f = DB::select(
                'SELECT update_fields FROM public.resource WHERE id = :resource_id AND update_fields LIKE :field',
                ['resource_id' => $resource_id, 'field' => '%' . $field . '%']
            );

            $update_fields = '';

            if (empty($f)) // Check si le champ "$field" est déjà présent dans resource.update_fields
            {
                $update_fields = ', update_fields = CONCAT(update_fields, :update_fields::VARCHAR)';
                $data['update_fields'] = $field . '|';
            }

            $update_list = ($field !== 'status')
                ? ', status = \'up\'' . $update_fields
                : $update_fields;
        }

        $str_replace = [['&quot;', '&#47;', '\'', '""'], ['"', '/', '’', '"']];
        $data['value'] = trim(urldecode(str_replace($str_replace[0], $str_replace[1], $value)));
        $data['resource_id'] = $resource_id;

        DB::update('UPDATE public.resource SET ' . $field . ' = :value ' . $update_list . ' WHERE id = :resource_id', $data);

        return true;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * resourceUpdate
     *
     * @param  Request $request
     * @return void
     */
    public function resourceUpdateAllLine(Request $request): void
    {
        $resources = DB::select('SELECT id FROM public.resource WHERE sends_id = :sends_id', ['sends_id' => $request->sends_id]);

        foreach ($resources as $resource)
            $this->resourceUpdate($resource->id, $request->field, $request->value);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * resourceMultiUpdate
     *
     * @param  int $resource_id
     * @param  array $fields_values
     * @return bool
     */
    public function resourceMultiUpdate(int $resource_id, array $fields_values): bool
    {
        $func = function($val): string
        {
            $str_replace = [['&quot;', '&#47;', 'Le'], ['"', '/', 'XX']];
            return urldecode(str_replace($str_replace[0], $str_replace[1], $val));
        };

        $fields_list = '';

        foreach ($fields_values as $k => $v)
            $fields_list .= $k . ' = :' . $k . ', ';

        $fields_values['resource_id'] = $resource_id;

        DB::update(
            'UPDATE public.resource SET ' . substr($fields_list, 0, -2) . ' WHERE id = :resource_id',
            array_map($func, $fields_values)
        );

        return true;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * adminResourceList
     *
     * @param int $sends_id
     * @return Application|Factory|View
     */
    public function adminResourceList(int $sends_id)
    {
        $ris = $this->resource_info_status;

        return view(
            'admin.resource',
            [
                'sends_id' => $sends_id,
                'resource_list' => DB::select('
                    SELECT r.id, r.nk_title, r.description, r.error, r.type, r.files, r.status, s.project, s.name AS sends_name,
                        CASE
                            WHEN r.status = \'wt\' THEN \'' . $ris['wt']['text_fr'].'|'.$ris['wt']['color'] . '\'
                            WHEN r.status = \'sp\' THEN \'' . $ris['sp']['text_fr'].'|'.$ris['sp']['color'] . '\'
                            WHEN r.status = \'po\' THEN \'' . $ris['po']['text_fr'].'|'.$ris['po']['color'] . '\'
                            WHEN r.status = \'pu\' THEN \'' . $ris['pu']['text_fr'].'|'.$ris['pu']['color'] . '\'
                            WHEN r.status = \'up\' THEN \'' . $ris['up']['text_fr'].'|'.$ris['up']['color'] . '\'
                            ELSE \'' . $ris['er']['text_fr'].'|'.$ris['er']['color'] . '\' END
                        AS status_text
                     FROM public.resource AS r
                         RIGHT JOIN sends AS s ON s.id = r.sends_id
                     WHERE sends_id = :sends_id
                     ORDER BY r.id',
                    ['sends_id' => $sends_id]
                )
            ]
        );
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * getResourcesForUpdateStatusOnNakala
     *
     * @param int $sends_id
     * @return void
     */
    public function getResourcesForUpdateStatusOnNakala(int $sends_id): void
    {
        $resources = DB::select('SELECT id, nakala_id FROM public.resource WHERE sends_id = :sends_id', ['sends_id' => $sends_id]);

        foreach ($resources as $resource)
            $this->resourceUpdateStatusOnNakala($resource);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * resourceUpdateStatusOnNakala
     *
     * @param object $resource
     * @return void
     */
    private function resourceUpdateStatusOnNakala(object $resource): void
    {
        $curl = 'curl \
              -X PUT "' . $this->api_url . '/datas/' . $resource->nakala_id . '/status/published" \
              -H "accept: application/json" \
              -H "X-API-KEY: ' . Auth::user()->api_key . '" \
              -H "Content-Type: application/json"';

        @exec($curl, $output, $result);

        Log::channel('curl')->info('[DATA US - curl] => ' . $curl);
        Log::channel('curl')->info('[DATA US - output] => ' . json_encode($output));
        Log::channel('curl')->info('[DATA US - result] => ' . $result);
        Log::channel('curl')->info('========================================================================================================================================');

        if ($result > 0) // Erreur curl
            ResourceErrorController::saveErrorNakala($resource->id, 'erreur curl - code ' . $result);
        else
        {
            if (!empty($output) and (count($output) > 0))
            {
                $output = json_decode($output[0]);

                if ($output->code !== 201)
                {
                    Log::info('ResourceSendController::sendResourceToNakalaForUpdate() : error - resource_id = ' . $resource->id);
                    ResourceErrorController::saveErrorNakala($resource->id, '', $output);
                }
            }
            else
                DB::update('UPDATE public.resource SET status = \'pu\' WHERE id = :resource_id', ['resource_id' => $resource->id]);
        }
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * setSendsStatus
     *
     * @desc Passe le statut d'une resource à "wt"
     * @param int $resource_id
     */
    public function updateResourceStatus(int $resource_id)
    {
        DB::update(
            'UPDATE public.resource SET status = \'wt\' WHERE id = :resource_id',
            ['resource_id' => $resource_id]
        );
    }
}
