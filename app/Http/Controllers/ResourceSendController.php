<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Resource;
use App\Http\Controllers\ResourceErrorController;
use App\Http\Controllers\ResourceController;
use App\Http\Controllers\DeleteController;
use App\Http\Controllers\CheckResourceController;

class ResourceSendController extends Controller
{
    private $resources = [];
    private $resources_sha = [];
    private $metas = [];
    private $check_errors = [];
    private $error_message = [];
    private $resource_controller = null;
    private $check_resource_controller = null;
    private $api_url = null;


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     *
     */
    public function __construct()
    {
        $this->resource_controller = new ResourceController;
        $this->check_resource_controller = new CheckResourceController;
        $this->api_url = config('aoroc_nakala.api.api_url');
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * getResourceToSend
     *
     * @param  int $sends_id
     * @return bool
     */
    public function getResourceToSend(int $sends_id): bool
    {
        $this->resources = DB::select('
            SELECT r.id AS resource_id, r.*, s.project, s.sends_status_users
            FROM public.resource AS r
                LEFT JOIN public.sends AS s ON r.sends_id = s.id
            WHERE (r.status = \'wt\' OR r.status = \'up\' OR r.status = \'po\')
                AND s.users_id = :users_id
                AND r.sends_id = :sends_id
            ORDER BY r.status DESC, r.id ASC',
            ['users_id' => Auth::user()->id, 'sends_id' => $sends_id]
        );

        if (count($this->resources) > 0)
        {
            $resources = [];

            foreach ($this->resources as $data)
                $resources[$data->status][] = $data;

            $this->resources = $resources;
            $n_wt = $n_up = '0';

            Log::info('ResourceSendController::getResourceToSend() : ================================================================================================================');

            if (!empty($resources['wt']))
            {
                $n_wt = count($resources['wt']);
                Log::info('ResourceSendController::getResourceToSend() : users_id = ' . Auth::user()->id . ' - ' . $n_wt . ' ressources a envoyer');
                return $this->sendResource();
            }

            if (!empty($resources['up']))
            {
                $n_up = count($resources['up']);
                Log::info('ResourceSendController::getResourceToSend() : users_id = ' . Auth::user()->id . ' - ' . $n_up . ' ressources a mettre a jour');
                return $this->updateResource();
            }
        }
        else
        {
            Log::info('ResourceSendController::getResourceToSend() : pas de ressources a envoyer');
            return false;
        }
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * sendResource
     *
     * @return bool
     */
    private function sendResource(): bool
    {
        foreach ($this->resources['wt'] as $resource) // Boucle principale d'envoi des ressources - exécution du code pour chaque ressource à envoyer
        {
            Log::info('ResourceSendController::sendResource() : resource_id = ' . $resource->resource_id);
            $this->resource_controller->resourceUpdate($resource->resource_id, 'status', 'sp'); // Passe les ressources en statut "sp" pour éviter un envoi en double

            if ($this->sendResourceFileToNakala($resource)) // Envoi du/des fichiers de la ressource
                $this->makeJsonResource($resource); // Création du JSON de la ressource : ne pas utiliser la valeur de retour sinon sortie de boucle
        }

        return false;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * updateResource
     *
     * @return bool
     */
    private function updateResource(): bool
    {
        foreach ($this->resources['up'] as $resource) // Boucle principale d'envoi des resources à mettre à jour
        {
            Log::info('ResourceSendController::updateResource() : resource_id = ' . $resource->resource_id);
            $this->prepareDataToUpdate($resource);
        }

        return true;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * updateResource
     *
     * @param object $resource
     * @return void
     */
    private function prepareDataToUpdate(object $resource)
    {
        //Log::info('ResourceSendController::prepareDataToUpdate() : resource_id = ' . $resource->update_fields);
        $update_fields = explode('|', substr($resource->update_fields, 0, -1));

        if (($key = array_search('files', $update_fields)) !== false) // Envoi des fichiers si nécessaire
        {
            unset($update_fields[$key]);

            if (!$this->sendResourceFileToNakala($resource))
                exit;
        }

        // Champs obligatoire
        foreach ($update_fields as $field)
            Log::info('ResourceSendController::prepareDataToUpdate() : field = ' . $field);

        $this->makeJsonResource($resource, true); // Création du JSON de la ressource : ne pas utiliser la valeur de retour sinon sortie de boucle
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * sendResourceFileToNakala
     *
     * @desc   Envoi du/des fichiers média vers NAKALA et réception du SHA1 retourné
     * @param  object $resource
     * @return bool
     */
    private function sendResourceFileToNakala(object $resource): bool
    {
        $file = explode(';', $resource->files);

        if (empty($file))
        {
            $this->check_errors['file'] = 'fichier média manquant';
            ResourceErrorController::saveError($resource->resource_id, $this->check_errors);
            return false;
        }

        foreach ($file as $k => $v)
        {
            Log::info('ResourceSendController::sendResourceFileToNakala() : files = ' . $resource->sends_id . '/' . $v);

            $curl = 'curl \
                  -X POST "' . $this->api_url . '/datas/uploads' . '" \
                  -H "accept: application/json" \
                  -H "X-API-KEY: ' . Auth::user()->api_key . '" \
                  -H "Content-Type: multipart/form-data" \
                  -F "file=@' . base_path('upload/media') . '/' . $resource->sends_id . '/' . $v . ';type=image/jpeg"';

            @exec($curl, $output, $result);

            Log::channel('curl')->info('[FILE - curl] => ' . $curl);
            Log::channel('curl')->info('[FILE - output] => ' . json_encode($output));
            Log::channel('curl')->info('[FILE - result] => ' . $result);
            Log::channel('curl')->info('------------------------------------------------------------------------------------------------------------------------------------');

            // TODO : prendre en compe le cas ou $output n'est pas un tableau
            $sha1 = @json_decode($output[0])->sha1;

            if (strlen($sha1) !== 40) // Marque la ressource en erreur si la chaine sha1 n'est par = à 40
            {
                $this->check_errors['file'] = 'fichier non envoyé';
                ResourceErrorController::saveError($resource->resource_id, $this->check_errors);

                return false;
            }

            $this->resources_sha[$resource->resource_id][$k]['file'] = [
                'sha1' => $sha1,
                'name' => $v,
                //'description' => $v, // Optionnel
            ];

            Log::info('ResourceSendController::sendResourceFileToNakala() : sha1 = ' . $sha1);
            unset($output);
        }

        return true;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * makeJsonResource
     *
     * @desc   Création du JSON de la ressource
     * @param  object $resource
     * @param  bool $update
     * @return void
     */
    private function makeJsonResource(object $resource, bool $update = false): void
    {
        $data = [];

        if (! $update) // Ajout d'une ressource
        {
            // Création du tableau de données brutes
            foreach ($resource as $k1 => $v1)
                $data[$resource->resource_id][$k1] = $v1;

            // Création du tableau de données
            $this->check_resource_controller->checkMetas($data[$resource->resource_id]);

            //dd($data[$resource->resource_id]);

            // statut posté : pending -> seulement pour une collection privée
            // statut publié : published
            $status = ($data[$resource->resource_id]['sends_status_users'] === 'published') ? 'pu' : 'po';

            $array_resource = [
                'collectionsIds' => $this->check_resource_controller->checkCollection($data[$resource->resource_id]['collection']),
                'files' => $this->check_resource_controller->checkFiles($this->resources_sha[$resource->resource_id]), // $resource->resource_id
                'metas' => $this->check_resource_controller->getMetas(),
                'status' => $data[$resource->resource_id]['sends_status_users'],
                //'rights' => [''],
            ];

            $this->sendResourceToNakala($array_resource, $resource->resource_id, $status); // Envoi d'une ressource vers NAKALA
            $this->check_resource_controller->unsetMetas();
        }
        else // Mise à jour d'une ressource
        {
            // Si SHA1 présent, on l'ajoute au tableau
            if (!empty($this->resources_sha[$resource->resource_id]))
                $array_resource['files'] = $this->check_resource_controller->checkFiles($this->resources_sha[$resource->resource_id]);

            $this->check_resource_controller->checkMetas((array)$resource);

            // Si collection présente, on ajoute au tableau
            if (!empty($resource->collection))
                $array_resource['collectionsIds'] = $this->check_resource_controller->checkCollection($resource->collection);

            $array_resource['metas'] = $this->check_resource_controller->getMetas();

            // Mise à jour d'une ressource sur NAKALA
            $this->sendResourceToNakalaForUpdate($array_resource, $resource->resource_id, $resource->nakala_id);
            $this->check_resource_controller->unsetMetas();
        }

        // Check des erreurs
        if (!empty($this->check_errors))
        {
            ResourceErrorController::saveError($resource->resource_id, $this->check_errors);
            exit;
        }

        //$metas = [];
        $this->check_errors = [];
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * sendResourceToNakala
     *
     * @desc   Envoi d'une ressource vers NAKALA
     * @param  array $array_resource
     * @param  int $resource_id
     * @param  string $status
     * @return void
     */
    private function sendResourceToNakala(array $array_resource, int $resource_id, string $status): void
    {
        $curl = 'curl \
              -X POST "' . $this->api_url . '/datas' . '" \
              -H "accept: application/json" \
              -H "X-API-KEY: ' . Auth::user()->api_key . '" \
              -H "Content-Type: application/json" \
              -d \'' . json_encode($array_resource, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . '\'';

        @exec($curl, $output, $result);

        Log::channel('curl')->info('[DATA I - curl] => ' . $curl);
        Log::channel('curl')->info('[DATA I - output] => ' . json_encode($output));
        Log::channel('curl')->info('[DATA I - result] => ' . $result);
        Log::channel('curl')->info('========================================================================================================================================');

        if ($result > 0) // Erreur curl
            ResourceErrorController::saveErrorNakala($resource_id, 'erreur curl - code ' . $result);
        else
        {
            if (is_array($output) and (! empty($output[0])))
                $output = json_decode($output[0]);
            else
                Log::channel('curl')->info('[DATA I - output err] => ' . json_encode($output));

            if (empty($output->code) or $output->code !== 201)
            {
                // Codes possible :
                // 401 : Erreur d'authentification (mauvaise clé d'API) ou compte utilisateur inexistant
                // 422 : La donnée ne contient pas les données obligatoires
                // 500 : Erreur lors de l'enregistrement de la donnée
                Log::info('ResourceSendController::sendResourceToNakala() : error - resource_id = ' . $resource_id);
                Log::info('[OUTPUT] => ' . json_encode($output));
                //ResourceErrorController::saveErrorNakala($resource_id, '', $output);
            }
            else
                $this->saveIdNakala($resource_id, $output->payload->id, $status);
        }
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * saveIdNakala
     *
     * @desc   Sauvegarde de l'ID retourné par l'API NAKALA et modif du statut en "pu" (published)
     * @param  int $resource_id
     * @param  string $curl_result
     * @param  string $status
     * @return void
     */
    private function saveIdNakala(int $resource_id, string $nakala_id, string $status): void
    {
        Log::info('ResourceSendController::saveIdNakala() : success - $resource_id = ' . $resource_id . ' - nakala_id = ' . $nakala_id);
        $this->resource_controller->resourceMultiUpdate($resource_id, ['nakala_id' => $nakala_id, 'status' => $status]);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * sendResourceToNakalaForUpdate
     *
     * @desc   Envoi d'une ressource vers NAKALA pour mise à jour
     * @param  array $array_resource
     * @param  int $resource_id
     * @param  string $nakala_id
     * @return void
     */
    private function sendResourceToNakalaForUpdate(array $array_resource, int $resource_id, string $nakala_id): void
    {
        $curl = 'curl \
              -X PUT "' . $this->api_url . '/datas/' . $nakala_id . '" \
              -H "accept: application/json" \
              -H "X-API-KEY: ' . Auth::user()->api_key . '" \
              -H "Content-Type: application/json" \
              -d \'' . json_encode($array_resource, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . '\'';

        @exec($curl, $output, $result);

        Log::channel('curl')->info('[DATA U - curl] => ' . $curl);
        Log::channel('curl')->info('[DATA U - output] => ' . json_encode($output));
        Log::channel('curl')->info('[DATA U - result] => ' . $result);
        Log::channel('curl')->info('========================================================================================================================================');

        if ($result > 0) // Erreur curl
            ResourceErrorController::saveErrorNakala($resource_id, 'erreur curl - code ' . $result);
        else
        {
            if (!empty($output) and (count($output) > 0))
            {
                $output = json_decode($output[0]);

                if ($output->code !== 201)
                {
                    Log::info('ResourceSendController::sendResourceToNakalaForUpdate() : error - resource_id = ' . $resource_id);
                    ResourceErrorController::saveErrorNakala($resource_id, '', $output);
                }
            }
            else
                DB::update('UPDATE public.resource SET status = \'pu\', update_fields = \'\' WHERE id = :resource_id', ['resource_id' => $resource_id]);
        }
    }


    // ###############################################################################################################################################


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * deleteResourceFromNakala
     *
     * @desc Suppression d'une ressource présente sur NAKALA
     * @param  int $sends_id
     * @return bool
     */
    public function deleteResourceFromNakala(int $sends_id): bool
    {
        $resources = DB::select('
            SELECT r.id AS resource_id, r.nakala_id
            FROM public.resource AS r
                LEFT JOIN public.sends AS s ON r.sends_id = s.id
            WHERE r.status = \'po\'
                AND s.users_id = :users_id
                AND r.sends_id = :sends_id
                AND s.sends_status_users = \'pending\'',
            ['users_id' => Auth::user()->id, 'sends_id' => $sends_id]
        );

        if (count($resources) > 0)
            foreach ($resources as $resource)
                $this->deleteResourceAPI($sends_id, $resource);

        return true;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * deleteResourceAPI
     *
     * @desc   Suppression de la ressource de NAKALA via l'API
     * @param  int $sends_id
     * @param  array $resource
     * @return void
     */
    private function deleteResourceAPI(int $sends_id, object $resource): void
    {
        $curl = 'curl \
              -X DELETE "' . $this->api_url . '/datas/' . $resource->nakala_id . '" \
              -H "accept: application/json" \
              -H "X-API-KEY: ' . Auth::user()->api_key . '" \
              -H "Content-Type: application/json"';

        @exec($curl, $output, $result);

        Log::channel('curl')->info('[DATA D - curl] => ' . $curl);
        Log::channel('curl')->info('[DATA D - output] => ' . json_encode($output));
        Log::channel('curl')->info('[DATA D - result] => ' . $result);
        Log::channel('curl')->info('========================================================================================================================================');

        if ($result > 0) // Erreur curl
            ResourceErrorController::saveErrorNakala($resource->resource_id, 'erreur curl - code ' . $result);
        else
        {
            if (!empty($output) and is_array($output))
            {
                $output = json_decode($output[0]);

                if ($output->code === 204)
                {
                    // API pourrie qui devrait retourner le code 204 mais ne renvoi évidemment que dalle !!!
                    Log::info('ResourceSendController::deleteResourceAPI() : [204] ok - resource_id = ' . $resource->resource_id);
                }
                else
                {
                    // Codes possible :
                    // 401 : Erreur d'authentification (mauvaise clé d'API) ou compte utilisateur inexistant
                    // 403 : Droit sur la donnée insuffisant
                    // 404 : La donnée n'existe pas
                    Log::info('ResourceSendController::deleteResourceAPI() : error - resource_id = ' . $resource->resource_id);
                    ResourceErrorController::saveErrorNakala($resource->resource_id, '', $output);
                }
            }
            else // Suppression de la ressource dans la base
            {
                $delete = new DeleteController;
                $delete->resourceDelete($sends_id, $resource->resource_id);
            }
        }
    }
}
