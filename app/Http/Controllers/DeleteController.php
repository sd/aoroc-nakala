<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class DeleteController extends Controller
{
    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * resourceDelete
     *
     * @param  int $sends_id
     * @param  int $resource_id
     * @return int
     */
    public function resourceDelete(int $sends_id, int $resource_id): int
    {
        // Suppression du / des fichiers associés à la ressource
        $this->mediaDelete($sends_id, $resource_id);

        // Suppression des données
        DB::delete('DELETE FROM public.resource WHERE id = :resource_id', ['resource_id' => $resource_id]);

        // Check si l'envoi contient encore des ressources et si ce n'est pas le cas, suppression de celui-ci
        $nb_resources = DB::select('SELECT COUNT(*) AS n FROM public.resource WHERE sends_id = :sends_id', ['sends_id' => $sends_id])[0]->n;

        if ($nb_resources === 0)
            $this->sendsDelete($sends_id);

        return $nb_resources;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * mediaDelete
     *
     * @desc   Suppression du / des fichiers associés à une ressource
     * @param  int $sends_id
     * @param  int $resource_id
     * @param  string $files
     * @return void
     */
    public function mediaDelete(int $sends_id, int $resource_id, string $files = ''): void
    {
        if ($files === '')
            $files = DB::select('SELECT files FROM public.resource WHERE id = :resource_id', ['resource_id' => $resource_id])[0]->files;

        $f = explode(';', $files);

        foreach ($f as $file)
        {
            $media_file = base_path('upload/media') . '/' . $sends_id . '/' . $file;

            if (file_exists($media_file))
                unlink($media_file);
        }
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * sendsDelete
     *
     * @param  int $sends_id
     * @return void
     */
    public function sendsDelete(int $sends_id): void
    {
        // Suppression des données
        DB::delete('DELETE FROM public.sends WHERE id = :sends_id', ['sends_id' => $sends_id]);

        // Suppression du / des fichiers associés à la ressource
        rrmdir(base_path('upload/media') . '/' . $sends_id);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * purgeMediaFiles
     *
     * @desc   Purge des fichiers ayant besoin de l'être
     * @return void
     */
    public function purgeMediaFiles()
    {
        $result = DB::select('
            SELECT sends_id, id AS resource_id, files, updated_at
            FROM public.resource
            WHERE status = \'pu\'
            AND updated_at < DATE(NOW()) - interval \'' . config('aoroc_nakala.media.purge.nb_days') . ' days\''
        );

        $purge_status = config('aoroc_nakala.media.purge.active');

        echo '==============================================================================' . PHP_EOL;
        echo ' Liste des fichiers qui seront purgés :' . PHP_EOL;
        echo '==============================================================================' . PHP_EOL;

        if (empty($result))
            echo PHP_EOL . ' Pas de fichiers à purger...' . PHP_EOL . PHP_EOL;

        foreach ($result as $r)
        {
            echo ' -=> [ ' . $r->updated_at . ' ] ' . $r->sends_id . ' / '. $r->resource_id . ' / ' . $r->files . PHP_EOL;

            if ($purge_status)
                $this->mediaDelete($r->sends_id, $r->resource_id, $r->files);
        }

        echo '==============================================================================' . PHP_EOL . PHP_EOL;
    }
}
