<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CheckResourceController extends Controller
{
    private array $metas = [];
    private array $check_errors = [];


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     *
     * @return array
     */
    public function getMetas() : array
    {
        return $this->metas;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     *
     * @return void
     */
    public function unsetMetas() : void
    {
        unset($this->metas);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     *
     * @return array
     */
    public function getErrors() : array
    {
        return $this->check_errors;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkCollection
     *
     * @desc   Retourne la/les collections
     * @param  string $collection
     * @return array
     */
    public function checkCollection(string $collection) : array
    {
        if (! empty($collection))
            return explode(';', $collection);
        else
            $this->check_errors['collection'] = 'collection manquante';

        return [];
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkNakalaLicense
     *
     * @desc   Retourne la licence (NAKALA)
     * @param  string $nk_license
     * @return void
     */
    public function checkNakalaLicense(string $nk_license) : void
    {
        if (! empty($nk_license))
            $this->metas[] = [
                'value'       => $nk_license,
                'propertyUri' => 'http://nakala.fr/terms#license',
                'typeUri'     => 'http://www.w3.org/2001/XMLSchema#string',
            ];
        else
            $this->check_errors['nk_license'] = 'Licence non renseignée';
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkNakalaType
     *
     * @desc   Retourne le type (NAKALA)
     * @param  string $nk_type
     * @return void
     */
    public function checkNakalaType(string $nk_type) : void
    {
        if (!empty($nk_type))
            $this->metas[] = [
                'value'       => $nk_type,
                'propertyUri' => 'http://nakala.fr/terms#type',
                'typeUri'     => 'http://www.w3.org/2001/XMLSchema#anyURI',
            ];
        else
            $this->check_errors['nk_type'] = 'Type non renseigné';
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkNakalaCreated
     *
     * @desc   Retourne la date de création (NAKALA)
     *         Date sous la forme année, année-mois ou année-mois-jour. Indiquez null si la date de création est inconnue
     * @param  string $nk_created
     * @return void
     */
    public function checkNakalaCreated(string $nk_created) : void
    {
        if (empty($nk_created))
            $this->metas[] = [
                'value'       => null,
                'lang'        => null,
                'propertyUri' => 'http://nakala.fr/terms#created',
                'typeUri'     => null,
            ];
        else
            $this->metas[] = [
                'value'       => $nk_created,
                'lang'        => null,
                'propertyUri' => 'http://nakala.fr/terms#created',
                'typeUri'     => 'http://www.w3.org/2001/XMLSchema#string',
            ];
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkNakalaCreator
     *
     * @desc   Retourne les infos sur le créateur (NAKALA)
     * @param  string $nk_creator
     * @return void
     */
    public function checkNakalaCreator(string $nk_creator) : void // TODO : code à revoir car peut être multiple
    {
        if (empty($nk_creator))
            $data = [
                'value' => null,
                'lang' => null,
                'propertyUri' => 'http://nakala.fr/terms#creator',
                'typeUri' => null,
            ];
        else
            $data = [
                'value' => [
                    'givenname' => $nk_creator,
                    'surname' => '',
                    'orcid' => null
                ],
                'lang' => null,
                'propertyUri' => 'http://nakala.fr/terms#creator',
                'typeUri' => 'http://www.w3.org/2001/XMLSchema#string',
            ];

        $this->metas[] = $data;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkTemporal
     *
     * @desc   Retourne les infos pour le champ temporal avec temporal_start et temporal_end
     * @param  string $temporal_litteraly
     * @param  string $temporal_start
     * @param  string $temporal_end
     * @return void
     */
    public function checkTemporal(string $temporal_litteraly = '', string $temporal_start = '', string $temporal_end = '') : void
    {
        if (! empty($temporal_litteraly) or ! empty($temporal_start) or ! empty($temporal_end))
        {
            $temporal = '';

            if (! empty($temporal_litteraly))
                $temporal .= $temporal_litteraly;

            $temporal .= ';';

            if ( !empty($temporal_start))
                $temporal .= 'start=' . $temporal_start;

            $temporal .= ';';

            if (! empty($temporal_end))
                $temporal .= 'end=' . $temporal_end;

            $this->metas[] = [
                'value'       => $temporal,
                'lang'        => null,
                'propertyUri' => 'http://purl.org/dc/terms/temporal',
                'typeUri'     => 'http://www.w3.org/2001/XMLSchema#string',
            ];
        }
        /*else
        {
            $this->metas[] = [
                'value'       => null,
                'lang'        => null,
                'propertyUri' => 'http://purl.org/dc/terms/temporal',
                'typeUri'     => null,
            ];
        }*/

    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkFiles
     *
     * @desc   Retourne les infos sur le/les fichiers sous forme de tableau avec ajout du SHA1
     * @param  array $resources_sha
     * @return array
     */
    public function checkFiles(array $resources_sha): array
    {
        $files = [];

        foreach ($resources_sha as $v)
        {
            $files[] = [
                'sha1' => $v['file']['sha1'],
                'name' => $v['file']['name'],
                'embargoed' => date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))),
                //'description' => $v['file']['description'], // Optionnel
            ];
        }

        return $files;
    }


    // ###############################################################################################################################################


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkMetas
     *
     * @desc   Préparation des datas pour les metas
     * @param  array $resource
     * @return void
     */
    public function checkMetas(array $resource) : void
    {
        // Champs obligatoire NAKALA
        $this->checkNakalaLicense($resource['nk_license']);
        $this->checkNakalaType($resource['nk_type']);
        $this->checkNakalaCreated($resource['nk_created'] ?: '');
        $this->checkNakalaCreator($resource['nk_creator'] ?: '');


        // Récupération des données et des éventuelles erreurs
        $this->metas = $this->getMetas();
        $this->check_errors = $this->getErrors();
        $this->checkUniqueMeta($resource['nk_title'], 'http://nakala.fr/terms#title', 'http://www.w3.org/2001/XMLSchema#string', null, true);
        $this->checkOtherMetas($resource);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkUniqueMeta
     *
     * @desc
     * @param  string $value
     * @param  string $property_uri
     * @param  string $type_uri
     * @param  string $lang
     * @param  bool $required
     * @return void
     */
    public function checkUniqueMeta(string $value, string $property_uri, string $type_uri, string $lang = null, bool $required = false) : void
    {
        if (! empty($value))
        {
            $this->metas[] = [
                'value'       => $value,
                'propertyUri' => $property_uri,
                'typeUri'     => $type_uri,
                'lang'        => $lang,
            ];
        }
        elseif ($required and ! empty($property_uri)) // Cette étape est lue seulement si le champ est requis et que $value est manquant
            $this->setErrorMeta($property_uri);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkMultipleMeta
     *
     * @desc
     * @param  string $value
     * @param  string $property_uri
     * @param  string $type_uri
     * @param  string $lang
     * @param  bool $required
     * @return void
     */
    public function checkMultipleMeta(string $value, string $property_uri, string $type_uri, string $lang = null, bool $required = false) : void
    {
        if (! empty($value))
        {
            $s = explode(';', $value);

            foreach ($s as $v)
            {
                $this->metas[] = [
                    'value'       => $v,
                    'propertyUri' => $property_uri,
                    'typeUri'     => $type_uri,
                    'lang'        => $lang,
                ];
            }
        }
        elseif ($required and ! empty($property_uri))
            $this->setErrorMeta($property_uri);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkAllMetasUpdate
     *
     * @desc   Prapération des datas pour les metas pour AUTRE
     * @param  array $resource
     * @return void
     */
    public function checkOtherMetas(array $resource) : void
    {
        // --- Valeurs restant à gérer ---------------------------------------------------------------------------------------------------------------
        // title              | nom de l'objet - voir valeur nakala -> ATTENTION car cette valeur est aussi la collection. Infos vue en récupérant les données
        //                    | d'une collection : https://apitest.nakala.fr/collections/10.34847/nkl.f9cd561m
        // -------------------------------------------------------------------------------------------------------------------------------------------
        // sameAs             | truc introuvable... y qu'a mettre null
        // -------------------------------------------------------------------------------------------------------------------------------------------

        // Si le projet est "autre" les 3 champs ci-dessous passent en non obligatoire
        if ($resource['project'] === 'autre')
        {
            $coverage = false;
            $publisher = false;
            $type = false;
        }
        else
        {
            $coverage = true;
            $publisher = true;
            $type = true;
        }

        unset($resource['project'], $resource['resource_id'], $resource['nakala_id']);

        $property_uri = 'http://purl.org/dc/terms/';
        $type_uri = 'http://www.w3.org/2001/XMLSchema#';

        //  key                      => [property_uri,                            type_uri,             lang, required, unique|multiple]
        $fields_list = [
            'abstract'               => [$property_uri . 'abstract',              $type_uri . 'string', null, false, 'u'], // résumé du contenu de la ressource sous forme de texte libre. préciser la langue du résumé.
            'access_rights'          => [$property_uri . 'accessRights',          $type_uri . 'string', null, false, 'u'], // information sur les conditions d'accès à la ressource ou sur son statut en terme de sécurité. par exemple: "Librement accessible" ou "Soumis à embargo en raison de...
            'accrual_method'         => [$property_uri . 'accrualMethod',         $type_uri . 'string', null, false, 'u'], // valeurs possibles : Deposit - Donation - Purchase - Loan - License - Item Creation : https://www.dublincore.org/specifications/dublin-core/collection-description/accrual-method/
            'accrual_periodicity'    => [$property_uri . 'accrualPeriodicity',    $type_uri . 'string', null, false, 'u'], // valeurs possibles : Triennial - Biennial - Annual - Semiannual - Three times a year - Quarterly - Bimonthly - Monthly - Semimonthly - Biweekly - Three times a month - Weekly - Semiweekly - Three times a week - Daily - Continuous - Irregular : https://www.dublincore.org/specifications/dublin-core/collection-description/frequency/
            'accrual_policy'         => [$property_uri . 'accrualPolicy',         $type_uri . 'string', null, false, 'u'], // valeurs possibles : Closed - Passive - Active - Partial : https://www.dublincore.org/specifications/dublin-core/collection-description/accrual-policy/
            'alternative'            => [$property_uri . 'alternative',           $type_uri . 'string', null, false, 'u'], // titre secondaire, titre abrégé ou autre nom donné à la ressource
            'audience'               => [$property_uri . 'audience',              $type_uri . 'string', null, false, 'u'], // type de population pour laquelle la ressource est destinée ou utile
            'available'              => [$property_uri . 'available',             $type_uri . 'string', null, false, 'u'], // a quelle date la ressource est ou deviendra disponible
            'bibliographic_citation' => [$property_uri . 'bibliographicCitation', $type_uri . 'string', null, false, 'm'], // une référence bibliographique pour la ressource
            'conforms_to'            => [$property_uri . 'conformsTo',            $type_uri . 'string', null, false, 'm'], // le standard, la norme ou les spécifications formelles auxquels la ressource décrite est conforme. si possible, indiquez son identifiant (DOI, ISBN...), une forme de citation, son nom ou une description
            'contributor'            => [$property_uri . 'contributor',           $type_uri . 'string', null, false, 'm'], // entité ayant contribué à l'élaboration du contenu de la ressource (individu, institution, organisation, service), autre que l'entité identifiée dans nakala:creator ou alors en précisant la nature de sa contribution
            'coverage'               => [$property_uri . 'coverage',              'http://purl.org/dc/terms/ISO3166', null, $coverage, 'm'], // code pays sur 2 lettres en majuscules
            'created'                => [$property_uri . 'created',               $type_uri . 'string', null, true,  'u'], // période de dates
            'creator'                => [$property_uri . 'creator',               $type_uri . 'string', null, false, 'm'], // auteur - voir valeur nakala
            // ---------------------------------------------------------------------------------------------------------------------------------------
            'date'                   => [$property_uri . 'date',                  $type_uri . 'string', null, false, 'u'], // date dans le cycle de vie de la ressource
            'date_accepted'          => [$property_uri . 'dateAccepted',          $type_uri . 'string', null, false, 'u'], // date d'acceptation de la ressource
            'date_copyrighted'       => [$property_uri . 'dateCopyrighted',       $type_uri . 'string', null, false, 'u'], // date de dépôt des droits d'auteur
            'date_submitted'         => [$property_uri . 'dateSubmitted',         $type_uri . 'string', null, false, 'u'], // date de soumission de la ressource
            // ---------------------------------------------------------------------------------------------------------------------------------------
            'description'            => [$property_uri . 'description',           $type_uri . 'string', null, false, 'u'], // description de l'objet
            'education_level'        => [$property_uri . 'educationLevel',        $type_uri . 'string', null, false, 'u'], // niveau d'éducation du public auquel la ressource est destinée
            'extent'                 => [$property_uri . 'extent',                $type_uri . 'string', null, false, 'm'], // taille ou la durée de la ressource
            'format'                 => [$property_uri . 'format',                $type_uri . 'string', null, false, 'm'], // le format de fichier, le support physique ou les dimensions de la ressource
            'has_format'             => [$property_uri . 'hasFormat',             $type_uri . 'string', null, false, 'u'], // une ressource liée qui est en substance la même que la ressource décrite préexistente mais dans un autre format
            'has_part'               => [$property_uri . 'hasPart',               $type_uri . 'string', null, false, 'm'], // une ressource liée qui est incluse physiquement ou logiquement dans la ressource décrite
            'has_version'            => [$property_uri . 'hasVersion',            $type_uri . 'string', null, false, 'm'], // une ressource liée qui est une version, une édition ou une adaptation de la ressource décrite
            'identifier'             => [$property_uri . 'identifier',            $type_uri . 'string', null, false, 'm'], // identifiant, cote de la ressource décrite. ne pas y mettre l'identifiant DOI qui est déjà exprimé dans la propriété nakala
            'instructional_method'   => [$property_uri . 'instructionalMethod',   $type_uri . 'string', null, false, 'u'], // un processus utilisé pour engendrer des connaissances, des attitudes et des compétences, pour le soutien duquel la ressource décrite est prévue
            'is_format_of'           => [$property_uri . 'isFormatOf',            $type_uri . 'string', null, false, 'm'], // une ressource liée qui est en substance la même que la ressource décrite mais dans un autre format
            'is_part_of'             => [$property_uri . 'isPartOf',              $type_uri . 'string', null, false, 'm'], // une ressource liée dans laquelle la ressource décrite est physiquement ou logiquement incluse
            'is_referenced_by'       => [$property_uri . 'isReferencedBy',        $type_uri . 'string', null, false, 'm'], // une ressource liée qui référence, cite ou encore pointe vers la ressource décrite
            'is_replaced_by'         => [$property_uri . 'isReplacedBy',          $type_uri . 'string', null, false, 'm'], // une ressource liée qui supplante, déplace ou remplace la ressource décrite
            'is_required_by'         => [$property_uri . 'isRequiredBy',          $type_uri . 'string', null, false, 'm'], // une ressource liée qui exige de la ressource décrite qu'elle soutienne sa fonction, sa distribution ou sa cohérence
            'issued'                 => [$property_uri . 'issued',                $type_uri . 'string', null, false, 'u'], // date de parution formelle (par exemple, la publication) de la ressource
            'is_version_of'          => [$property_uri . 'isVersionOf',           $type_uri . 'string', null, false, 'm'], // une ressource liée dont la ressource décrite est une version, une édition ou une adaptation
            'language'               => [$property_uri . 'language',              $type_uri . 'string', null, false, 'm'], // code langue sur 2 caractères
            'license'                => [$property_uri . 'license',               $type_uri . 'string', null, false, 'u'], // voir valeur nakala
            'mediator'               => [$property_uri . 'mediator',              $type_uri . 'string', null, false, 'm'], // une entité qui modère l'accès à la ressource et à qui la ressource est destinée ou utile
            'medium'                 => [$property_uri . 'medium',                $type_uri . 'string', null, false, 'm'], // la matière ou le support physique de la ressource
            'modified'               => [$property_uri . 'modified',              $type_uri . 'string', null, false, 'm'], // date à laquelle la ressource a été modifiée
            'provenance'             => [$property_uri . 'provenance',            $type_uri . 'string', null, false, 'm'], // une déclaration de tous les changements d'appartenance et de garde de la ressource depuis sa création qui sont importants pour son authenticité
            'publisher'              => [$property_uri . 'publisher',             $type_uri . 'string', null, $publisher,  'm'], // éditeur
            'reference'              => [$property_uri . 'references',            $type_uri . 'string', null, false, 'm'], // une ressource liée qui est référencée, citée ou encore vers laquelle pointe la ressource décrite
            'relation'               => [$property_uri . 'relation',              $type_uri . 'string', null, false, 'm'], // ressources liées
            'replaces'               => [$property_uri . 'replaces',              $type_uri . 'string', null, false, 'm'], // une ressource liée qui est supplantée, déplacée ou remplacée par la ressource décrite
            'requires'               => [$property_uri . 'requires',              $type_uri . 'string', null, false, 'm'], // une ressource liée qui est exigée par la ressource décrite pour soutenir sa fonction, sa distribution ou sa cohérence
            'rights'                 => [$property_uri . 'rights',                $type_uri . 'string', null, false, 'm'], // crédits
            'rights_holder'          => [$property_uri . 'rightsHolder',          $type_uri . 'string', null, false, 'm'], // une personne ou une organisation possédant ou exploitant les droits sur la ressources
            'source'                 => [$property_uri . 'source',                $type_uri . 'string', null, false, 'm'], // fonds et n° d'inventaire
            'spatial'                => [$property_uri . 'spatial',               $type_uri . 'string', null, false, 'm'], // lieu de découverte
            'subject'                => [$property_uri . 'subject',               $type_uri . 'string', null, false, 'm'], // sujet
            'table_of_contents'      => [$property_uri . 'tableOfContents',       $type_uri . 'string', null, false, 'u'], // table des matière du contenu de la ressource sous forme de texte libre

            // Champs gérés avec la méthode checkTemporal()
            //'temporal_litteraly'     => [$property_uri . 'temporal',              $type_uri . 'string', null, false, 'm'], // datation : période ou date
            //'temporal_start'         => [$property_uri . 'temporal',              $type_uri . 'string', null, false, 'm'], // datation : début
            //'temporal_end'           => [$property_uri . 'temporal',              $type_uri . 'string', null, false, 'm'], // datation : fin

            'type'                   => [$property_uri . 'type',                  $type_uri . 'string', null, $type, 'm'], // type de document
            'valid'                  => [$property_uri . 'valid',                 $type_uri . 'string', null, false, 'u'], // date (souvent un intervalle) de validité d'une ressource
        ];

        foreach ($resource as $k => $v)
        {
            if (array_key_exists($k, $fields_list))
            {
                $value = ($resource[$k] !== null) ? $resource[$k] : '';

                    ($fields_list[$k][4] === 'u')
                    ? $this->checkUniqueMeta($value, $fields_list[$k][0], $fields_list[$k][1], $fields_list[$k][2], $fields_list[$k][3])
                    : $this->checkMultipleMeta($value, $fields_list[$k][0], $fields_list[$k][1], $fields_list[$k][2], $fields_list[$k][3]);
            }
        }

        // Check des champs temporal
        $this->checkTemporal(
            ((! empty($resource['temporal_litteraly'])) ? $resource['temporal_litteraly'] : ''),
            ((! empty($resource['temporal_start'])) ? $resource['temporal_start'] : ''),
            ((! empty($resource['temporal_end'])) ? $resource['temporal_end'] : '')
        );
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * setErrorMeta
     *
     * @desc
     * @param  string $propertyUri
     * @return void
     */
    public function setErrorMeta(string $property_uri)
    {
        $t = explode('/', $property_uri);
        $field = end($t);
        $this->check_errors[$field] = 'Champ ' . $field . ' non renseigné'; //$this->error_message[$field];
    }
}
