<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use ZipArchive;

class ProcessingMediaFileController extends Controller
{
    private $sends_id;
    private $rand_media_dir;
    private $media_dir;
    private $files_list = [];         // Liste des fichiers en tableau multi-dimensionel
    private $files_list_zip = [];     // Liste des fichiers ZIP
    private $files_list_archive = []; // Liste des fichiers présents dans le ZIP
    private $replace_car = [];


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * Constructeur
     *
     * @return void
     */
    public function __construct(string $rand_media_dir, int $sends_id)
    {
        $this->rand_media_dir = $rand_media_dir;
        $this->sends_id = $sends_id;
        $this->replace_car = config('aoroc_nakala.replace_car');
        $this->media_dir = base_path('upload/media') . '/' . $this->sends_id;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * processingMediaFile
     *
     * @return bool
     */
    public function processingMediaFile(): bool
    {
        $this->listMediaFile($this->rand_media_dir);

        if (!empty($this->files_list_zip))
        {
            $this->files_list = [];
            $this->decompressZipFile();
            $this->listMediaFile($this->rand_media_dir); // Regénération des listes des fichiers pour prise en compte des fichiers décompréssés
        }

        $this->moveMediaFile();

        return $this->checkConcordance();
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * listMediaFile
     *
     * @return void
     */
    private function listMediaFile(string $dir): void
    {
        if ($handle = opendir($dir))
        {
            while (false !== ($file = readdir($handle)))
            {
                if ($file != '.' && $file != '..')
                {
                    if (is_dir($dir . '/' . $file))
                        self::listMediaFile($dir . '/' . $file); // $this->files_list[] =
                    else
                    {
                        if (substr($file, -3) == 'zip')
                            $this->files_list_zip[] = $file;
                        else
                            $this->files_list[] = $dir. '/' . $file;
                    }
                }
            }

            closedir($handle);
        }
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * decompressZipFile
     *
     * @desc Décompression du/des fichiers ZIP
     * @return void
     */
    private function decompressZipFile(): void
    {
        $za = new ZipArchive;

        foreach ($this->files_list_zip as $k => $v)
        {
            $zipFile = $this->rand_media_dir . '/' . $v;
            $za->open($zipFile);

            if ($za->extractTo($this->rand_media_dir))
                unlink($zipFile);

            for ($i = 0; $i < $za->numFiles; $i++)
            {
                if ($za->statIndex($i)['size'] > 0)
                    $this->files_list_archive[] = $za->statIndex($i)['name'];
            }

            // Renomage des fichiers décompréssés au format DIR_FILENAME
            $this->renameFile();
            $za->close();
        }
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * renameFile
     *
     * @return void
     */
    private function renameFile(): void
    {
        foreach ($this->files_list_archive as $k => $v)
        {
            if ($v !== null)
            {
                $filename = str_replace($this->replace_car[0], $this->replace_car[1], $v);
                rename($this->rand_media_dir . '/' . $v, $this->rand_media_dir . '/' . $filename);
            }
        }
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * moveMediaFile
     *
     * @return bool
     */
    private function moveMediaFile(): bool
    {
        if (!file_exists($this->media_dir))
            mkdir($this->media_dir);

        $this->files_list = [];

        //{jpg,JPG,png,PNG,gif,GIF,pdf,PDF,zip,ZIP,7z,7Z}
        foreach (glob($this->rand_media_dir . '/*.{jpg,png,gif,pdf,zip,7z}', GLOB_BRACE) as $dir_and_filename)
        {
            $explode = explode('/', $dir_and_filename);
            $filename = strtolower(str_replace($this->replace_car[0], $this->replace_car[1], end($explode)));

            if (!rename($dir_and_filename, $this->media_dir . '/' . $filename))
                return false;

            $this->files_list[] = $filename;
        }

        rrmdir($this->rand_media_dir);

        return true;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkConcordance
     *
     * @desc Vérrification de la concordance entre les données en base et la liste des fichiers envoyés
     * @return bool
     */
    private function checkConcordance(): bool
    {
        $listMediaInDatabase = $this->getMediaListInDatabase();

        foreach ($listMediaInDatabase as $k => $v)
        {
            $files = explode(';', $v->files); // Pour le cas ou il y a plusieurs fichiers on en fait un tableau

            foreach ($files as $kf => $vf)
            {
                if (!in_array($vf, $this->files_list))
                    DB::update(
                        'UPDATE public.resource SET status = \'er\', error = \'files|Image manquante;\' WHERE id = :id',
                        ['id' => $v->id]
                    );
            }
        }

        return true;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * getMediaListInDatabase
     *
     * @desc Retourne la liste des noms de fichiers présents en base de données
     * @return array
     */
    private function getMediaListInDatabase(): array
    {
        $list = DB::select(
            'SELECT id, files FROM public.resource WHERE sends_id = :sends_id',
            ['sends_id' => $this->sends_id]
        );

        return $list;
    }

}
