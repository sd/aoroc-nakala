<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Log;

class ProcessingDataFileController extends Controller
{
    private $data_file_name = '';
    private $datas = [];
    private $nb_col = 0;
    private $fields_name = '';
    private $check_field = [];
    private $data_lines = [];
    private $sends_id = 0;
    private $result_insert_data = false;
    private $replace_car = [];
    private $replace_car_all_fields = [];
    private $project = '';


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkFileType
     */
    public function checkFileType(string $data_file_name, int $sends_id, string $project)
    {
        $this->replace_car = config('aoroc_nakala.replace_car');
        $this->replace_car_all_fields = config('aoroc_nakala.resource.all_fields.replace_car');
        $this->data_file_name = base_path('upload/data/') . $data_file_name;
        $this->sends_id = $sends_id;
        // Inutilisé pour le moment. Permettra par la suite de checker si tous les champs obligatoire sont présent pour le type de projet donné
        //$this->project = $project;

        $file = explode('.', $this->data_file_name);
        $extension = $file[1];

        ini_set('auto_detect_line_endings', true);
        //$data_file = fopen($this->data_file_name, 'r');

        ($extension === 'csv')
            ? $this->readCsvFile()
            : $this->readJsonFile();

        //fclose($data_file);
        ini_set('auto_detect_line_endings', false);

        return $this->result_insert_data; // Valeur définie plus loin lors de l'insertion en base
    }


    /* ============================================================================================================================================ */


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * readCsvFile
     *
     * @return void
     */
    private function readCsvFile(): void
    {
        //Log::channel('upload')->info('Lecture du fichier => ' . $this->data_file_name);

        $this->addCsvLinesInDB();
        $this->loadCsvFieldsLine();
        $this->loadCsvCheckLine();
        $this->loadCsvDatalines();
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * addCsvLinesInDB
     *
     * @return bool
     */
    private function addCsvLinesInDB()
    {
        $file = file($this->data_file_name);

        $this->datas[0] = str_getcsv($file[0]);
        $this->datas[1] = str_getcsv($file[1]);

        $this->data_lines = array_slice($file, 2);

        foreach ($this->data_lines as $k => $v)
        {
            DB::insert('INSERT INTO public.csv (sends_id, data) VALUES (:sends_id, :data)', [$this->sends_id, $v]);
        }
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * loadCsvFieldsLine
     *
     * @return void
     */
    public function loadCsvFieldsLine(): void
    {
        $fields = json_encode($this->datas[0]);
        $before = config('aoroc_nakala.replace_fields.before');
        $after = config('aoroc_nakala.replace_fields.after');
        $this->fields_name = str_replace($before, $after, $fields);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * loadCsvCheckLine
     *
     * @return void
     */
    private function loadCsvCheckLine(): void
    {
        $this->check_field = $this->datas[1];
        $this->nb_col = count($this->check_field); // Nombre de colonnes dans la seconde ligne
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * loadCsvDatalines
     *
     * @return bool
     */
    private function loadCsvDatalines(): bool
    {
        $data_line = DB::select('SELECT id AS csv_id, data FROM public.csv WHERE sends_id = :sends_id', [$this->sends_id]);

        foreach ($data_line as $k => $v)
        {
            $this->checkCsvConcordance(str_getcsv($v->data), $v->csv_id);
        }

        return true;
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkCsvConcordance
     *
     * @param array $data
     * @param int $csv_id
     * @return bool
     */
    private function checkCsvConcordance(array $data, int $csv_id): bool
    {
        // $data correspond à une ligne de donnée dans le CSV en rpovenance de la table public.csv
        if ($this->nb_col == count($data))
        {
            $sql_values = '';

            foreach ($data as $a => $b)
            {
                if ($this->check_field[$a] === '1') // Check si on est sur un champ obligatoire -> 1 - non obligatoire -> 0
                    if (empty($b)) // Pas de valeur renseignée pour le champ obligatoire : erreur
                        return false;

                if ($a === 1) // Second champ du tableau (l'image)
                    $b = strtolower(str_replace($this->replace_car[0], $this->replace_car[1], $b)); // Remplacement de caractères
                else
                    $b = str_replace($this->replace_car_all_fields[0], $this->replace_car_all_fields[1], $b);

                $sql_values .= '\'' . str_replace('\'', '\'\'', $b) . '\',';
            }

            // Ajout de la ligne dans la table public.resource
            $this->saveLineInDB(
                'INSERT INTO public.resource (sends_id,' . $this->fields_name . ')
                 VALUES (' . $this->sends_id . ', ' . substr($sql_values, 0, -1) . ')'
            );

            // Suppression de la ligne de la table public.csv
            DB::delete('DELETE FROM public.csv WHERE id = :csv_id', [$csv_id]);
        }

        return true;
    }


    /* ============================================================================================================================================ */


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * readJsonFile
     *
     * @return void
     */
    private function readJsonFile(): void
    {
        $this->datas = json_decode(file_get_contents($this->data_file_name), true);
        $this->loadJsonCheckLine();
        $this->loadJsonDatalines();
        $this->checkJsonConcordance();
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * loadJsonCheckLine
     *
     * @return void
     */
    private function loadJsonCheckLine(): void
    {
        $this->check_field = $this->datas['fields'];
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * loadJsonDatalines
     *
     * @return void
     */
    private function loadJsonDatalines(): void
    {
        $this->data_lines = $this->datas['data'];
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * checkJsonConcordance
     *
     * @return void
     */
    private function checkJsonConcordance(): void
    {
        $new_data = [];

        foreach ($this->data_lines as $k => $v) // Lecture du tableau principal
        {
            $new_data[$k]['files'] = $this->data_lines[$k]['files'][0]['name'];
            $new_data[$k]['collection'] = $this->data_lines[$k]['collectionsIds'][0];

            // Définition de variable ou remise à 0 pour si plus de 1 boucle effectuée
            $statut = 'wt';
            $f = [];
            $fields_list = $values_list = $error = '';

            foreach ($this->data_lines[$k]['metas'] as $k1 => $metas) // Tableau des metas
            {
                $f[] = $metas['propertyUri']; // Sert pour le check des champs obligatoire
                $fields_list .= $metas['propertyUri'] . ',';
                $values_list .= '\'' . str_replace(
                    '\'', '\'\'',
                    str_replace($this->replace_car_all_fields[0], $this->replace_car_all_fields[1], $metas['value'])
                ) . '\',';
            }

            // Check de champs présents
            foreach ($this->check_field as $k2 => $datas_fields)
            {
                // $datas_fields : champ obligatoire présent dans le tableau [fields]
                // $f : liste des champs présents dans un "meta"
                if (!in_array($datas_fields, $f))
                {
                    $statut = 'er';
                    $error .=  $datas_fields . ';';
                }
            }

            // Création de la requête SQL et éxecution
            $new_data[$k]['fields_list'] = 'sends_id,files,collection,status,error,' . substr(str_replace(['dcterms:', 'nakala:'], ['', 'nk_'], $fields_list), 0, -1);

            $new_data[$k]['values_list'] = $this->sends_id . ',\''
                . str_replace($this->replace_car[0], $this->replace_car[1], $new_data[$k]['files']) . '\',\''
                . $new_data[$k]['collection'] . '\',\''
                . $statut . '\',\''
                . $error . '\','
                . substr($values_list, 0, -1);

            $this->saveLineInDB('INSERT INTO public.resource (' . $new_data[$k]['fields_list'] . ') VALUES (' . $new_data[$k]['values_list'] . ');');
        }
    }


    /* ============================================================================================================================================ */


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * saveLineInDB
     *
     * @param string $query
     * @return void
     */
    private function saveLineInDB(string $query): void
    {
        $this->result_insert_data = (DB::insert($query))
            ? true
            : false;
    }
}
