<?php

namespace App\Http\Controllers\Admin;

use App\User;

use App\Models\Admin\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * Display a listing of the resource.
     */
    public function list()
    {
        $users = DB::table('public.users')
            ->leftJoin('role', 'users.id_role', '=', 'role.id')
            ->select('users.id', 'users.name', 'users.email', 'users.lastname', 'users.firstname', 'users.api_key', 'role.name AS role')
            ->orderBy('role.id', 'asc')
            ->orderBy('users.name', 'asc')
            ->get();

        return view('admin.user', [
            'users'      => $users,
            'roles'      => Role::all(),
        ]);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * Store a newly created resource in storage.
     *
     * @param  string  $request
     */
    public function create(Request $request)
    {
        $r = $request->all();
        $resultValidation = $this->validationData($r, 'insert');

        if ($resultValidation['error'] === true)
            return response()->json($resultValidation, 422);

        $user = User::create([
            'id_role'   => $r['id_role'],
            'name'      => ucfirst($r['name']),
            'email'     => strtolower($r['email']),
            'password'  => bcrypt($r['password']),
            'lastname'  => strtoupper($r['lastname']),
            'firstname' => ucfirst(strtolower($r['firstname'])),
            'api_key'   => strtolower($r['api_key']),
        ]);

        return response()->json([
            'user' => $user,
            'role' => Role::find($r['id_role']),
        ]);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_user
     *
     * @return mixed [ array | bool ]
     */
    public function edit($id_user)
    {
        return response()->json([
            'user' => User::findOrFail($id_user)
        ]);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * Update the specified resource in storage.
     *
     * @param  string  $request
     * @param  int  $id_user
     */
    public function update(Request $request, $id_user)
    {
        $user = User::find($id_user);

        $user->id_role   = ucfirst($request->id_role);
        $user->name      = ucfirst($request->name);
        $user->email     = strtolower($request->email);
        $user->lastname  = strtoupper($request->lastname);
        $user->firstname = ucfirst(strtolower($request->firstname));
        $user->api_key   = strtolower($request->api_key);

        if (!empty($request->password))
            $user->password = bcrypt($request->password);

        // Enregistrement modif user

        $resultValidation = $this->validationData($request->all(), 'update', $id_user);

        if ($resultValidation['error'] === true)
            return response()->json($resultValidation, 422);

        $user->save();

        return response()->json([
            'user' => $user,
            'role' => Role::find($request->id_role)
        ]);
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * Remove the specified resource from storage.
     *
     * @param  int  $id_user
     */
    public function delete($id_user)
    {
        return response()->json(User::destroy($id_user));
    }


    /** ----------------------------------------------------------------------------------------------------------------------------------------------
     * Validation des datas
     *
     * @param  array $data
     * @param  string $type
     * @param  int $id_user
     * @access private
     */
    private function validationData(array $data, string $type = 'insert', int $id_user = null)
    {
        $password_rules =  ['required',
            'min:2',
            //'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/',
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            //'regex:/[A-Z]/',      // must contain at least one uppercase letter
            //'regex:/[0-9]/',      // must contain at least one digit
            //'regex:/[@$!%*#?&]/', // must contain a special character
            'confirmed'
        ];

        if ($type == 'insert')
            $rules = [
                'name'     => 'required|min:5|unique:users,name',
                'email'    => 'required|email|min:10|unique:users,email',
                'password' => $password_rules,
                'api_key'  => 'nullable|size:36|unique:users,api_key',
            ];
        else
            $rules = [
                'name'    => 'required|min:5',
                'email'   => 'required|email|min:10',
                /*'api_key' => [
                    'nullable',
                    'size:36',
                    Rule::unique('users')->ignore($id_user),
                ],*/
            ];

            if (!empty($data['password']))
                $rules['password'] = $password_rules;

        $data['name']    = ucfirst($data['name']);
        $data['email']   = strtolower($data['email']);
        $data['api_key'] = strtolower($data['api_key']);

        $validator = Validator::make($data, $rules);

        return ($validator->fails()) ? ['error' => true, 'messages' => $validator->errors()] : ['error' => false];
    }
}
