<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /** **********************************************************************************************************************************************
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        return view('admin.role', ['roles' => DB::table('role')->orderBy('id', 'asc')->get()]);
    }


    /** **********************************************************************************************************************************************
     * Store a newly created resource in storage.
     *
     * @param  string  $request
     * @return json
     */
    public function create(Request $request)
    {
        $r = $request->all();

        $resultValidation = $this->validationData($r, 'insert');

        if ($resultValidation['error'] === true)
            return response()->json($resultValidation, 422);

        $role = Role::create(['name' => ucfirst($r['name'])]);

        return response()->json($role);
    }


    /** **********************************************************************************************************************************************
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_role
     */
    public function edit($id_role)
    {
        $role = Role::findOrFail($id_role);

        return response()->json($role);
    }


    /** **********************************************************************************************************************************************
     * Update the specified resource in storage.
     *
     * @param  string  $request
     * @param  int     $id_role
     */
    public function update(Request $request, $id_role)
    {
        $resultValidation = $this->validationData($request->all(), 'update');

        if ($resultValidation['error'] === true)
            return response()->json($resultValidation, 422);

        $role = Role::find($id_role);
        $role->name = ucfirst($request->name);
        $role->save();

        return response()->json($role);
    }


    /** **********************************************************************************************************************************************
     * Remove the specified resource from storage.
     *
     * @param  int  $id_role
     */
    public function delete($id_role)
    {
        return response()->json(Role::destroy($id_role));
    }


    /** **********************************************************************************************************************************************
     * Validation des datas
     *
     * @param  array  $data
     * @param  string $type
     * @access  private
     * @return  mixed  array | bool
     */
    private function validationData(array $data, string $type = 'insert')
    {
        $rules        = ['name' => 'required|min:5'.(($type == 'insert') ? '|unique:role,name' : '')];
        $data['name'] = ucfirst($data['name']);
        $validator    = Validator::make($data, $rules);

        return ($validator->fails()) ? ['error' => true, 'messages' => $validator->errors()] : true;
    }
}
