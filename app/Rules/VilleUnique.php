<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class VilleUnique implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ville       = explode('*', $value);
        $step        = $ville[0];
        $id_pays     = $ville[1];
        $name        = ucfirst(strtolower(trim($ville[2])));
        $code_postal = trim($ville[3]);

        if ($step == 'insert')
            $result = DB::table('ville')
                ->where('name', '=', $name)
                ->where('id_pays', '=', $id_pays)
                ->where('code_postal', '=', $code_postal)
                ->get();
        else
            $result = DB::table('ville')
                ->where('name', '=', $name)
                ->where('id_pays', '=', $id_pays)
                ->where('code_postal', '=', $code_postal)
                ->where('id', '!=', $ville[4])
                ->get();

        return count($result) === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.ville_unique');
    }
}
