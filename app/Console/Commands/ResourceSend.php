<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
//use App\Http\Controllers\ResourceController;

class ResourceSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aoroc_nakala:send_resource';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoi des ressources vers NAKALA';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //$resource_controller = new ResourceController;
        //$resource_controller->getResourceToSend();

        return 0;
    }
}
