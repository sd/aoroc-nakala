<?php

// ---------------------------------------------------------------------------------------------------------------------------------------------------
//
if (! function_exists('rrmdir'))
{
    function rrmdir($src): void
    {
        if (file_exists($src))
        {
            $dir = opendir($src);

            while (false !== ($file = readdir($dir)))
            {
                if (($file != '.') && ($file != '..'))
                {
                    $full = $src . '/' . $file;

                    if (is_dir($full))
                        rrmdir($full);
                    else
                        @unlink($full);
                }
            }

            closedir($dir);
            rmdir($src);
        }
    }
}


// ---------------------------------------------------------------------------------------------------------------------------------------------------
//
if (! function_exists('rand_string'))
{
    function rand_string($length = 8, $upper = true, $lower = true, $number = false, $string = 'hello world'): string
    {
        $string = '';

        if ($upper)
            $string .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if ($lower)
            $string .= 'abcdefghijklmnopqrstuvwxyz';

        if ($number)
            $string .= '0123456789';

        mt_srand((double)microtime() * 1000000);
        $s = '';

        if ($length > 0)
            while (strlen($s) < $length)
                $s.= $string[mt_rand(0, strlen($string) - 1)];

        return $s;
    }
}


// ---------------------------------------------------------------------------------------------------------------------------------------------------
//
if (! function_exists('get_git_commit_id'))
{
    function get_git_commit_id()
    {
        $git_commit_id_dev = trim(substr(file_get_contents(base_path() . '/.git/refs/heads/develop'), 0, 7));
        $git_commit_id_prod = trim(substr(file_get_contents(base_path() . '/.git/refs/heads/main'), 0, 7));
        $git_last_commit_date_dev = substr(exec('git log develop -1 --pretty=format:\'%ci\''), 0, 19);
        $git_last_commit_date_prod = substr(exec('git log main -1 --pretty=format:\'%ci\''), 0, 19);

        return '[prod : ' . $git_commit_id_dev . ' - ' . $git_last_commit_date_prod . ']
                [dev : ' . $git_commit_id_prod . ' - ' . $git_last_commit_date_dev . ']';
    }
}


// ---------------------------------------------------------------------------------------------------------------------------------------------------
// Passe en linuscule une extention de fichier
//
if (! function_exists('lower_extension'))
{
    function lower_extension($file)
    {
        return str_replace(
            substr($file, strripos($file, '.')),
            strtolower(substr($file, strripos($file, '.'))),
            $file
        );
    }
}
