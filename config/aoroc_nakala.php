<?php

return [

    'api' => [
        // prod
        'nakala_url' => 'https://nakala.fr',
        'api_url' => 'https://api.nakala.fr',

        // test
        //'nakala_url' => 'https://test.nakala.fr',
        //'api_url' => 'https://apitest.nakala.fr',
    ],

    'replace_car' => [
        [
            'Ä', 'Â', 'À', 'Æ', 'ä', 'â', 'à', 'á', 'ã', 'å', 'æ',
            'Ç', 'ç',
            'Ð',
            'Ë', 'Ê', 'É', 'È', 'ë', 'ê', 'è', 'é',
            'Ï', 'Î', 'Ì', 'Í', 'ï', 'î', 'ì', 'í',
            'Ñ', 'ñ',
            'Ö', 'Ô', 'Ò', 'Ó', 'Õ', 'Ø', 'ö', 'ô', 'ø', 'õ', 'ò', 'ó', 'ð',
            'ß',
            'Ü', 'Û', 'Ù', 'Ú', 'ü', 'û', 'ù', 'ú', 'µ',
            'Ÿ', 'Ý', 'ÿ', 'ý',

            '/', ' ', '\'', '"',
            '[', ']', '{', '}', '(',  ')',
            '&', '°', ',', '?', '*', ':', '´',
            '!', '@', '%', '#', 'Þ', '<', '>', '\\',
            '.JPG', '.PNG', '.GIF', '.PDF',
        ],
        [
            'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a',
            'c', 'c',
            'd',
            'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e',
            'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i',
            'n', 'n',
            'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o',
            's',
            'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u',
            'y', 'y', 'y', 'y',

            '_', '_', '_', '_',
            '', '', '', '', '', '',
            '', '', '', '', '', '', '',
            '', 'a', '', '', '', '', '', '',
            '.jpg', '.png', '.gif', '.pdf',
        ],
    ],

    'replace_fields' => [
        'before' => [
            '"', 'dcterms:', 'nakala:', 'fichier', '[', ']',
            'accessRights', 'accrualMethod', 'accrualPeriodicity', 'accrualPolicy', 'bibliographicCitation', 'conformsTo', 'dateAccepted',
            'dateCopyrighted', 'dateSubmitted', 'educationLevel', 'hasFormat', 'hasPart', 'hasVersion', 'instructionalMethod', 'isFormatOf',
            'isPartOf', 'isReferencedBy', 'isReplacedBy', 'isRequiredBy', 'isVersionOf', 'rightsHolder', 'tableOfContents',
        ],
        'after' => [
            '', '', 'nk_', 'files', '', '',
            'access_rights', 'accrual_method', 'accrual_periodicity', 'accrual_policy', 'bibliographic_citation', 'conforms_to', 'date_accepted',
            'date_copyrighted', 'date_submitted', 'education_level', 'has_format', 'has_part', 'has_version', 'instructional_method', 'is_format_of',
            'is_part_of', 'is_referenced_by', 'is_replaced_by', 'is_required_by', 'is_version_of', 'rights_holder', 'table_of_contents',
        ]
    ],

    'sends' => [
        'name' => [
            'replace_car' => [
                ['#', '/', '\\', '"', '\'', '*', '=', '@'],
                ['_', '_', '_', '_', '_', '_', '_', '_'],
            ],
        ]
    ],

    'resource' => [
        'all_fields' => [
            'replace_car' => [
                ['\'', '""'],
                ['’', '"']
            ],
        ],
        'error_message' => [
            'nk_license' => 'license nakala manquante',
            'nk_title'   => 'titre nakala manquant',
            'nk_type'    => 'type nakala manquant',
            'coverage'   => 'code pays manquant',
            'created'    => 'période manquant',
            'publisher'  => 'éditeur manquant',
            'type'       => 'type de document manquant',
        ],
        'updateable_fields' => [
            'collection',
            'language',
            'publisher',
            'rights',
            'nk_license',
            'nk_type',
            //statut
        ],
    ],

    'media' => [
        'purge' => [
            'active' => false,
            'nb_days' => 2,
        ],
    ],

];
