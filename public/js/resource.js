
/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Affichage en détail de la ressource pour en permettre l'édition
 */
function resourceView(resource_id, project)
{
    console.log('resourceView() : ' + project)
    resourceHide() // On ferme toutes les lignes éventuellement ouvertes

    // Récupération des données
    $.get(route('resource.detail.ajax', [resource_id, project]))
        .done(function(result)
        {
            const ress_keys = Object.keys(result)
            const ress_values = Object.values(result)
            let data = '', error_color = '', errors = reformatErrors(ress_values[1])

            $.each(ress_keys, function(k, field)
            {
                if (field !== 'status' && field !== 'error' && field !== 'status_text'
                    && field !== 'nakala_url' && field !== 'nakala_id' && field !== 'nakala_image')
                {
                    // Check si le champ fait parti du tableau des erreurs ou pas
                    error_color = (typeof errors[field] !== 'undefined') ? 'danger' : 'primary'

                    data += `
                        <li id="${field}_${resource_id}">
                            <strong class="text-${error_color}">
                                <button class="btn btn-xxs btn-outline-${error_color}"
                                        onclick="resourceEdit(\'${field}\', ${resource_id}, \'${project}\')">
                                <i class="fa fa-pen"></i>
                                </button> ${field} :
                            </strong>
                            <span class="text-dark">${((ress_values[k] !== null) ? ress_values[k] : '')}</span>
                        </li>`
                }
            });

            let line = `
                <tr id="resource_detail_${resource_id}"> 
                    <td colspan="5"> 
                        <button class="btn btn-xs btn-outline-secondary" onclick="resourceHide()"><i class="fa fa-window-close"></i> Annuler / Fermer</button> 
                        <ul>${data}</ul> 
                    </td> 
                </tr>`

            $(line).insertAfter('#resource_list_' + resource_id)
        })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Reformatage des erreurs sous forme de tableau exploitable pour resourceView()
 */
function reformatErrors(errors)
{
    if (errors === null)
        return []

    let error = [], a = ''

    errors.substring(0, errors.length - 1).split(';').forEach((v, k) => {
        a = v.split('|')
        error[a[0]] = a[1]
    })

    return error
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function resourceHide()
{
    $('tbody').find('tr[id^="resource_detail_"]').hide()
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function resourceEdit(field, resource_id, project)
{
    let line = $('#' + field + '_' + resource_id),
        input = '',
        data = line.find('span').text().replace(/"/g, '&quot;'),
        message = message_info(field)

    if (field !== 'files')
    {
        input = `
            <div class="input-group input-group-sm mb-3">
                <input class="form-control form-control-sm col-12" type="text" value="${data}">
                <div class="input-group-append ">
                    <button class="btn btn-sm btn-outline-success" type="button"
                            onclick="resourceUpdate(\'${field}\', ${resource_id}, \'${project}\')">Ok</button>
                    <button class="btn btn-sm btn-outline-secondary" type="button"
                            onclick="resourceCancelEdit(\'${field}\', ${resource_id}, \'${data}\')">Annuler</button>
                </div>
            </div>
            ${(message) ? `<div class="text-danger mb-3"><small><i class="fa fa-exclamation-triangle"></i> ${message}</small></div>` : ''}`
    }
    else
    {
        input = `
            <form method="post" enctype="multipart/form-data" id="update_media_file">
                <div class="input-group input-group-sm mb-3">
                    <input type="hidden" name="save_sends_data" value="false">
                    <input type="hidden" name="resource_id" value="${resource_id}">
                    <input class="form-control form-control-sm col-12" multiple type="file" id="media_file_${resource_id}" name="media_file[]">
                    <div class="input-group-append ">
                        <button class="btn btn-sm btn-outline-success" type="button"
                                onclick="resourceFileUpdate(${resource_id}, \'${project}\')">Ok</button>
                        <button class="btn btn-sm btn-outline-secondary" type="button"
                                onclick="resourceCancelEdit(\'${field}\', ${resource_id}, \'${data}\')">Annuler</button>
                    </div>
                </div>
                ${(message) ? `<div class="text-danger mb-3"><small><i class="fa fa-exclamation-triangle"></i> ${message}</small></div>` : ''}
            </form>`
    }

    line.find('button[onclick^="resourceEdit"]').hide()
    line.find('span').html(input)
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function resourceUpdate(field, resource_id, project)
{
    let line = $('#' + field + '_' + resource_id)
    let value = line.find('input').val().replace(/"/g, '&quot;').replace(/\//g, '&#47;')

    if (value === '')
        value = '-'

    $.get(route('resource.update.ajax', [resource_id, field, encodeURI(value)]))
        .done(function (result)
        {
            resourceReloadLineTab(resource_id, '' + project + '')
            resourceView(resource_id, '' + project + '')
        })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Rechargement de la ligne du tableau HTML des ressources suite à une modification
 */
function resourceReloadLineTab(resource_id, project)
{
    let line = $('#resource_list_' + resource_id), errors = '', error = '', badge_error = ''

    $.get(route('resource.detail.ajax', [resource_id, project])).done(function (resource)
    {
        let status_text = resource.status_text.split('|'), nakala_url = '', resource_type = '', resource_description = ''

        // Gestion de l'affichage du badge d'erreur si présent
        if (resource.status ===  'er')
        {
            errors = resource.error.substring(0, resource.error.length - 1).split(';');
            errors.forEach((v, k) => {
                error = v.split('|')
                badge_error = `<span class="badge badge-pill badge-danger">${error[1]}</span><br>`
            })
        }

        // Check si resource.type est null
        resource_type = (!resource.type) ? '' : resource.type

        // Affichage du picto pour URL vers NAKALA
        if (resource.nakala_url)
            nakala_url = `<br/>
            <a href="${resource.nakala_url}/${resource.nakala_id}" target="_blank">
                <img src="${resource.nakala_image}" width="25" alt="NAKALA" title="Voir la ressource sur NAKALA"></img>
            </a>`

        // Condition d'affichage de la description
        if (resource.description)
            resource_description = `
            <ul class="pl-3">
                <li><span class="small">${resource.description}</span></li>
            </ul>`

        let html_line = `
            <td class="text-center">
                <button class="btn btn-xs btn-outline-primary" onclick="resourceView(${resource_id}, \'${project}\')"><i class="fa fa-pen"></i> Editer</button>
            </td>
            <td class="text-center">
                <span href="#" class="badge badge-${status_text[1]}">${status_text[0]}</span>
                ${nakala_url}
            </td>
            <td>
                <span class="badge badge-info mb-2">${resource.files}</span><br>
                ${resource.nk_title}
                ${resource_description}
                ${badge_error}
            </td>
            <td class="text-center">${resource_type}</td>`

        line.html(html_line)

        console.log(resource.files)
    })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function resourceFileUpdate(resource_id, project)
{
    if ($('#media_file').val() === '')
        alert('Attention : vous n\'avez pas sélectionné de fichiers à envoyer !')
    else
    {
        let file = document.getElementById('media_file_' + resource_id)
        let formData = new FormData()

        if (file.files.length > 0)
            for (var i = 0; i < file.files.length; i++)
                formData.append('media_file['+i+']', file.files[i])

        formData.append('sends_id', $('#sends_id').val())
        formData.append('resource_id', resource_id)

        $.ajax({
            url: route('sends.ajax.update_media_file'),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: 'POST',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data)
            {
                resourceReloadLineTab(resource_id, '' + project + '')
                resourceView(resource_id, '' + project + '')
            },
            error: function (data)
            {
                let r = data.responseText.split('|')

                if (r[0] === 'error')
                    alert(r[1])
            }
        });
    }
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function resourceCancelEdit(field, resource_id, data)
{
    let line = $('#' + field + '_' + resource_id)
    line.find('button[onclick^="resourceEdit"]').show()
    line.find('span').html(data)
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Envoi des ressources
 */
function sendResources(sends_id)
{
    $('#btn_send_resource').hide()
    $('#message').removeClass('text-danger').addClass('text-success').html('Envoi en cours, veuillez ne pas quitter cette page. La redirection se fera automatiquement<br/><br/>')

    $.get(route('resource.send_nakala.ajax', sends_id))
        .done(function (result)
        {
            document.location.href='./' + sends_id;
        })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function message_info(field)
{
    let message = ''

    switch (field)
    {
        case 'collection':
            message = 'Ne saisir ici que l\'identifiant complet de la collection. Si il y en a plusieurs, séparez les par un point-virgule.'
            break

        case 'files':
            message = 'ATTENTION : si vous modifiez ou ajoutez un ou plusieurs fichiers, vous devez renvoyer tous les fichiers pour cette ressource.'
            break

        case 'nk_created':
            message = `Donnée facultative, au format du standard W3CDTF, <a href="http://www.w3.org/TR/NOTE-datetime" target="_blank">consultable sur cette page</a>.`
            break
            message = 'ATTENTION : si vous modifiez ou ajoutez un ou plusieurs fichiers, vous devez renvoyer tous les fichiers pour cette ressource.'
            break

        case 'nk_creator':
            message = 'Donnée facultative, sous forme de nom prénom.'
            break

        case 'nk_license':
            message = 'Donnée requise. Se référer au guide pour les différentes valeurs possibles.'
            break

        case 'nk_title':
            message = 'Donnée requise (texte libre).'
            break

        case 'nk_type':
            message = `Consulter la liste des types (référentiel COAR) 
            <a href="https://documentation.huma-num.fr/nakala-guide-de-description/#metadonnees-obligatoires-et-fortement-recommandees" target="_blank">sur cette page</a>.
            Le type est à indiquer sous la forme de son URI.`
            break

        case 'language':
            message = `Langue utilisée pour la saisie des métadonnées, d\'après la norme ISO 639-1 (à 2 lettres).
            <a href="https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1" target="_blank">Consulter la liste des codes sur cette page</a>`
            break

        default:
    }

    return message
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function saveUpdateableField()
{
    let sends_id = $('#sends_id').val()
    let field = $('#field').val()
    let value = $('#value').val()

    if (field === 'x')
    {
        alert('Choix du champs incorrect...')
        return false
    }

    if (value === '')
    {
        alert('Valeur non renseignée...')
        return false
    }

    $.get(route('resource.update.all.ajax', [sends_id, field, encodeURIComponent(value)]))
        .done(function (result)
        {
            document.location.href='./' + sends_id;
        })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function updateLine(csv_id)
{
    let formData = new FormData();

    formData.append('csv_id', csv_id);
    formData.append('collection', $('#collection_' + csv_id).val());
    formData.append('fichier', $('#fichier_' + csv_id).val());
    formData.append('nakala_type', $('#nakala_type_' + csv_id).val());
    formData.append('dcterms_type', $('#dcterms_type_' + csv_id).val());
    formData.append('dcterms_creator', $('#dcterms_creator_' + csv_id).val());
    formData.append('dcterms_created', $('#dcterms_created_' + csv_id).val());
    formData.append('dcterms_source', $('#dcterms_source_' + csv_id).val());
    formData.append('nakala_title', $('#nakala_title_' + csv_id).val());
    formData.append('dcterms_description', $('#dcterms_description_' + csv_id).val());
    formData.append('dcterms_coverage', $('#dcterms_coverage_' + csv_id).val());
    formData.append('dcterms_spatial', $('#dcterms_spatial_' + csv_id).val());
    formData.append('dcterms_temporal_litteraly', $('#dcterms_temporal_litteraly_' + csv_id).val());
    formData.append('dcterms_temporal_start', $('#dcterms_temporal_start_' + csv_id).val());
    formData.append('dcterms_temporal_end', $('#dcterms_temporal_end_' + csv_id).val());
    formData.append('dcterms_subject', $('#dcterms_subject_' + csv_id).val());
    formData.append('dcterms_language', $('#dcterms_language_' + csv_id).val());
    formData.append('dcterms_relation', $('#dcterms_relation_' + csv_id).val());
    formData.append('dcterms_publisher', $('#dcterms_publisher_' + csv_id).val());
    formData.append('dcterms_rights', $('#dcterms_rights_' + csv_id).val());
    formData.append('nakala_license', $('#nakala_license_' + csv_id).val());
    formData.append('dcterms_bibliographicCitation', $('#dcterms_bibliographicCitation_' + csv_id).val());
    formData.append('dcterms_instructionalMethod', $('#dcterms_instructionalMethod_' + csv_id).val());
    formData.append('dcterms_contributor', $('#dcterms_contributor_' + csv_id).val());

    $.ajax({
        url: route('resource.update'),
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: 'POST',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (data) {},
        error: function (data) {}
    });

    $('#csv_line_' + csv_id).remove()
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Modification du statut de la ressource
 */
function changeResourceStatus(resource_id)
{
    $.get(route('resource.change_status.ajax', resource_id))
        .done(function (result)
        {
            $('#btn_chg_status_' + resource_id).remove()
            $('#status_' + resource_id).removeClass('badge-warning').addClass('badge-secondary').text('en attente')
        })
}
