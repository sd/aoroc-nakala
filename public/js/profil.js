
/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Récupération des données sur l'user et affichage du formulaire d'édition
 */
function showFormEdit()
{
    $.get(route('profil.edit'))
        .done(function (result)
        {
            $('#btn_show_profil').hide()
            $('#form_profil').show()
            $('#api_key').val(result.api_key)
        })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function cancelShowFormEdit()
{
    $('#btn_show_profil').show()
    $('#form_profil').hide()
    $('#api_key').val('')
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function saveProfil()
{
    let api_key = $('#api_key').val(),
        message = '',
        alert_type = ''

    $('#message').removeClass('alert-success alert-danger')

    $.get(route('profil.update', api_key))
        .done(function (result)
        {
            if (result === 'error_api_key')
            {
                message = '<i class="fas fa-exclamation-triangle"></i> Données non enregistrées, le format de votre clé d\'API est incorrecte !'
                alert_type = 'alert-danger'
            }
            else
            {
                cancelShowFormEdit()
                message = '<i class="fas fa-thumbs-up"></i> Données enregistrées avec succès !'
                alert_type = 'alert-success'
            }

            $('#message').addClass(alert_type).html(message).show().fadeIn().delay(4000).fadeOut()
        })

}
