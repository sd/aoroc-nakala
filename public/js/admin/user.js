
jQuery(document).ready(function($)
{
    ////----- Open the modal to CREATE a user -----////
    jQuery('#btn-add').click(function ()
    {
        resetMessageErrorForm();
        jQuery('#btn-save').val("add");
        jQuery('#id_pays').val(1);
        reloadCityList();
        jQuery('#modalFormData').trigger("reset");
        jQuery('#userEditorModal').modal('show');
    });

    ////----- Open the modal to UPDATE a user -----////
    jQuery('body').on('click', '.open-modal', function ()
    {
        let id_user = $(this).val();
        $.get('user/' + id_user, function (data) {
            resetMessageErrorForm();
            jQuery('#id_user').val(data.user['id']);
            jQuery('#id_role').val(data.user['id_role']);
            jQuery('#name').val(data.user['name']);
            jQuery('#email').val(data.user['email']);
            jQuery('#lastname').val(data.user['lastname']);
            jQuery('#firstname').val(data.user['firstname']);
            jQuery('#api_key').val(data.user['api_key']);
            jQuery('#btn-save').val("update");
            jQuery('#userEditorModal').modal('show');
        })
    });

    // Clicking the save button on the open modal for both CREATE and UPDATE
    $("#btn-save").click(function (e)
    {
        resetMessageErrorForm();
        e.preventDefault();

        let type     = "POST";
        let ajaxurl  = 'user';
        let state    = jQuery('#btn-save').val();
        let id_user  = jQuery('#id_user').val();
        let formData = {
                          id_role: jQuery('#id_role').val(),
                             name: jQuery('#name').val(),
                            email: jQuery('#email').val(),
                         lastname: jQuery('#lastname').val(),
                        firstname: jQuery('#firstname').val(),
                          api_key: jQuery('#api_key').val(),
                         password: jQuery('#password').val(),
            password_confirmation: jQuery('#password_confirmation').val(),
                         "_token": $('input[name="_token"]').val()};

        if (state === "update")
        {
            type = "PUT";
            ajaxurl = 'user/' + id_user;
        }

        $.ajax({
            type: type,
            url: ajaxurl,
            data: formData,
            dataType: 'json',
            success: function (data)
            {
                let user =
                '<tr id="name' + data.user.id + '">' +
                    '<td>' + data.user.lastname + ' ' + data.user.firstname + '</td>' +
                    '<td>' + data.user.email + '</td>' +
                    '<td>' + data.role.name + '</td>' +
                    '<td class="text-left">' +
                        '<button class="btn btn-xs btn-info open-modal" value="' + data.user.id + '">' +
                            '<i class="fas fa-pen"></i> ' + trans('app.button_edit') + '</button>';

                if (data.user.id > 3)
                    user += '&nbsp;<button class="btn btn-xs btn-danger delete-user" value="' + data.user.id + '" user="button">' +
                            '<i class="fas fa-trash"></i> ' + trans('app.button_delete') + '</button>';

                user += '</td></tr>';

                if (state === "add")
                    jQuery('#users-list').append(user);
                else
                    $("#name" + id_user).replaceWith(user);

                jQuery('#modalFormData').trigger("reset");
                jQuery('#userEditorModal').modal('hide')
            },
            error: function (data)
            {
                let response = $.parseJSON(data.responseText);

                // Erreur sur le nom d'utilisateur
                if (response.messages.name)
                    $('#errorMessageName').html(response.messages.name[0]).show();

                // Erreur sur l'adresse mail
                if (response.messages.email)
                    $('#errorMessageEmail').html(response.messages.email[0]).show();

                // Erreur sur le mot de passe
                let messagePassword = '';
                $.each(response.messages.password, function (k,v) {
                    messagePassword += v + '<br/>';
                });

                $('#errorMessagePassword').html(messagePassword).show();
            }
        });
    });

    ////----- DELETE a user and remove from the page -----////
    jQuery('body').on('click', '.delete-user', function ()
    {
        if (confirm(trans('app.confirm_delete')))
        {
            let id_user = $(this).val();

            $.ajax({
                type: "DELETE",
                url: 'user/' + id_user,
                data: {
                    "_token": $('input[name="_token"]').val(),
                },
                success: function (data) {
                    $("#name" + id_user).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });
});
