
/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function resourceDelete(sends_id, resource_id)
{
    if (confirm('Voulez vous vraiment supprimer définitivement cette resource ?'))
        $.get(route('resource.delete.ajax', [sends_id, resource_id]))
            .done(function (result)
            {
                if (result > 0)
                    $('#resource_list_' + resource_id).remove()
                else
                    document.location.href = route('admin.sends.list');
            })
    else
        return false
}

