
jQuery(document).ready(function($)
{


    ////----- Open the modal to CREATE a role -----////
    jQuery('#btn-add').click(function ()
    {
        resetMessageErrorForm();
        jQuery('#btn-save').val("add");
        jQuery('#modalFormData').trigger("reset");
        jQuery('#roleEditorModal').modal('show');
    });

    ////----- Open the modal to UPDATE a role -----////
    jQuery('body').on('click', '.open-modal', function ()
    {
        let id_role = $(this).val();
        $.get('role/' + id_role, function (data) {
            resetMessageErrorForm();
            jQuery('#id_role').val(data.id);
            jQuery('#name').val(data.name);
            jQuery('#btn-save').val("update");
            jQuery('#roleEditorModal').modal('show');
        })
    });

    // Clicking the save button on the open modal for both CREATE and UPDATE
    $("#btn-save").click(function (e)
    {
        resetMessageErrorForm();
        e.preventDefault();

        let type = "POST";
        let ajaxurl = 'role';
        let formData = { name: jQuery('#name').val(), "_token": $('input[name="_token"]').val() };
        let state = jQuery('#btn-save').val();
        let id_role = jQuery('#id_role').val();

        if (state === "update")
        {
            type = "PUT";
            ajaxurl = 'role/' + id_role;
        }

        $.ajax({
            type: type,
            url: ajaxurl,
            data: formData,
            dataType: 'json',
            success: function (data)
            {
                let role =
                '<tr id="name' + data.id + '">' +
                    '<td class="text-center">' + data.id + '</td>' +
                    '<td>' + data.name + '</td>' +
                    '<td class="text-left">' +
                        '<button class="btn btn-xs btn-info open-modal" value="' + data.id + '">' +
                            '<i class="fas fa-pen"></i> ' + trans('app.button_edit') + '</button>';

                if (data.id > 3)
                    role += '&nbsp;<button class="btn btn-xs btn-danger delete-role" value="' + data.id + '" role="button">' +
                                '<i class="fas fa-trash"></i> ' + trans('app.button_delete') + '</button>';

                role += '</td></tr>';

                if (state === "add")
                    jQuery('#roles-list').append(role);
                else
                    $("#name" + id_role).replaceWith(role);

                jQuery('#modalFormData').trigger("reset");
                jQuery('#roleEditorModal').modal('hide')
            },
            error: function (data)
            {
                let response = $.parseJSON(data.responseText);
                $('#errorMessageName').html(response.messages.name[0]).show();
            }
        });
    });

    ////----- DELETE a role and remove from the page -----////
    jQuery('body').on('click', '.delete-role', function ()
    {
        if (confirm(trans('app.confirm_delete')))
        {
            let id_role = $(this).val();

            $.ajax({
                type: "DELETE",
                url: 'role/' + id_role,
                data: {
                    "_token": $('input[name="_token"]').val(),
                },
                success: function (data) {
                    $("#name" + id_role).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });
});
