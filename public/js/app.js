
/*
|-----------------------------------------------------------------------------------------------------------------------------------------------------
| Sélection automatique du menu et ouverture du menu déroulant de la sidebar
|-----------------------------------------------------------------------------------------------------------------------------------------------------
|
| Récuperation de l'url de la page courante et extraction du nom de la page
| Reset des classes sur les balises actives
| Ajout de classes sur les balises du lien de la page concernée
|
*/
function activeLink(page)
{
    $('li[class="nav-item active"], a[class="collapse-item active"]').removeClass('active');
    $('div[class="collapse show"]').removeClass('show');

    let link = $('a[href$="' + page + '"]');

    link.addClass('active')
    link.parent().parent().addClass('show')
    link.parent().parent().parent().parent().addClass('active')
}

let page = document.location.href.match(/[^\/]+$/)[0];
activeLink(page);


/*
|-----------------------------------------------------------------------------------------------------------------------------------------------------
| Utilisation de l'i18n dans la partie JS
|-----------------------------------------------------------------------------------------------------------------------------------------------------
*/
function trans(key, replace = {})
{
    let translation = key.split('.').reduce((t, i) => t[i] || null, window.translations);

    for (let placeholder in replace) {
        translation = translation.replace(`:${placeholder}`, replace[placeholder]);
    }

    return translation;
}


/*
|-----------------------------------------------------------------------------------------------------------------------------------------------------
| Reset des messages d'erreurs affichés sous les champs dans les formulaires
|-----------------------------------------------------------------------------------------------------------------------------------------------------
*/
function resetMessageErrorForm()
{
    $('div[id^="errorMessage"]').html('').hide();
}


/*
|-----------------------------------------------------------------------------------------------------------------------------------------------------
| Rechargement de la liste des villes selon le pays choisi
|-----------------------------------------------------------------------------------------------------------------------------------------------------
*/
function reloadCityList(id_ville = 0)
{
    let id_pays = $('#id_pays').val();

    if (id_pays === '1')
        $('#id_ville').html('<option value="1">---</option>');
    else
    {
        $.get('ville_filter/' + id_pays + '/json', function(data) {
            let liste_ville = '', selected = '';

            $.each(data['villes'], function(k,v)
            {
                selected = (id_ville === v['id']) ? 'selected' : '';

                liste_ville += '<option value="' + v['id'] + '" ' + selected + '>' + v['name'] + (v['code_postal'] ? ' - ' + v['code_postal'] : '') + '</option>';
            });

            $('#id_ville').html(liste_ville);
        });
    }
}
