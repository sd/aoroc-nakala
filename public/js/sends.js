
/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Bloc 1 - choix du projet
 */
function setProjectChoice(choice)
{
    $.get(route('sends.ajax.project_choice', choice))
        .done(function(result){
            $('#p1_project_start, #p1_project').hide()
            $('#p1_project_result').show().find('div')
                .html(`Etape 1 - Projet sélectionné : <strong>${result}</strong><i class="fa fa-check fa-2x float-right"></i>`)
            $('#p2_name').show()
        })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Bloc 2 - définition d'un nom pour l'envoi
 */
function setSendsName()
{
    let name = $('#sends_name').val()
    name = name.replace(/[#"'/\\=*@]/g,'_') // Si ajout d'un caractère ici, ajouter aussi dans config/aoroc_nakala.php

    if (name === '')
    {
        $('.danger_name').html('Erreur, vous devez nommer votre envoi !')
        return
    }

    $.get(route('sends.ajax.sends_name', name))
        .done(function(result){
            if (result === 'error')
            {
                $('.danger_name').html('Erreur, vous devez nommer votre envoi !')
                return
            }

            $('#p2_name_start, #p2_name').hide()
            $('#p2_name_result').show().find('div')
                .html(`Etape 2 - Nom de votre envoi : <strong>${result}</strong><i class="fa fa-check fa-2x float-right"></i>`)
            $('#p3_data').show()
            $('.danger_name').html('')
        })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function checkDataFile()
{
    if ($('#data_file').val() === '')
        $('.danger_data').html('Attention : fichier de données incorrect ou non envoyé !')
    else
    {
        $('#p3_data_result').hide()
        $('#p3_data').submit
        $('.progress_data').show()
    }
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Bloc 3 - envoi du fichier de données en csv ou json
 */
$('#p3_data').ajaxForm(
{
    beforeSend:function()
    {
        $('.progress-bar').text('0%')
        $('.progress-bar').css('width', '0%')
    },
    uploadProgress:function(event, position, total, percentComplete)
    {
        $('.progress-bar').text(percentComplete + '%')
        $('.progress-bar').css('width', percentComplete + '%')
    },
    success:function(data)
    {
        $('.danger_data').html('')

        if (data === 'success')
        {
            let file = $('#data_file').val().split('\\').at(-1)
            $('.progress-bar').text('Uploaded');
            $('.progress-bar').css('width', '100%')
            $('#p3_data_start, #p3_data').hide()
            $('#p3_data_result').show().find('div').removeClass('alert-danger').addClass('alert-success')
                .html(`Etape 3 - Fichier <strong>${file}</strong> envoyé<i class="fa fa-check fa-2x float-right"></i>`)
            $('#p4_media').show()
        }
        else
            $('.danger_data').html('Erreur, format de fichier incorrect ! Vous ne devez envoyer que du CSV ou du JSON')
    }
});


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function checkMediaFile()
{
    if ($('#media_file').val() === '')
        $('.danger_media').html('Attention : vous n\'avez pas sélectionné de fichiers à envoyer !')
    else
    {
        $('#p4_media_result').hide()
        $('#p4_media').submit
    }
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Bloc 4 - envoi des fichiers média image ou pdf
 */
$('#p4_media').ajaxForm(
{
    beforeSend:function()
    {
        $('#p4_wait_message').show()
    },
    uploadProgress:function(event, position, total, percentComplete) {},
    success:function(data)
    {
        $('.danger_media').html('')
        $('#p4_wait_message').hide()

        if (data === 'success')
        {
            $('#p4_media_start, #p4_media, #p4_wait_message').hide()
            $('#p4_media_result').show().find('div').removeClass('alert-danger').addClass('alert-success')
                .html('Etape 4 - Fichier(s) envoyé(s)<i class="fa fa-check fa-2x float-right"></i>')
            $('#p5_status').show()
        }
        else
            $('.danger_media').html('Erreur, format de fichier incorrect ! Veuillez respecter les formats requis')
    }
});


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Bloc 5 - Enregistrement du statut d'envoi
 */
function setSendsStatus(choice)
{
    $.get(route('sends.ajax.sends_status', choice))
        .done(function(result)
        {
            document.location.href = route('sends.list')
        })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function deleteSends(sends_id)
{
    if (confirm('Voulez vous vraiment supprimer définitivement cet envoi ?'))
        $.get(route('sends.ajax.delete', sends_id))
            .done(function(result)
            {
                $('#sends_' + sends_id).remove()
            })
    else
        return false
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Envoi des ressources depuis la page sends_list
 */
function sendResources(sends_id)
{
    $('#btn_send_resource_' + sends_id).html('<span class="small text-danger"><i class="fa fa-sync fa-spin"></i><br/>Envoi en cours</span>')

    $.get(route('resource.send_nakala.ajax', sends_id))
        .done(function (result)
        {
            document.location.href = './list'
        })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Modification du statut d'envoi - public.sends.sends_status_users
 */
function changeSendsStatus(sends_id, status)
{
    $.get(route('sends.ajax.change_status', [sends_id, status]))
        .done(function (result)
        {
            let btns = (status === 'published')
                ? `<button type="button" class="btn btn-sm btn-dark" disabled>Public</button>
                   <button type="button" class="btn btn-sm btn-outline-dark" onclick="changeSendsStatus(${sends_id}, 'pending')">Privé</button>`
                : `<button type="button" class="btn btn-sm btn-outline-dark" onclick="changeSendsStatus(${sends_id}, 'published')">Public</button>
                   <button type="button" class="btn btn-sm btn-dark" disabled>Privé</button>`

            $('#sends_status_' + sends_id).html(btns)
        })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Modification du statut d'envoi - public.sends.sends_status_users
 */
function changeSendsStatusOnNakala(sends_id)
{
    $.get(route('sends.ajax.change_status_on_nakala', sends_id))
        .done(function (result)
        {
            document.location.href = './list'
        })
}


/** --------------------------------------------------------------------------------------------------------------------------------------------------
 * Envoi des ressources depuis la page sends_list
 */
function deleteResources(sends_id)
{
    $.get(route('resource.delete_nakala.ajax', sends_id))
        .done(function (result)
        {
            document.location.href = './list'
        })
}

