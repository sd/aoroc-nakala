<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\SendsController;
use App\Http\Controllers\ResourceController;
use App\Http\Controllers\ResourceSendController;
use App\Http\Controllers\UpdateMediaFileController;
use App\Http\Controllers\DeleteController;
use App\Http\Controllers\SendsErrorController;


Auth::routes([
    'login' => true,
    'logout' => true,
    //'register' => false,
    'reset' => true,
    'confirm' => false,
    'verify' => false,
]);

//Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');


// ---------------------------------------------------------------------------------------------------------------------------------------------------
// Visu et gestion des envois
Route::get('/sends', [SendsController::class, 'index'])->name('sends.index');
Route::get('/sends/list', [SendsController::class, 'sendsList'])->name('sends.list');
Route::get('/sends/ajax/change_status/{sends_id}/{status}', [SendsController::class, 'changeSendsStatus'])->name('sends.ajax.change_status');
Route::get('/sends/ajax/change_status_on_nakala/{sends_id}', [SendsController::class, 'changeSendsStatusOnNakala'])->name('sends.ajax.change_status_on_nakala');

// ---------------------------------------------------------------------------------------------------------------------------------------------------
// Gestion des lignes en erreur
Route::get('/sends/error', [SendsErrorController::class, 'sendsListError'])->name('sends.error');
Route::get('/resource/error/{sends_id}', [SendsErrorController::class, 'resourcesListError'])->name('resource.error');
Route::post('/resource/update', [SendsErrorController::class, 'updateLine'])->name('resource.update');

// ---------------------------------------------------------------------------------------------------------------------------------------------------
// Envoi de données
Route::prefix('sends')->group(function () {
    Route::get('ajax/project_choice/{choice}', [SendsController::class, 'setProjectChoice'])->name('sends.ajax.project_choice');
    Route::get('ajax/sends_name/{name}', [SendsController::class, 'setSendsName'])->name('sends.ajax.sends_name');
    Route::post('ajax/upload_data_file', [SendsController::class, 'uploadDataFile'])->name('sends.ajax.upload_data_file');
    Route::post('ajax/upload_media_file', [SendsController::class, 'uploadMediaFile'])->name('sends.ajax.upload_media_file');
    Route::get('ajax/sends_status/{choice}', [SendsController::class, 'setSendsStatus'])->name('sends.ajax.sends_status');
    Route::get('ajax/delete/{sends_id}', [DeleteController::class, 'sendsDelete'])->name('sends.ajax.delete');
    Route::post('ajax/update_media_file', [UpdateMediaFileController::class, 'updateMediaFile'])->name('sends.ajax.update_media_file');
});

Route::prefix('resource')->group(function () {
    Route::get('list/{sends_id}', [ResourceController::class, 'resourceList'])->name('resource.list');
    Route::get('detail/ajax/{resource_id}/{project}', [ResourceController::class, 'resourceDetail'])->name('resource.detail.ajax');
    Route::get('delete/ajax/{sends_id}/{resource_id}', [DeleteController::class, 'resourceDelete'])->name('resource.delete.ajax');
    Route::get('update/ajax/{resource_id}/{field}/{value}', [ResourceController::class, 'resourceUpdate'])->name('resource.update.ajax');
    Route::get('update/all/ajax/{sends_id}/{field}/{value}', [ResourceController::class, 'resourceUpdateAllLine'])->name('resource.update.all.ajax');
    Route::get('send_nakala/ajax/{sends_id}', [ResourceSendController::class, 'getResourceToSend'])->name('resource.send_nakala.ajax');
    Route::get('delete_nakala/ajax/{sends_id}', [ResourceSendController::class, 'deleteResourceFromNakala'])->name('resource.delete_nakala.ajax');
    Route::get('ajax/change_status/{resource_id}', [ResourceController::class, 'updateResourceStatus'])->name('resource.change_status.ajax');
});

// ---------------------------------------------------------------------------------------------------------------------------------------------------
// Liste des envois et ressources en mode admin
Route::get('/admin/sl', [SendsController::class, 'adminSendsList'])->name('admin.sends.list');
Route::get('/admin/rl/{sends_id}', [ResourceController::class, 'adminResourceList'])->name('admin.resource.list');

// ---------------------------------------------------------------------------------------------------------------------------------------------------
// Gestion du profil
Route::get('/profil', [ProfilController::class, 'show'])->name('profil.show');
Route::get('/profil/edit', [ProfilController::class, 'edit'])->name('profil.edit');
Route::get('/profil/update/{api_key}', [ProfilController::class, 'update'])->name('profil.update');

// ---------------------------------------------------------------------------------------------------------------------------------------------------
// Gestion des rôles
Route::get('/role', 'Admin\RoleController@list')->name('role.list');
Route::post('/role', 'Admin\RoleController@create')->name('role.create');
Route::get('/role/{id_role?}', 'Admin\RoleController@edit')->name('role.edit');
Route::put('/role/{id_role?}', 'Admin\RoleController@update')->name('role.update');
Route::delete('/role/{id_role?}', 'Admin\RoleController@delete')->name('role.delete');

// ---------------------------------------------------------------------------------------------------------------------------------------------------
// Gestion des users
Route::get('/user', 'Admin\UserController@list')->name('user.list');
Route::post('/user', 'Admin\UserController@create')->name('user.create');
Route::get('/user/{id_user?}', 'Admin\UserController@edit')->name('user.edit');
Route::put('/user/{id_user?}', 'Admin\UserController@update')->name('user.update');
Route::delete('/user/{id_user?}', 'Admin\UserController@delete')->name('user.delete');
