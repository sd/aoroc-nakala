
@include('layouts.header')

@auth
    @include('layouts.sidebar', ['id_role' => Auth::user()->id_role])
@endauth

<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">

        @auth
            @include('layouts.navbartop', ['page_name' => 'Liste de vos envois'])
        @endauth

        <div class="container-fluid">

            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <a class="btn btn-sn btn-outline-success" href="{{ route('sends.index') }}">Envoyer un lot de données</a>
            </div>

            <table class="table table-sm">
                <thead>
                    <tr class="small">
                        <th scope="col" class="col-5">NOM / TYPE / date de dépôt sur importNakala</th>
                        <th scope="col" class="col-1 text-center">STATUT</th>
                        <th scope="col" class="col-2 text-center">ÉTAT</th>
                        <th scope="col" class="col-1 text-center">ACTION</th>
                        <th scope="col" class="col-1 text-center">DONNÉES(S)</th>
                        <th scope="col" class="col-1 text-center"></th>
                        <th scope="col" class="col-1 text-center">SUPPRIMER</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sends_list as $sends)
                    <tr id="sends_{{ $sends->sends_id }}">
                        <td>
                            <strong class="text-dark">{{ $sends->name }}</strong><br/>
                            <span class="badge badge-success">ID {{ $sends->sends_id }}</span>
                            <span class="badge @if($sends->project === 'nahan') badge-primary @else badge-secondary @endif" style="font-weight:0;">
                                Projet : <span class="text-uppercase">{{ $sends->project }}</span>
                            </span>
                            <span class="small">Déposé le {{ $sends->created_at }}</span><br/>
                        </td>

                        <td class="text-center align-middle">
                            @if($sends->resources_on_nakala > 0 and $sends->sends_status_users === 'pending')
                                {{-- Ressources en ligne sur NAKALA et statut en privé (pending) : affichage bouton de switch du statut en ligne --}}
                                <div class="btn-group" role="group" id="sends_status_{{ $sends->sends_id }}">
                                    <button type="button" class="btn btn-sm btn-outline-dark"
                                            onclick="changeSendsStatusOnNakala({{ $sends->sends_id }})">Public</button>
                                    <button type="button" class="btn btn-sm btn-dark" disabled>Privé</button>
                                </div>
                            @elseif($sends->resources_on_nakala > 0 and $sends->sends_status_users === 'published')
                            @else
                                {{-- Ressources non présentes sur NAKALA : affichage du bouton de switch du statut en local --}}
                                <div class="btn-group" role="group" id="sends_status_{{ $sends->sends_id }}">
                                    @if($sends->sends_status_users === 'published')
                                        <button type="button" class="btn btn-sm btn-dark" disabled>Public</button>
                                        <button type="button" class="btn btn-sm btn-outline-dark"
                                                onclick="changeSendsStatus({{ $sends->sends_id }}, 'pending')">Privé</button>
                                    @else
                                        <button type="button" class="btn btn-sm btn-outline-dark"
                                                onclick="changeSendsStatus({{ $sends->sends_id }}, 'published')">Public</button>
                                        <button type="button" class="btn btn-sm btn-dark" disabled>Privé</button>
                                    @endif
                                </div>
                            @endif
                        </td>

                        <td class="text-center align-middle">
                            <span id="btn_send_resource_{{ $sends->sends_id }}">
                                @if($sends->statut == 'er')
                                    <span class="small text-danger"><i class="fa fa-exclamation-triangle"></i><br/>Il y a des erreurs</span>
                                @elseif($sends->statut == 'wt' or $sends->statut == 'up')
                                    <span class="small text-dark">En attente d'envoi</span>
                                @elseif($sends->statut == 'po')
                                    <span class="small text-success"><i class="fa fa-thumbs-up"></i><br/>Posté sur NAKALA</span>
                                @else
                                    <span class="small text-success"><i class="fa fa-thumbs-up"></i><br/>Publié sur NAKALA</span>
                                @endif
                            </span>
                        </td>

                        <td class="text-center align-middle">
                            <span id="btn_send_resource_{{ $sends->sends_id }}">
                                @if($sends->statut == 'er')
                                    <span class="small text-danger"><i class="fa fa-exclamation-triangle"></i><br/>Il y a des erreurs</span>
                                @elseif($sends->statut == 'wt' or $sends->statut == 'up')
                                    <button class="btn btn-sm btn-outline-success btn-block"
                                            onclick="sendResources({{ $sends->sends_id }})">
                                        <small><i class="fa fa-arrow-right"></i> Déposer<br/>sur NAKALA</small>
                                    </button>
                                @elseif($btn_delete_on_nakala[$sends->sends_id] === 'ok')
                                    <button class="btn btn-sm btn-outline-danger btn-block"
                                            onclick="deleteResources({{ $sends->sends_id }})" id="btn_delete_resource_from_nakala_{{ $sends->sends_id }}">
                                        <small>Supprimer<br/>de NAKALA</small>
                                    </button>
                                @else
                                @endif
                            </span>
                        </td>

                        <td class="text-center align-middle">{{ $sends->nb_sends_line }}</td>

                        <td class="text-center align-middle">
                            <a class="btn btn-sm btn-outline-dark btn-block" href="{{ route('resource.list', ['sends_id' => $sends->sends_id]) }}">
                                <small><i class="fa fa-pen"></i> Détails<br/>de ce lot</small>
                            </a>
                        </td>

                        <td class="text-center align-middle">
                            <button class="btn btn-sm btn-outline-danger btn-block" onclick="deleteSends({{ $sends->sends_id }})">
                                <small><i class="fa fa-times fa-fw"></i> Supprimer<br/>ce lot</small>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @include('layouts.footerbar')

</div>

@include('layouts.footer', ['page' => 'sends_list'])
