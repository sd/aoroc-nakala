
@include('layouts.header')

@auth
    @include('layouts.sidebar', ['id_role' => Auth::user()->id_role])
@endauth

<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">

        @auth
            @include('layouts.navbartop', ['page_name' => 'Profil'])
        @endauth

        <div class="container-fluid">
            <div id="message" class="alert" role="alert" style="display: none"></div>

            <div class="card shadow mb-4">
                <div class="card-body">
                    <h1 class="h5">{{ $user->firstname . ' ' . $user->lastname }}</h1>

                    <p>
                        <strong>@lang('app.profil.label_email')</strong> : {{ $user->email }}<br/>
                        <strong>@lang('app.profil.label_role')</strong> : {{ $user->role }}<br/>
                    </p>

                    <button id="btn_show_profil" class="btn btn-outline-primary btn-xs" type="button" onclick="showFormEdit()">
                        <i class="fas fa-key"></i> Enregistrer / mettre à jour votre clé d'API Nakala
                    </button>

                    <div id="form_profil" style="display: none">
                        <hr>
                        <form>
                            <div class="form-group row">
                                <label for="api_key" class="col-sm-2 col-form-label">Clé d'API Nakala</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-sm" id="api_key" placeholder="La clé doit contenir 36 caractères" maxlength="36">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="button" class="btn btn-xs btn-outline-success" onclick="saveProfil()"><i class="fas fa-save"></i> Enregistrer</button>
                                    <button type="button" class="btn btn-xs btn-outline-danger" onclick="cancelShowFormEdit()"><i class="fas fa-times"></i> Annuler</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('layouts.footerbar')

</div>

@include('layouts.footer', ['page' => 'profil'])
