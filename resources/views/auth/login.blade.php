
@include('layouts.header')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-6 col-lg-8 col-md-7">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">{{ config('app.name', 'AOROC - NAKALA') }}</h1>
                                </div>

                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-user @error('email') is-invalid @enderror"
                                               id="email" aria-describedby="emailHelp" placeholder="Adresse email"
                                               name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <input type="password" id="password" class="form-control form-control-user @error('password') is-invalid @enderror"
                                               name="password" required autocomplete="current-password" placeholder="Mot de passe">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                        @enderror
                                    </div>

                                    <!--<div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <input type="checkbox" class="form-check-input custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="customCheck">{{ __('Remember Me') }}</label>
                                        </div>
                                    </div>-->

                                    <button type="submit" class="btn btn-primary btn-user btn-block">{{ __('passwords.login') }}</button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">{{ __('passwords.forgot_password') }}</a>
                                    @endif

                                </form>

                                <hr>

                                <div class="small">
                                    L'utilisation de cette application nécessite de disposer d'un compte Nakala et d'une clé d'API valide. Votre navigateur doit également être à jour.<br/><br/>
                                    Pour toute nouvelle inscription ou question, merci de contacter Agnès Tricoche
                                    (<a href="./mailto.php">agnes.tricoche@ens.fr</a>), administratrice de ce site.
                                </div>

                            <!--<div class="text-center">
                                    <a class="small" href="{{ route('register') }}">Create an Account!</a>
                                </div>-->
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@include('layouts.footer', ['page' => ''])
