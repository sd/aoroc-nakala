
@include('layouts.header')

@auth
    @include('layouts.sidebar', ['id_role' => Auth::user()->id_role])
@endauth

<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">

        @auth
            @include('layouts.navbartop', ['page_name' => 'Home'])
        @endauth

        <div class="container-fluid">
            @if(!Auth::user()->api_key)
            <div class="alert alert-primary" role="alert">
                L'utilisation de cette application nécessite de disposer d'un compte Nakala et d'une clé d'API valide. Votre navigateur doit également être à jour.<br/>
                Vous n'avez pas encore renseigné votre clé, <a href="{{ route('profil.show') }}">vous pouvez le faire sur la page profil.</a>
            </div>
            @endif

            <div class="card-deck mb-3 text-center">
                <div class="card mb-4 box-shadow">
                    <div class="card-body">
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>Envoyez vos fichiers de données et vos images</li>
                            <li>&nbsp;</li>
                        </ul>
                        <a type="button" class="btn btn-lg btn-block btn-outline-primary" href="{{ route('sends.index') }}">Envoyer des données</a>
                    </div>
                </div>
                <div class="card mb-4 box-shadow">
                    <div class="card-body">
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>Voir les envois déjà effectués</li>
                            <li>Gérer les envois (suppression et modification)</li>
                        </ul>
                        <a type="button" class="btn btn-lg btn-block btn-outline-primary" href="{{ route('sends.list') }}">Voir / gérer vos envois</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @include('layouts.footerbar')

</div>

@include('layouts.footer', ['page' => ''])
