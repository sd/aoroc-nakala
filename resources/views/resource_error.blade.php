
@include('layouts.header')

@auth
    @include('layouts.sidebar', ['id_role' => Auth::user()->id_role])
@endauth

<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">

        @auth
            @include('layouts.navbartop', ['page_name' => 'Liste des données'])
        @endauth

        <div class="container-fluid">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">{{ $resources[0]['sends_name'] }}</h1>
                <a class="btn btn-sn btn-outline-info" href="{{ route('sends.error') }}">Retour à la liste des lignes en erreur</a>
            </div>

            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th scope="col" class="col-1 text-center"></th>
                        <th scope="col" class="col-11 text-center">Titre</th>
                    </tr>
                </thead>

                <tbody>
                @foreach($resources as $resource)
                    <tr id="csv_line_{{ $resource['csv_id'] }}">
                        <td class="text-center">
                            {{--<button class="btn btn-xs btn-outline-primary" onclick="resourceView({{ $resource['csv_id'] }})">
                                <i class="fa fa-pen"></i> Editer
                            </button>--}}
                            #{{ $resource['csv_id'] }}
                        </td>
                        <td style="font-size: 1em">
                            <form id="csv_id_{{ $resource['csv_id'] }}">
                            @foreach($resource['data'] as $line)
                                @if($loop->index <= count($fields) - 1)

                                    <strong class="">{{ $fields[$loop->index] }}</strong> :
                                    {{--<span class="@if($check[$loop->index] === '1')text-danger @else text-success @endif">{{ $fields[$loop->index] }}</span> :--}}
                                    {{--{{ $check[$loop->index] }}--}}

                                    @switch($fields[$loop->index])
                                        @case('dcterms_description')
                                        @case('dcterms_bibliographicCitation')
                                            <textarea id="{{ $fields[$loop->index] }}_{{ $resource['csv_id'] }}" rows="3"
                                                      class="form-control form-control-sm @if($check[$loop->index] === '1')alert-danger @else alert-primary @endif">{{ $line }}
                                            </textarea>
                                            @break
                                        @default
                                            <input id="{{ $fields[$loop->index] }}_{{ $resource['csv_id'] }}" value="{{ $line }}"
                                                   class="form-control form-control-sm @if($check[$loop->index] === '1')alert-danger @else alert-primary @endif">
                                    @endswitch
                                @else
                                    <div class="alert alert-info mt-2 mb-0">
                                        <strong class="text-dark">Données hors limite</strong> :<br>
                                        <span class="text-primary">{{ $line }}</span>
                                    </div>
                                @endif
                            @endforeach
                            </form>

                            <br>
                            <button class="btn btn-xs btn-success" onclick="updateLine({{ $resource['csv_id'] }})">
                                <i class="fa fa-save"></i> Enregistrer les modifications
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @include('layouts.footerbar')

</div>

@include('layouts.footer', ['page' => 'resource'])
