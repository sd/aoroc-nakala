
@include('layouts.header')

@auth
    @include('layouts.sidebar', ['id_role' => Auth::user()->id_role])
@endauth

<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">

        @auth
            @include('layouts.navbartop', ['page_name' => 'Liste des données'])
        @endauth

        <div class="container-fluid">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <input type="hidden" id="sends_id" value="{{ $sends_id }}">
                <input type="hidden" id="project" value="{{ $resource_list[0]->project }}">
                <h1 class="h3 mb-0 text-gray-800">{{ $resource_list[0]->sends_name }}</h1>
                <a class="btn btn-sn btn-outline-info" href="{{ route('sends.list') }}">Retour à la liste de vos envois</a>
            </div>

            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <td colspan="3" class="text-center">
                            <div class="input-group input-group-sm">
                                <select class="custom-select form-control form-control-sm col-4" id="field">
                                    <option value="x" selected>--[ Choisissez le champ à modifier ]--</option>
                                    @foreach($updateable_fields as $field)
                                        <option value="{{ $field }}">{{ $field }}</option>
                                    @endforeach
                                </select>
                                <input type="text" class="form-control form-control-sm" placeholder="Saisissez ici la nouvelle valeur" id="value">
                                <div class="input-group-append">
                                    <button class="btn btn-sm btn-outline-success" onclick="saveUpdateableField()">Valider</button>
                                </div>
                            </div>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-xs btn-outline-success p-1" onclick="sendResources({{ $sends_id }})" id="btn_send_resource">
                                <i class="fa fa-arrow-right"></i> Publier vos données sur NAKALA
                            </button>
                            <br/><span class="" id="message"></span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col" class="col-1"></th>
                        <th scope="col" class="col-1"></th>
                        <th scope="col" class="col-8 text-center">Titre</th>
                        <th scope="col" class="col-2 text-center">Type de doc</th>
                    </tr>
                </thead>

                <tbody>
                @foreach($resource_list as $resource)
                    <tr id="resource_list_{{ $resource->id }}">
                        <td class="text-center">
                            <button class="btn btn-xs btn-outline-primary" onclick="resourceView({{ $resource->id }}, '{{ $resource->project }}')">
                                <i class="fa fa-pen"></i> Editer
                            </button>
                        </td>

                        <td class="text-center">
                            @php
                                $r = explode('|', $resource->status_text)
                            @endphp
                            <span id="status_{{ $resource->id }}" class="badge badge-{{ $r[1] }}">{{ $r[0] }}</span>
                            @if($resource->nakala_id)
                                <br/>
                                <a href="{{ $nakala_url }}/{{ $resource->nakala_id }}" target="_blank">
                                    <img src="{{ asset('images/nakala.png') }}" width="25" alt="NAKALA" title="Voir la ressource sur NAKALA"></img>
                                </a>
                            @endif
                            @if($resource->status == 'sp')
                                <button class="btn btn-xs btn-outline-danger text-center" onclick="changeResourceStatus({{ $resource->id }})"
                                        id="btn_chg_status_{{ $resource->id }}">
                                    <span class="fa fa-exclamation-triangle"></span><br>
                                    Repasser le statut à<br> "en attente"
                                </button>
                            @endif
                        </td>

                        <td>
                            <span class="badge badge-info mb-2">{{ $resource->files }}</span><br>
                            {{ $resource->nk_title }}
                            @if($resource->description)
                            <ul class="pl-3">
                                <li><span class="small">{{ $resource->description }}</span></li>
                            </ul>
                            @endif
                            @if($resource->status == 'er')
                                @php
                                    /*$errors = explode(';', substr($resource->error, 0, -1));

                                    foreach($errors as $v)
                                    {
                                        $error = explode('|', $v);
                                        echo '<span class="badge badge-pill badge-danger">' . $error[1] . '</span>';
                                    }*/
                                @endphp
                            @endif
                        </td>

                        <td class="text-center">{{ $resource->type }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @include('layouts.footerbar')

</div>

@include('layouts.footer', ['page' => 'resource'])
