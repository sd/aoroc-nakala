
@include('layouts.header')

@auth
    @include('layouts.sidebar', ['id_role' => Auth::user()->id_role])
@endauth

<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">

        @auth
            @include('layouts.navbartop', ['page_name' => 'Liste des envois avec des lignes en erreur'])
        @endauth

        <div class="container-fluid">

            {{--<div class="d-sm-flex align-items-center justify-content-between mb-4">
                <a class="btn btn-sn btn-outline-success" href="{{ route('sends.index') }}">Envoyer un lot de données</a>
            </div>--}}

            <table class="table table-sm">
                <thead>
                    <tr class="small">
                        <th scope="col" class="col-5">NOM / TYPE / date de dépôt sur importNakala</th>
                        <th scope="col" class="col-1 text-center">STATUT</th>
                        <th scope="col" class="col-1 text-center">LIGNES</th>
                        <th scope="col" class="col-1 text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sends_list as $sends)
                    <tr id="sends_{{ $sends->sends_id }}">
                        <td>
                            <strong class="text-dark">{{ $sends->name }}</strong><br/>
                            <span class="badge @if($sends->project === 'nahan') badge-primary @else badge-secondary @endif" style="font-weight:0;">
                                Projet : <span class="text-uppercase">{{ $sends->project }}</span>
                            </span>
                            <span class="small">Déposé le {{ $sends->created_at }}</span><br/>
                        </td>

                        <td class="text-center align-middle">
                            @if($sends->sends_status_users === 'pending')
                                {{-- Ressources en ligne sur NAKALA et statut en privé (pending) : affichage bouton de switch du statut en ligne --}}
                                <div class="btn-group" role="group" id="sends_status_{{ $sends->sends_id }}">
                                    <button type="button" class="btn btn-sm btn-dark" disabled>Privé</button>
                                </div>
                            @else
                                {{-- Ressources non présentes sur NAKALA : affichage du bouton de switch du statut en local --}}
                                <div class="btn-group" role="group" id="sends_status_{{ $sends->sends_id }}">
                                    @if($sends->sends_status_users === 'published')
                                        <button type="button" class="btn btn-sm btn-dark" disabled>Public</button>
                                    @else
                                        <button type="button" class="btn btn-sm btn-dark" disabled>Privé</button>
                                    @endif
                                </div>
                            @endif
                        </td>

                        <td class="text-center align-middle">{{ $sends->nb_sends_line }}</td>

                        <td class="text-center align-middle">
                            <a class="btn btn-sm btn-outline-dark btn-block" href="{{ route('resource.error', ['sends_id' => $sends->sends_id]) }}">
                                <small><i class="fa fa-pen"></i> Voir<br/>les lignes</small>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @include('layouts.footerbar')

</div>

@include('layouts.footer', ['page' => 'sends_list'])
