
@include('layouts.header')

@auth
    @include('layouts.sidebar', ['id_role' => Auth::user()->id_role])
@endauth

<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">

        @auth
            @include('layouts.navbartop', ['page_name' => 'Envoi'])
        @endauth

        <div class="container-fluid">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <input type="hidden" id="sends_id" value="{{ $sends_id }}">
                <input type="hidden" id="project" value="{{ $resource_list[0]->project }}">
                <h1 class="h3 mb-0 text-gray-800">{{ $resource_list[0]->sends_name }}</h1>
                <a class="btn btn-sn btn-outline-info" href="{{ route('admin.sends.list') }}">Retour à la liste des envois</a>
            </div>

            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th scope="col" class="col-1"></th>
                        <th scope="col" class="col-1"></th>
                        <th scope="col" class="col-7 text-center">Titre</th>
                        <th scope="col" class="col-2 text-center">Type de doc</th>
                        <th scope="col" class="col-1"></th>
                    </tr>
                </thead>

                <tbody>
                @foreach($resource_list as $resource)
                    <tr id="resource_list_{{ $resource->id }}">
                        <td class="text-center">
                            <button class="btn btn-xs btn-outline-primary" onclick="resourceView({{ $resource->id }}, '{{ $resource->project }}')"><i class="fa fa-pen"></i> Editer</button>
                        </td>
                        <td class="text-center">
                            @php
                                $r = explode('|', $resource->status_text)
                            @endphp
                            <span href="#" class="badge badge-{{ $r[1] }}">{{ $r[0] }}</span>
                        </td>
                        <td>
                            <ul class="pl-3">
                                <li>{{ $resource->nk_title }}</li>
                                <li><span class="small">{{ $resource->description }}</span></li>
                            </ul>
                            @if($resource->status == 'er')
                                @php
                                    $errors = explode(';', substr($resource->error, 0, -1));

                                    foreach($errors as $v)
                                    {
                                        $error = explode('|', $v);
                                        echo '<span class="badge badge-pill badge-danger">' . $error[1] . '</span>';
                                    }
                                @endphp
                            @endif
                        </td>
                        <td class="text-center">{{ $resource->type }}</td>
                        <td class="text-center">
                            <button class="btn btn-xs btn-outline-danger" onclick="resourceDelete({{ $sends_id }}, {{ $resource->id }})"><i class="fa fa-trash"></i> Supprimer</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @include('layouts.footerbar')

</div>

@include('layouts.footer', ['page' => 'admin_resource'])
