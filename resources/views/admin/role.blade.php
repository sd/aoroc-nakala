
@include('layouts.header')

@auth
    @include('layouts.sidebar', ['id_role' => Auth::user()->id_role])
@endauth

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        @include('layouts.navbartop', ['page_name' => 'Gestion des rôles'])

        <div class="container-fluid">

            <!--<div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h4 mb-3 text-gray-800">Gestion des rôles</h1>
            </div>-->

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    <button id="btn-add" name="btn-add" class="btn btn-sm btn-outline-success"><i class="fas fa-plus"></i> Ajouter un rôle</button>
                </div>

                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="col-1 text-center">#</th>
                                    <th class="col-10">Rôles</th>
                                    <th class="col-1"></th>
                                </tr>
                            </thead>
                            <tbody id="roles-list" name="roles-list">
                                @foreach ($roles as $role)
                                <tr id="name{{$role->id}}">
                                    <td class="text-center">{{$role->id}}</td>
                                    <td>{{$role->name}}</td>
                                    <td class="text-center">
                                        <button class="btn btn-xs btn-outline-primary open-modal" value="{{$role->id}}"><i class="fas fa-pen"></i> Editer</button>
                                        {{--@if($role->id > 3)
                                        <button class="btn btn-xs btn-outline-danger delete-role" value="{{$role->id}}"><i class="fas fa-trash"></i> Supprimer</button>
                                        @endif--}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="modal fade" id="roleEditorModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="roleEditorModalLabel">Ajout / Edition d'un rôle</h4>
                                </div>
                                <div class="modal-body">

                                    <form id="modalFormData" name="modalFormData" class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Nom du rôle" value="">
                                            </div>
                                        </div>
                                        <div class="form-group text-danger pl-3 messageErrorForm" style="display: none;" id="errorMessageName"></div>
                                    </form>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-xs btn-outline-primary" id="btn-save" value="add">Enregistrer</button>
                                    <button type="button" class="btn btn-xs btn-outline-danger" id="btn-save" onclick="$('#roleEditorModal').modal('hide');">Annuler</button>
                                    <input type="hidden" id="id_role" name="id_role" value="0">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- End of Main Content -->

    @include('layouts.footerbar')

</div>
<!-- End of Content Wrapper -->

@include('layouts.footer', ['page' => 'role'])
