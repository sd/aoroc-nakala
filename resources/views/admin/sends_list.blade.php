
@include('layouts.header')

@auth
    @include('layouts.sidebar', ['id_role' => Auth::user()->id_role])
@endauth

<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">

        @auth
            @include('layouts.navbartop', ['page_name' => 'Liste des envois'])
        @endauth

        <div class="container-fluid">

            <table class="table table-sm table-bordered table-hover">
                <thead>
                    <tr>
                        <th scope="col" class="col-1 text-center"></th>
                        <th scope="col" class="col-2 text-center">Utilisateur</th>
                        <th scope="col" class="col-5">Envoi</th>
                        <th scope="col" class="col-2 text-center">Statuts de vos données</th>
                        <th scope="col" class="col-1 text-center">Données</th>
                        <th scope="col" class="col-1 text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sends_list as $sends)
                    <tr id="sends_{{ $sends->sends_id }}">
                        <td class="text-center">
                            <a class="btn btn-xs btn-outline-primary" href="{{ route('admin.resource.list', ['sends_id' => $sends->sends_id]) }}">
                                <i class="fa fa-pen"></i> Détails
                            </a>
                        </td>
                        <td>{{ $sends->user }}</td>
                        <td>
                            <span class="badge @if($sends->project === 'nahan') badge-dark @else badge-primary @endif">projet : {{ $sends->project }}</span>
                            <span class="badge badge-dark">données déposées le {{ $sends->created_at }}</span><br/>
                            {{ $sends->name }}&nbsp;
                        </td>
                        <td>
                            @foreach($sends_resource_status_list[$sends->sends_id] as $sends_resource_status)
                                <span class="badge badge-pill badge-{{ $sends_resource_status['color'] }}">
                                    {{ $sends_resource_status['nb'] . ' ' . $sends_resource_status['description'] }}
                                </span><br/>
                            @endforeach
                        </td>
                        <td class="text-center">{{ $sends->nb_sends_line }}</td>
                        <td class="text-center">
                            <button class="btn btn-xs btn-outline-danger" onclick="deleteSends({{ $sends->sends_id }})">
                                <i class="fa fa-trash"></i> Supprimer
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @include('layouts.footerbar')

</div>

@include('layouts.footer', ['page' => ''])
