
@include('layouts.header')

@auth
    @include('layouts.sidebar', ['id_role' => Auth::user()->id_role])
@endauth

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        @include('layouts.navbartop', ['page_name' => 'Gestion des utilisateurs'])

        <div class="container-fluid">

            <!--<div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h4 mb-3 text-gray-800">Gestion des utilisateurs</h1>
            </div>-->

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    <button id="btn-add" name="btn-add" class="btn btn-sm btn-outline-success"><i class="fas fa-plus"></i> Ajouter un utilisateur</button>
                </div>

                <div class="card-body">

                    <!-- Liste des données -->

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="col-3">Nom prénom</th>
                                    <th class="col-3">Email</th>
                                    <th class="col-2">Rôle</th>
                                    <th class="col-1"></th>
                                </tr>
                            </thead>
                            <tbody id="users-list" name="users-list">
                                @foreach ($users as $user)
                                <tr id="name{{$user->id}}">
                                    <td>{{$user->lastname}} {{$user->firstname}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->role}}</td>
                                    <td class="text-center">
                                        <button class="btn btn-xs btn-outline-primary open-modal" value="{{$user->id}}"><i class="fas fa-pen"></i> Editer</button>
                                        {{--@if($user->id > 3)
                                        <button class="btn btn-xs btn-outline-danger delete-user" value="{{$user->id}}"><i class="fas fa-trash"></i> Supprimer</button>
                                        @endif--}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <!-- Formulaire d'ajout / édition -->

                    <div class="modal fade" id="userEditorModal" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="userEditorModalLabel">Ajout / Edition d'un utilisateur</h4>
                                </div>
                                <div class="modal-body">

                                    {{-- Formulaire d'ajout / édition --}}

                                    <form id="modalFormData" name="modalFormData" class="form-horizontal">

                                        {{-- Titres onglets --}}

                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="compte-tab" data-toggle="tab" href="#compte" role="tab"
                                                   aria-controls="compte" aria-selected="true">Compte</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="profil-tab" data-toggle="tab" href="#profil" role="tab"
                                                   aria-controls="profil" aria-selected="true">Profil</a>
                                            </li>
                                        </ul>

                                        {{-- Fin titres onglets --}}

                                        {{-- Panneaux des onglets --}}

                                        <div class="tab-content" id="myTabContent">

                                            {{-- Onglet "Compte" --}}

                                            <div class="tab-pane fade show active pt-3" id="compte" role="tabpanel" aria-labelledby="compte-tab">

                                                <div class="form-group row">
                                                    <label for="id_role" class="col-sm-3 col-form-label text-right">Rôle :</label>
                                                    <div class="col-sm-8">
                                                        <select class="form-control" id="id_role" name="id_role">
                                                            @foreach ($roles as $role)
                                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-3 col-form-label text-right">Nom d'utilisateur :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="name" name="name">
                                                    </div>
                                                    <div class="offset-sm-3 col-sm-9 text-danger messageErrorForm" style="display: none;" id="errorMessageName"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="email" class="col-sm-3 col-form-label text-right">Adresse email :</label>
                                                    <div class="col-sm-8">
                                                        <input type="email" class="form-control" id="email" name="email" >
                                                    </div>
                                                    <div class="offset-sm-3 col-sm-9 text-danger messageErrorForm" style="display: none;" id="errorMessageEmail"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="password" class="col-sm-3 col-form-label text-right">Mot de passe :</label>
                                                    <div class="col-sm-8">
                                                        <input type="password" class="form-control" id="password" name="password" >
                                                    </div>
                                                    <div class="offset-sm-3 col-sm-9 text-danger messageErrorForm" style="display: none;" id="errorMessagePassword"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="password_confirmation" class="col-sm-3 col-form-label text-right">Confirmation :</label>
                                                    <div class="col-sm-8">
                                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                                    </div>
                                                    <div class="offset-sm-3 col-sm-9 text-danger messageErrorForm" style="display: none;" id="errorMessagePasswordConfirm"></div>
                                                </div>

                                            </div>

                                            {{-- Onglet "Profil" --}}

                                            <div class="tab-pane fade pt-3" id="profil" role="tabpanel" aria-labelledby="profil-tab">

                                                <div class="form-group row">
                                                    <label for="lastname" class="col-sm-3 col-form-label text-right">Nom :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="lastname" name="lastname" >
                                                    </div>
                                                    <div class="offset-sm-3 col-sm-9 text-danger messageErrorForm" style="display: none;" id="errorMessageLastname"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="firstname" class="col-sm-3 col-form-label text-right">Prénom :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="firstname" name="firstname">
                                                    </div>
                                                    <div class="offset-sm-3 col-sm-9 text-danger messageErrorForm" style="display: none;" id="errorMessageFirstname"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="api_key" class="col-sm-3 col-form-label text-right">Clé API Nakala :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="api_key" name="api_key" maxlength="36">
                                                    </div>
                                                    <div class="offset-sm-3 col-sm-9 text-danger messageErrorForm" style="display: none;" id="errorMessageApiKey"></div>
                                                </div>

                                            </div>

                                        </div>

                                        {{-- Fin panneaux des onglets --}}

                                    </form>

                                    {{-- Fin formulaire d'ajout / édition --}}

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-xs btn-outline-primary" id="btn-save" value="add">Enregistrer</button>
                                    <button type="button" class="btn btn-xs btn-outline-danger" id="btn-save" onclick="$('#userEditorModal').modal('hide');">Annuler</button>
                                    <input type="hidden" id="id_user" name="id_user" value="0">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- End of Main Content -->

    @include('layouts.footerbar')

</div>
<!-- End of Content Wrapper -->

@include('layouts.footer', ['page' => 'user'])
