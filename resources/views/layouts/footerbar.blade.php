
<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>
                {{ config('app.name') }} -
                {{ get_git_commit_id() }}
            </span>
        </div>
    </div>
</footer>
<!-- End of Footer -->
