<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>{{ env('APP_NAME') }}</title>

    <!-- Font Awesome 5.9.0 -->
    <link href="{{ asset('css/fontawesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Bootstrap v4.3.1 - SB Admin 2 v4.0.6 -->
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

    <!-- Modifs perso -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script type="text/javascript">
        window.translations = {!! Cache::get('translations') !!};
    </script>

    @routes
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">
