
<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/') }}">
        <div class="sidebar-brand-icon"></div>
        <div class="sidebar-brand-text mx-3">{{ config('app.name') }}</div>
    </a>

<!--    <hr class="sidebar-divider my-0">

    <li class="nav-item active">
        <a class="nav-link" href="{{ url('/') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>@lang('app.dashboard')</span>
        </a>
    </li>-->

    <hr class="sidebar-divider">

    @include('layouts.sidebar_' . $id_role)

</ul>
<!-- End of Sidebar -->

