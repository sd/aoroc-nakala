
</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Déconnection ?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Cliquer sur "Quitter" pour mettre fin à la session actuelle.</div>
            <div class="modal-footer">
                <a class="btn btn-primary" href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Quitter</a>

                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </div>
        </div>
    </div>
</div>

<!-- jQuery v3.4.1 -  Bootstrap v4.3.1 -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

<!-- Core plugin JavaScript -->
<script src="{{ asset('js/jquery.easing.min.js') }}"></script>

<!-- Custom scripts for all pages-->
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

<script src="{{ asset('js/jquery.form.min.js') }}"></script>

@switch($page)
    @case('role') <script src="{{ asset('js/admin/role.js') }}"></script> @break
    @case('user') <script src="{{ asset('js/admin/user.js') }}"></script> @break
    @case('profil') <script src="{{ asset('js/profil.js') }}"></script> @break
    @case('resource') <script src="{{ asset('js/resource.js') }}"></script> @break
    @case('admin_resource') <script src="{{ asset('js/resource.js') }}"></script><script src="{{ asset('js/admin/resource.js') }}"></script> @break
    @case('sends' or 'sends_list') <script src="{{ asset('js/sends.js') }}"></script> @break
    @default
@endswitch

</body>
</html>
