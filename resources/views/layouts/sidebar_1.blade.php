
<!-- Nav Item - Rôles et utilisateurs -->
<li class="nav-item">
    <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapse_1" aria-expanded="true" aria-controls="collapse_1">
        <i class="fas fa-fw fa-user"></i>
        <span>@lang('app.sidebar_1.bloc.user')</span>
    </a>
    <div id="collapse_1" class="collapse" aria-labelledby="heading_1" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            {{--<h6 class="collapse-header">Custom Components:</h6>--}}
            <a class="collapse-item" href="{{ route(('user.list')) }}">@lang('app.sidebar_1.menu.utilisateur')</a>
            <a class="collapse-item" href="{{ route(('role.list')) }}">@lang('app.sidebar_1.menu.role')</a>
        </div>
    </div>
</li>

<!-- Nav Item --------------- -->
@include('layouts.sidebar_2')

<!-- Nav Item --------------- -->
<li class="nav-item">
    <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapse_3" aria-expanded="true" aria-controls="collapse_3">
        <i class="fas fa-fw fa-user-ninja"></i>
        <span>Admin</span>
    </a>
    <div id="collapse_3" class="collapse" aria-labelledby="heading_3" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ route('admin.sends.list') }}">liste des envois</a>
        </div>
    </div>
</li>
