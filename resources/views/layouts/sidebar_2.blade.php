
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_2" aria-expanded="true" aria-controls="collapse_2">
            <i class="fas fa-fw fa-cog"></i>
            <span>Envois</span>
        </a>
        <div id="collapse_2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <!--<h6 class="collapse-header">Infos...</h6>-->
                <a class="collapse-item" href="{{ route('sends.index') }}">Envoi de données</a>
                <a class="collapse-item" href="{{ route('sends.list') }}">Gestion des envois</a>
                <a class="collapse-item" href="{{ route('sends.error') }}">Lignes en erreur</a>
            </div>
        </div>
    </li>
