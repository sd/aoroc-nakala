
@include('layouts.header')
@include('layouts.sidebar', ['id_role' => Auth::user()->id_role])

<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">

        @include('layouts.navbartop', ['page_name' => 'Envoi de données'])

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="container-fluid">

            <div class="row" id="p1_project_start">
                <div class="alert alert-secondary alert-secondary-light col-12" role="alert">
                    <strong>Etape 1</strong> - Choix du projet <i class="fa fa-check fa-2x float-right" style="color: transparent"></i>
                </div>
            </div>
            <div class="row" id="p1_project_result" style="display: none"><div class="alert alert-success col-12" role="alert"></div>
            </div>

            <div class="row" id="p2_name_start">
                <div class="alert alert-secondary alert-secondary-light col-12" role="alert">
                    <strong>Etape 2</strong> - Nommage de votre envoi <i class="fa fa-check fa-2x float-right" style="color: transparent"></i>
                </div>
            </div>
            <div class="row" id="p2_name_result" style="display: none">
                <div class="alert alert-success col-12" role="alert"></div>
            </div>

            <div class="row" id="p3_data_start">
                <div class="alert alert-secondary alert-secondary-light col-12" role="alert">
                    <strong>Etape 3</strong> - Envoir de votre fichier de données <i class="fa fa-check fa-2x float-right" style="color: transparent"></i>
                </div>
            </div>
            <div class="row" id="p3_data_result" style="display: none">
                <div class="alert alert-success col-12" role="alert"></div>
            </div>

            <div class="row" id="p4_media_start">
                <div class="alert alert-secondary alert-secondary-light col-12" role="alert">
                    <strong>Etape 4</strong> - Envoi de vos fichiers média <i class="fa fa-check fa-2x float-right" style="color: transparent"></i>
                </div>
            </div>
            <div class="row" id="p4_media_result" style="display: none">
                <div class="alert alert-success col-12" role="alert"></div>
            </div>

            <div class="row" id="p5_status_start">
                <div class="alert alert-secondary alert-secondary-light col-12" role="alert">
                    <strong>Etape 5</strong> - Choix du statut <i class="fa fa-check fa-2x float-right" style="color: transparent"></i>
                </div>
            </div>
            <div class="row" id="p5_status_result" style="display: none">
                <div class="alert alert-success col-12" role="alert"></div>
            </div>

            <!-- Etape 1 : Choix du type d'envoi ------------------------------------------------------------------------------------------------- -->

            <div class="row" id="p1_project">
                <div class="display-1 pl-5 col-2 text-right">1.</div>
                <div class="col-9 pt-5">
                    <div class="form-group row">
                    <label class="col-sm-4 col-form-label text-right">Envoyer des données pour</label>
                    <div class="col-sm-4 text-left">
                        <button class="btn btn-outline-primary btn-block" type="button" onclick="setProjectChoice('nahan')">NAHAN</button>
                    </div>
                    <div class="col-sm-4 text-left">
                        <button class="btn btn-outline-primary btn-block" type="button" onclick="setProjectChoice('autre')">AUTRE</button>
                    </div>
                    </div>
                </div>
            </div>

            <!-- Etape 2 : Nommage de l'envoi ---------------------------------------------------------------------------------------------------- -->

            <div class="row" id="p2_name" style="display: none">
                <div class="display-1 pl-5 col-2 text-right">2.</div>
                <div class="col-9 pt-1">
                    <div class="form-group row mt-5">
                        <label class="col-sm-4 col-form-label text-right">
                            Nommer votre envoi
                        </label>
                        <div class="col-sm-6 text-left">
                            <div class="input-group">
                                <input type="text" class="form-control" id="sends_name" maxlength="50">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-success" type="button" onclick="setSendsName()">Enregistrer</button>
                                </div>
                            </div>
                            <small id="sends_name" class="form-text text-muted">
                                Il s'agit d'un nom de 50 caractères maximum permettant de référencer votre envoi.
                            </small>
                            <div class="text-danger danger_name"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Etape 3 : Envoi du fichier de données ------------------------------------------------------------------------------------------- -->

            <div class="row" id="p3_data" style="display: none">
                <div class="display-1 pl-5 col-2 text-right">3.</div>
                <div class="col-9 pt-1">
                    <form action="{{ route('sends.ajax.upload_data_file') }}" method="post" enctype="multipart/form-data">
                        <div class="form-group row mt-5">
                            @csrf

                            <label class="col-sm-4 col-form-label text-right">
                                Envoyer votre fichier de données
                            </label>
                            <div class="col-sm-6 text-left">
                                <div class="input-group input-group-sm">
                                    <div class="custom-file">
                                        <input type="file" class="form-control-file" id="data_file" name="data_file">
                                        <label class="custom-file-label" for="data_file">Choix du fichier</label>
                                    </div>
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-success" onclick="checkDataFile()">Envoyer</button>
                                    </div>
                                </div>
                                <small id="sends_name" class="form-text text-muted">
                                    Format CSV
                                </small>
                                <div class="text-danger danger_data"></div>
                                <div class="progress mt-2 progress_data" style="display: none">
                                    <div class="progress-bar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width: 0"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Etape 4 : Envoi des fichiers média ---------------------------------------------------------------------------------------------- -->

            <div class="row" id="p4_media" style="display:none">
                <div class="display-1 pl-5 col-2 text-right">4.</div>
                <div class="col-9 pt-1">
                    <form action="{{ route('sends.ajax.upload_media_file') }}" method="post" enctype="multipart/form-data" >
                    <div class="form-group row mt-5">
                        @csrf

                        <label class="col-sm-4 col-form-label text-right">
                            Envoyer vos fichiers média
                        </label>
                        <div class="col-sm-6 text-left">
                            <div class="input-group input-group-sm">
                                <div class="custom-file">
                                    <input type="file" multiple class="custom-file-input" id="media_file" name="media_file[]">
                                    <label class="custom-file-label" for="data_file">Choix du/des fichiers</label>
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-success" onclick="checkMediaFile()">Envoyer</button>
                                </div>
                            </div>

                            <small id="sends_name" class="form-text text-muted">
                                Type de fichiers autorisé : images (jpg, png, gif), archive (zip), autre (pdf et 7z)
                            </small>

                            <div class="row mt-3">
                                <div class="col-12" style="color: #FF5131; font-size: 0.9rem;">
                                    <i class="fa fa-exclamation-triangle fa-2x float-left mr-2"></i>
                                    Attention, la limite d'upload ne peut<br/>dépasser 500 Mo par envoi (ou 100 fichiers, hors ZIP).
                                </div>
                            </div>

                            <div class="text-danger danger_media"></div>

                            <div class="mt-2" style="display:none" id="p4_wait_message">
                                <div class="text-danger text-center p-2">
                                    <i class="fa fa-spinner fa-pulse fa-2x p-4"></i><br/>Veuillez patienter pendant l'envoi des fichiers en ligne.
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>

            <!--  Etape 5 : Choix du statut d'envoi ---------------------------------------------------------------------------------------------- -->

            <div class="row" id="p5_status" style="display: none">
                <div class="display-1 pl-5 col-2 text-right">5.</div>
                <div class="col-9 pt-5">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label text-right">Choix du statut</label>
                        <div class="col-sm-4 text-left">
                            <button class="btn btn-outline-primary btn-block" type="button" onclick="setSendsStatus('published')">PUBLIC</button>
                        </div>
                        <div class="col-sm-4 text-left">
                            <button class="btn btn-outline-primary btn-block" type="button" onclick="setSendsStatus('pending')">PRIVÉ</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @include('layouts.footerbar')
</div>

@include('layouts.footer', ['page' => 'sends'])
