<?php

return [
    'dashboard' => 'Dashboard',

    // Pages de la section Admin
    'admin' => [
        // Page admin.user
        'user' => [
            'title'                             => 'User management',
            'title_box_add'                     => 'Add / Edit user',
            'th_user'                           => 'Name',
            'th_email'                          => 'Email',
            'th_role'                           => 'Role',
            'th_type_users'                     => 'User type',
            'button_add'                        => 'Add user',

            'label_role'                        => 'Role',
            'label_type_users'                  => 'User type',
            'label_name'                        => 'User name',
            'label_email'                       => 'Email address',
            'label_password'                    => 'Password',
            'label_password_confirmation'       => 'Password confirmation',
            'label_firstname'                   => 'Firstname',
            'label_lastname'                    => 'Lastname',
            'label_social_reason'               => 'Social reason',
            'label_address_1'                   => 'Address',
            'label_address_note'                => 'Internal memo',
            'label_country'                     => 'Country',
            'label_city'                        => 'City',

            'placeholder_address_2'             => 'Address supplement',
            'placeholder_address_3'             => 'Address supplement',
        ],

        // Page admin.role
        'role' => [
            'title'            => 'Role management',
            'title_box_add'    => 'Add / Edit role',
            'th_role'          => 'Role',
            'button_add'       => 'Add role',
            'placeholder_name' => 'Role name',
        ],

        // Page admin.type_users
        'type_users' => [
            'title'            => 'User types namagement',
            'title_box_add'    => 'Add / Edit user type',
            'th_status'        => 'User types',
            'button_add'       => 'Add user type',
            'placeholder_name' => 'user type name',
        ],

        // Page admin.contenant
        'contenant' => [
            'title'                   => 'Container management',
            'title_box_add'           => 'Add / Edit container',
            'th_contenant'            => 'Containers',
            'th_description'          => 'Description',
            'button_add'              => 'Add container',
            'placeholder_name'        => 'Container name',
            'placeholder_description' => 'Description',
        ],

        // Page admin.statut
        'statut' => [
            'title'            => 'Sending status management',
            'title_box_add'    => 'Add / Edit status',
            'th_status'        => 'Status',
            'button_add'       => 'Add status',
            'placeholder_name' => 'Status name',
        ],

        // Page admin.pays
        'pays' => [
            'title'            => 'Country management',
            'title_box_add'    => 'Add / Edit country',
            'th_pays'          => 'Country',
            'th_code'          => 'Code',
            'button_add'       => 'Add country',
            'placeholder_name' => 'Country name',
            'placeholder_code' => 'Country code (2 characters)',
        ],

        // Page admin.ville
        'ville' => [
            'title'                   => 'City management',
            'title_box_add'           => 'Add / Edit city',
            'th_ville'                => 'City',
            'th_code_postal'          => 'Postal code',
            'th_complement'           => 'Complement',
            'button_add'              => 'Add city',
            'placeholder_name'        => 'City name',
            'placeholder_code_postal' => 'Postal code',
            'placeholder_complement'  => 'Complement',
            'texte_filtre'            => 'Filter cities by country :',
        ],

        // Page admin.contenant
        'etape_transit' => [
            'title'                   => 'Transit management step',
            'title_box_add'           => 'Add / Edit transit step',
            'th_etape'                => 'Transit step',
            'th_ville'                => 'City',
            'button_add'              => 'Add transit step',
            'placeholder_name'        => 'Nom de l\'étape',
            'placeholder_adresse_1'   => 'Address 1',
            'placeholder_adresse_2'   => 'Address supplement 1',
            'placeholder_adresse_3'   => 'Address supplement 2',
            'placeholder_note'        => 'Note',
        ],
    ],

    // Page profil
    'profil' => [
        'title'       => 'Profile',
        'label_email' => 'Email',
        'label_role'  => 'Role',
    ],

    // Nav bar top
    'navbartop' => [
        'profil' => 'Profile',
        'logout' => 'Logout',
    ],

    // Side bar
    'sidebar_1' => [
        'head' => [
            'admin' => 'Admin',
        ],
        'bloc' => [
            'user' => 'Roles and users',
            'data' => 'Datas',
        ],
        'menu' => [
            'role'          => 'Roles',
            'utilisateur'   => 'Users',
            'contenant'     => 'Containers',
            'statut'        => 'Sending status',
            'type_users'    => 'User types',
            'pays'          => 'Country',
            'ville'         => 'City',
            'etape_transit' => 'Transit step',
        ],
    ],

    // Boutons
    'button_add'    => 'Add',
    'button_edit'   => 'Edit',
    'button_delete' => 'Delete',
    'button_save'   => 'Save',
    'button_cancel' => 'Cancel',

    // Confirmation
    'confirm_delete' => 'Do you confirm the deletion ?',
];
