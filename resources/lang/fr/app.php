<?php

return [
    'dashboard' => 'Tableau de bord',

    // Pages de la section Admin
    'admin' => [
        // Page admin.user
        'user' => [
            'title'                             => 'Gestion des utilisateurs',
            'title_box_add'                     => 'Ajout / Edition d\'un utilisateur',
            'th_user'                           => 'Nom',
            'th_email'                          => 'Email',
            'th_role'                           => 'Rôle',
            'th_type_users'                     => 'Type util.',
            'button_add'                        => 'Ajouter un utilisateur',

            'label_role'                        => 'Rôle',
            'label_type_users'                  => 'Type d\'utilisateur',
            'label_name'                        => 'Nom d\'utilisateur',
            'label_email'                       => 'Adresse email',
            'label_password'                    => 'Mot de passe',
            'label_password_confirmation'       => 'Confirmation',
            'label_firstname'                   => 'Prénom',
            'label_lastname'                    => 'Nom',
            'label_social_reason'               => 'Raison sociale',
            'label_address_1'                   => 'Adresse',
            'label_address_note'                => 'Note interne',
            'label_country'                     => 'Pays',
            'label_city'                        => 'Ville',

            'placeholder_address_2'             => 'Complément d\'adresse',
            'placeholder_address_3'             => 'Complément d\'adresse',
        ],

        // Page admin.role
        'role' => [
            'title'            => 'Gestion des rôles',
            'title_box_add'    => 'Ajout / Edition d\'un rôle',
            'th_role'          => 'Rôles',
            'button_add'       => 'Ajouter un rôle',
            'placeholder_name' => 'Nom du rôle',
        ],

        // Page admin.type_users
        'type_users' => [
            'title'            => 'Gestion des types d\'utilisateur',
            'title_box_add'    => 'Ajout / Edition d\'un type d\'utilisateur',
            'th_status'        => 'Types',
            'button_add'       => 'Ajouter un type',
            'placeholder_name' => 'Nom du type',
        ],

        // Page admin.contenant
        'contenant' => [
            'title'                   => 'Gestion des contenants',
            'title_box_add'           => 'Ajout / Edition d\'un contenant',
            'th_contenant'            => 'Contenants',
            'th_description'          => 'Description',
            'button_add'              => 'Ajouter un contenant',
            'placeholder_name'        => 'Nom du contenant',
            'placeholder_description' => 'Description',
        ],

        // Page admin.statut
        'statut' => [
            'title'            => 'Gestion des statuts d\'envoi',
            'title_box_add'    => 'Ajout / Edition d\'un statut',
            'th_status'        => 'Statuts',
            'button_add'       => 'Ajouter un statut',
            'placeholder_name' => 'Nom du statut',
        ],

        // Page admin.pays
        'pays' => [
            'title'            => 'Gestion des pays',
            'title_box_add'    => 'Ajout / Edition d\'un pays',
            'th_pays'          => 'Pays',
            'th_code'          => 'Code',
            'button_add'       => 'Ajouter un pays',
            'placeholder_name' => 'Nom du pays',
            'placeholder_code' => 'Code du pays (2 caractères)',
        ],

        // Page admin.ville
        'ville' => [
            'title'                   => 'Gestion des villes',
            'title_box_add'           => 'Ajout / Edition d\'une ville',
            'th_ville'                => 'Villes',
            'th_code_postal'          => 'Code postal',
            'th_complement'           => 'Complément',
            'button_add'              => 'Ajouter une ville',
            'placeholder_name'        => 'Nom de la ville',
            'placeholder_code_postal' => 'Code postal',
            'placeholder_complement'  => 'Complément',
            'texte_filtre'            => 'Filtrer les villes par pays :',
        ],

        // Page admin.contenant
        'etape_transit' => [
            'title'                 => 'Gestion des étapes de transit',
            'title_box_add'         => 'Ajout / Edition d\'une étape de transit',
            'th_etape'              => 'Etape de transit',
            'th_ville'              => 'Ville',
            'button_add'            => 'Ajouter une étape de transit',
            'placeholder_nom'       => 'Nom de l\'étape',
            'placeholder_adresse_1' => 'Adresse 1',
            'placeholder_adresse_2' => 'Complément adresse 1',
            'placeholder_adresse_3' => 'Complément adresse 2',
            'placeholder_note'      => 'Note',
        ],
    ],

    // Page profil
    'profil' => [
        'title'       => 'Profil',
        'label_email' => 'Email',
        'label_role'  => 'Rôle',
    ],

    // Nav bar top
    'navbartop' => [
        'profil' => 'Profil',
        'logout' => 'Deconnexion',
    ],

    // Side bar
    'sidebar_1' => [
        'head' => [
            'admin' => 'Admin',
        ],
        'bloc' => [
            'user' => 'Rôles et utilisateurs',
            'data' => 'Datas',
        ],
        'menu' => [
            'role'          => 'Rôles',
            'utilisateur'   => 'Utilisateurs',
            'contenant'     => 'Contenants',
            'statut'        => 'Statut d\'envoi',
            'type_users'    => 'Types d\'utilisateur',
            'pays'          => 'Pays',
            'ville'         => 'Villes',
            'etape_transit' => 'Etapes de transit',
        ],
    ],

    // Boutons
    'button_add'    => 'Ajouter',
    'button_edit'   => 'Editer',
    'button_delete' => 'Supprimer',
    'button_save'   => 'Enregistrer',
    'button_cancel' => 'Annuler',

    // Confirmation
    'confirm_delete' => 'Confirmer vous la suppression ?',
];
